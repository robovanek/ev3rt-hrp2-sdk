#include "ev3api.h"
#include "app.h"
#include <unwind.h>
#include <t_syslog.h>

struct frame_state {
    int depth;
};

const char *reg_name[16] = {
    "r0",
    "r1",
    "r2",
    "r3",
    "r4",
    "r5",
    "r6",
    "r7",
    "r8",
    "r9",
    "r10",
    "r11",
    "r12",
    "sp",
    "lr",
    "pc",

};



_Unwind_Reason_Code handle_frame(_Unwind_Context *ctx, void *usr) {
    struct frame_state *state = (struct frame_state *)usr;

    int depth = ++state->depth;
    syslog(LOG_EMERG, "Frame %d:", depth);
    for (int i = 0; i < 16; i++) {
        syslog(LOG_EMERG, "%2d| %s = 0x%08X", depth, reg_name[i], _Unwind_GetGR(ctx, i));
    }

    return _URC_NO_REASON;
}

void a() __attribute__((noinline));
void doBacktrace() __attribute__((noinline));

void a() {
    doBacktrace();
}

void doBacktrace() {
    struct frame_state info = {0};
    syslog(LOG_EMERG, "BACKTRACE:");
    _Unwind_Reason_Code code = _Unwind_Backtrace(&handle_frame, &info);
    switch (code) {
    case _URC_OK:
        syslog(LOG_EMERG, "Unwind OK");
        break;
    case _URC_FOREIGN_EXCEPTION_CAUGHT:
        syslog(LOG_EMERG, "Unwind foreigh exception caught");
        break;
    case _URC_END_OF_STACK:
        syslog(LOG_EMERG, "Unwind end of stack");
        break;
    case _URC_HANDLER_FOUND:
        syslog(LOG_EMERG, "Unwind handler found");
        break;
    case _URC_INSTALL_CONTEXT:
        syslog(LOG_EMERG, "Unwind install context");
        break;
    case _URC_CONTINUE_UNWIND:
        syslog(LOG_EMERG, "Unwind continue");
        break;
    case _URC_FAILURE:
        syslog(LOG_EMERG, "Unwind failed");
        break;
    }
}

void main_task(intptr_t unused) {
    a();
}

void abort() {
    syslog(LOG_EMERG, "Abort called");
}
