#!/bin/sh

set -x
set -e
if [ "$#" -ne "1" ]; then
  echo "Usage: $0 [filename]" 1>&2
  exit 1
fi

cd "$(dirname "$0")"

DEV="/dev/disk/by-label/EV3DEV_BOOT"
MOUNT="/media/kuba/EV3DEV_BOOT"

mount "$DEV" || true
mkdir -p "$MOUNT/ev3rt/apps"

#cp "/home/kuba/lego/ev3rt-hrp2/base-workspace/uImage" "$MOUNT/"
#cp "/home/kuba/lego/ev3rt-hrp2/sdk/workspace/bin/$1.elf" "$MOUNT/ev3rt/apps/"
cp "/home/kuba/lego/ev3rt-hrp2/sdk/workspace/uImage" "$MOUNT/"

umount "$DEV"
sync
#udisksctl power-off -b "$DEV"
