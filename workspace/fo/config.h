#pragma once

// THR
#define   BATT_DELAY_MSEC  2000
#define    REG_DELAY_MSEC    10

// PORTS
#define S(x) (x - 1)
#define M(x) (x - 1)

// IN
#define SEN_COLOR S(2)

// DEBUG
#define DEBUG_UART   0
#define DEBUG_BT     1
#define DEBUG_LCD    2
#define DEBUG_STDOUT 3
#define DEBUG_STDERR 4
#define DEBUG_IO     DEBUG_LCD

