#include "header.h"

void input_init() {
	ev3_sensor_config(SEN_COLOR, COLOR_SENSOR);
}


int16_t input_read() {
	return ev3_color_sensor_get_reflect(SEN_COLOR);
}
