#include "header.h"

FILE *rmt;


void log_init() {
	rmt = 0;

#if   DEBUG_IO == DEBUG_BT
	while (!ev3_bluetooth_is_connected()) tslp_tsk(10);
	rmt = ev3_serial_open_file(EV3_SERIAL_BT);
#elif DEBUG_IO == DEBUG_UART
	rmt = ev3_serial_open_file(EV3_SERIAL_UART);
#elif DEBUG_IO == DEBUG_LCD
	rmt = ev3_serial_open_file(EV3_SERIAL_DEFAULT);
#elif DEBUG_IO == DEBUG_STDOUT
	rmt = stdout;
#elif DEBUG_IO == DEBUG_STDERR
	rmt = stderr;
#endif

	if (rmt == 0) {
		rmt = stdout;
	}
	
	fprintf(rmt, "[Log opened]\n");
}
