#include "header.h"

//
// main entry point
//

static SYSTIM last2;
static SYSTIM last;
static SYSTIM now;

void update_now() {
	get_tim(&now);
}

#define END_MAX  5
#define END_VAL  2

void main_task(intptr_t arg) {
	log_init();
	input_init();

	update_now();
	last = now;
	last2 = now;

	int16_t lmax = 0;
	SYSTIM tmax = now;

	RELTIM arr[6];
	memset(arr, 0, 6 * sizeof(RELTIM));

	while(true) {
		update_now();

		int16_t l = input_read();

		if (lmax < l) {
			lmax = l;
			tmax = now;
		}

		if (lmax > END_MAX && l < END_VAL) {
			RELTIM val = tmax - last2;
			
			memmove(arr + 1, arr + 0, 5 * sizeof(RELTIM));
			arr[0] = val;
			
			float avg = (arr[0] + arr[1] + arr[2] + arr[3] + arr[4] + arr[5]) / 6.0f;
			
			fprintf(rmt, "%4d (%f)\n", val, avg);
			last2 = last;
			last = tmax;
			tmax = now;
			lmax = 0;
		}

		dly_tsk(1);
	}

	os_exit();
}
