#include "header.h"

void snd_init(void) {
	ev3_speaker_set_volume(SND_VOLUME);
}

void snd_beep_ok(void) {
	ev3_speaker_play_tone(800, 100);
}

void snd_beep_error(void) {
	ev3_speaker_play_tone(500, 200);
}
