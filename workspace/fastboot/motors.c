#include "header.h"

/////////////////
// MOTOR LOCAL //
/////////////////

#define MOT_STEER (MOT_LEFT | MOT_RIGHT)
#define MOT_ALL   (MOT_STEER | MOT_SHOOT)

static void mot_ball_init(void);

static ringpos position;
static bool forward = true;


//////////
// INIT //
//////////

void mot_init(void) {
	ev3_motor_config(MOT_LEFT,  LARGE_MOTOR);
	ev3_motor_config(MOT_RIGHT, LARGE_MOTOR);
	ev3_motor_config(MOT_SHOOT, MEDIUM_MOTOR);
	mot_direction(true);
	ev3_motor_stop(MOT_STEER, true);
	ev3_motor_reset_counts(MOT_STEER);
	mot_ball_init();
}


////////////////////
// UNLIMITED MOVE //
////////////////////

void mot_freestart(void) {
	ev3_motor_start(MOT_STEER);
}

void mot_freespeed(int8_t speedR, int8_t speedL) {
	ev3_motor_speed(MOT_LEFT, speedL);
	ev3_motor_speed(MOT_RIGHT, speedR);
}

void mot_stop(void) {
	ev3_motor_stop(MOT_STEER, true);
}

void mot_float(void) {
	ev3_motor_stop(MOT_STEER, false);
}

void mot_direction(bool fwd) {
	forward = fwd;
	ev3_motor_polarity(MOT_STEER, fwd ? EV3_POL_REVERSE : EV3_POL_FORWARD);
}
bool mot_forward(void) {
	return forward;
}


//////////////////////
// SPEED & POSITION //
//////////////////////

int8_t mot_lspeed(void) {
	int8_t result;
	ev3_motor_get_velocity(MOT_LEFT, &result);
	return -result;
}

int8_t mot_rspeed(void) {
	int8_t result;
	ev3_motor_get_velocity(MOT_RIGHT, &result);
	return -result;
}

int32_t mot_lcount(void) {
	int32_t tacho;
	ev3_motor_get_counts(MOT_LEFT, &tacho);
	return -tacho;
}

int32_t mot_rcount(void) {
	int32_t tacho;
	ev3_motor_get_counts(MOT_RIGHT, &tacho);
	return -tacho;
}

void mot_resetcount(void) {
	ev3_motor_reset_counts(MOT_STEER);
}


/////////////////
// SYNCED MOVE //
/////////////////

void mot_travelsteps(int32_t steps, int16_t ratio, int8_t speed) {
	ev3_motor_move_sync(MOT_STEER, speed, EV3_MOVE_TACHO, ratio, steps, true);
}

////////////////
// TIMED MOVE //
////////////////

void mot_traveltime(int32_t time, int8_t power) {
	ev3_motor_move_power(MOT_STEER, power, EV3_MOVE_TIME, 0, power, 0, true);
}

/////////////////////////
// WAIT FOR COMPLETION //
/////////////////////////

void mot_waitfor(void) {
	ev3_motor_poll(MOT_STEER);
}


//////////////
// SHOOTING //
//////////////

// begin shooting
void mot_ball_begin(ringpos initial) {
	gfx_msgbox("SHOOTING");
	position = initial;
}

// configure for ball
void mot_ball_goto(ringpos new) {
	gfx_msgbox("SHOOTING-GOTO");
	int8_t sign = 1;
	int32_t diff = (new - position) * MOT_TURN_DEGPERRING;
	if (diff == 0) {
		goto aligned;
	} else if (diff < 0) {
		diff = -diff;
		sign = -1;
	}
	
	int8_t  speed  = MOT_TURN_SPEED;
	int16_t ratio  = 200 * sign;
	int32_t length = diff;
	
	
	ev3_motor_move_sync(MOT_STEER, speed, EV3_MOVE_TACHO, ratio, length, true);
	ev3_motor_poll(MOT_STEER);
aligned:
	position = new;
}

// rotate the carriage
void mot_ball_put(void) {
	gfx_msgbox("SHOOTING-PUT");
	ev3_motor_move_speed(MOT_SHOOT, MOT_SHOOT_SPEED, EV3_MOVE_TACHO,
		MOT_SHOOT_INOUT, MOT_SHOOT_DEGREES - 2 * MOT_SHOOT_INOUT, MOT_SHOOT_INOUT, true);
	ev3_motor_poll(MOT_SHOOT);
	dly_tsk(MOT_SHOOT_DELAY);
}

// make position known
void mot_ball_init(void) {
	// set correct polarity
	ev3_motor_polarity(MOT_SHOOT, MOT_SHOOTER_POLARITY);
	ev3_motor_stop(MOT_SHOOT, true);
	ev3_motor_reset_counts(MOT_SHOOT);
}
