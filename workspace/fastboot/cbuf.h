#pragma once

#include "header.h"

typedef uint8_t cbuf_elem;

typedef struct {
	volatile cbuf_elem* pStart;
	volatile cbuf_elem* pEnd;
	volatile cbuf_elem* pRead;
	volatile cbuf_elem* pWrite;
} cbuf;

extern void               cbuf_set     (cbuf volatile * pBuf, cbuf_elem in);
extern bool               cbuf_push    (cbuf volatile * pBuf);
extern volatile cbuf_elem cbuf_get     (cbuf volatile * pBuf);
extern bool               cbuf_pop     (cbuf volatile * pBuf);
extern void               cbuf_init    (cbuf volatile * pBuf, cbuf_elem volatile * pArray, size_t capacity);

extern size_t             cbuf_avail_r (cbuf volatile * pBuf);
extern size_t             cbuf_avail_w (cbuf volatile * pBuf);
