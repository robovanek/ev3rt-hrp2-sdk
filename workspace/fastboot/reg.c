#include "header.h"

//
// Local stuff
//

////////
// local functions
//

// speed generator
static int8_t reg_speedgen(void);
static int8_t reg_speedgen_max(void);
static void   reg_speedgen_configure(const RegSpeedGen *pDetails);
static int8_t  genRampup  (int32_t delta);
static int8_t  genSteady  (void);
static int8_t  genRampdown(int32_t delta);
static int8_t  genClamp   (int32_t speed);
static inline int32_t fastSqrt(int32_t hue);
static inline int32_t fastCos(int32_t hue);

//
// Steering regulator
//

// thread unsafe, but I don't care; we save stack space
static RegSpeedGenCached spdData;

#ifndef REG_AVOID_SIDETOUCH
void reg_step(const RegParam* pParams, const RegHooks* pHooks, const RegSpeedGen *pSpeed) {
	int8_t  basespeed, maxspeed;
	int16_t measurement, setpoint, error, d, dd, action;
	int16_t lspeed, rspeed;
	int32_t integral = 0;

//#ifdef REG_AVAIL_GYRO
//	int16_t angleNow, angleError;
//	int16_t angleOld = headingRight();
//#else
	d = 0;
//#endif

	reg_speedgen_configure(pSpeed);

	// first push
	basespeed = reg_speedgen();
	mot_freespeed(basespeed, basespeed);
	mot_freestart();

	// loop
	while(true) {
		// get speeds
		basespeed   = reg_speedgen();
		maxspeed    = reg_speedgen_max();
		
		// check stop condition
		if (basespeed <= 0) {
			break;
		}
		
		// get P
		measurement = pHooks->doMeasurement();
		setpoint    = pHooks->getSetpoint  ();
		error      = -(setpoint - measurement);
		

		// get DD
		lspeed = mot_lspeed();
		rspeed = mot_rspeed();
		dd = -(lspeed - rspeed);


		// get D
//#ifdef REG_AVAIL_GYRO
//		angleNow = headingNow();
//		angleError = angleOld - angleNow;
//		d = fastCos(angleError);
//#endif

		// get I
		integral += error;
		if (integral > pParams->LimitI) {
			integral = pParams->LimitI;
		} else if (integral < -pParams->LimitI) {
			integral = -pParams->LimitI;
		}
		

		// sum it all
		action = (pParams->Kp  * error    +
		          pParams->Ki  * integral +
		          pParams->Kd  * d / 1000 +
		          pParams->Kdd * dd) / pParams->Divisor;


		// wheel steering
		lspeed = basespeed + action /* * basespeed / maxspeed */;
		rspeed = basespeed - action /* * basespeed / maxspeed */;

		// push & sleep
		mot_freespeed(rspeed, lspeed);
		dly_tsk(REG_DELAY);
	}
	// stop and wait
	mot_stop();
	mot_waitfor();
}
#else

void reg_step(const RegParam* pParams, const RegHooks* pHooks, const RegSpeedGen *pSpeed) {
	reg_straight(pSpeed);
}

#endif

//
// Straight regulator
//
#ifndef REG_SYNCED_FORWARD

void reg_straight(const RegSpeedGen *pSpeed) {
	int8_t spd;
	
	reg_speedgen_configure(pSpeed);
	
	// first push
	spd = reg_speedgen();
	mot_freespeed(spd, spd);
	mot_freestart();

	while(true) {
		// sleep
		dly_tsk(REG_DELAY);

		// fetch
		spd = reg_speedgen();

		// stop condition
		if (spd <= 0) {
			break;
		}

		// push
		mot_freespeed(spd, spd);
	}

	// stop & wait
	mot_stop();
	mot_waitfor();
}

#else

void reg_straight(const RegSpeedGen *pSpeed) {
	int8_t spd;
	bool invert = mot_forward();
	
	reg_speedgen_configure(pSpeed);
	
	// first push
	spd = reg_speedgen();

	while(true) {

		// fetch
		spd = reg_speedgen();

		// stop condition
		if (spd <= 0) {
			break;
		}

		// push
		if (invert)
			mot_travelsteps(-REG_ULTRAMEGA, 0, -spd);
		else
			mot_travelsteps(+REG_ULTRAMEGA, 0, +spd);
		
		// sleep
		dly_tsk(REG_DELAY_SYNC);
	}

	// stop & wait
	mot_stop();
	mot_waitfor();
}
#endif


//
// SPEED GENERATOR
//

// speed limit
static int8_t genClamp(int32_t speed) {
	if (speed < spdData.lowerSpeed) {
		return spdData.lowerSpeed;
	} else if (speed > spdData.upperSpeed) {
		return spdData.upperSpeed;
	} else {
		return (int8_t)speed;
	}
}

// rampup with constant acceleration
static int8_t genRampup(int32_t delta) {
	int32_t velocity = fastSqrt(spdData.linPartAccel * delta);
	return genClamp(velocity);
}

// constant velocity
static int8_t genSteady() {
	return spdData.upperSpeed;
}

// rampdown with constant deceleration
static int8_t genRampdown(int32_t delta) {
	int32_t velocity = fastSqrt(spdData.absPart + spdData.linPartDecel * delta);
	return genClamp(velocity);
}

//
// speed generator
//
int8_t reg_speedgen() {
	int32_t measurement;

	if (spdData.shallWeStop(spdData.arg)) {
		return 0;
	}

	measurement = spdData.doMeasurement();
	
	gfx_clear(5);
	gfx_printf(0, 5, "%d", measurement);

	if (measurement >= spdData.decelRampStart) {
		return genRampdown(measurement -  spdData.decelRampStart);
	} else if (measurement >= spdData.accelRampEnd) {
		return genSteady();
	} else {
		return genRampup(measurement);
	}
}

int8_t reg_speedgen_max() {
	return spdData.upperSpeed;
}

// configure 
void   reg_speedgen_configure(const RegSpeedGen *pDetails) {
	spdData.doMeasurement  = pDetails->doMeasurement;
	spdData.shallWeStop    = pDetails->shallWeStop;
	spdData.arg            = pDetails->arg;

	spdData.upperSpeed     = pDetails->upperSpeed;
	spdData.lowerSpeed     = pDetails->lowerSpeed;

	spdData.absPart        = spdData.upperSpeed * spdData.upperSpeed;
	spdData.linPartAccel   = +2 * pDetails->startAcceleration;
	spdData.linPartDecel   = -2 * pDetails->stopDeceleration;

	spdData.accelRampEnd   = spdData.absPart / spdData.linPartAccel;
	spdData.decelRampStart = pDetails->decelStartDistance;
}

// fast sqrt
int32_t fastSqrt(int32_t hue) {
	float n = (float)hue;
	float nhalf;
	uint32_t i;

	nhalf = n * 0.5F;
	i = * (uint32_t*) &n;
	i = 0x5f3759df - ( i >> 1 );
	n = * (float*) &i;
	n = n * ( 1.5F - ( nhalf * n * n ) );

	return (int32_t) (1/n);
}

int32_t fastCos(int32_t hue) {
	int8_t sign = 1;
	if (hue > 90) {
		hue -= 180;
		sign = -1;
	}
	if (hue < -90) {
		hue += 180;
		sign = -1;
	}
	int32_t result = 1000 - hue * hue * 126924 / 1000000; // hue huerino
	return sign * result;
}
