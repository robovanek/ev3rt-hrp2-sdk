#include "header.h"

#pragma once

// arbitrary limit
#define MAX_BALLS 20

// ball colors

typedef enum {
	BALL_NONE,
	BALL_UNCERTAIN,
	BALL_BLUE,
	BALL_GREEN,
	BALL_RED,
	BALL_YELLOW
} ballcolor;

// init
extern void sen_init(void);

// "dumb sensing"
extern bool    sen_front(void);

#ifndef REG_AVOID_SIDETOUCH
extern bool    sen_side (void);
#else
extern bool    sen_backtouch(void);
#endif


#ifndef REG_AVOID_SONIC
extern int32_t sen_back (void);
#endif

#ifdef REG_AVAIL_GYRO
extern int16_t sen_gyro(void);
extern void    sen_gyro_calibrate(void);
#endif


// smart ball sensing
extern ballcolor sen_ball_pop(void);
extern int8_t    sen_ball_avail(void);
extern void      sen_loop_enter(void);
