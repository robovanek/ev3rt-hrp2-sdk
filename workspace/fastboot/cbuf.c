#include "header.h"

//
// pointer wrapping
//

volatile cbuf_elem* nextread(cbuf volatile * pBuf) {
	volatile cbuf_elem* pNext = pBuf->pRead + 1;
	if (pNext == pBuf->pEnd)
		return   pBuf->pStart;
	return pNext;
}

volatile cbuf_elem* nextwrite(cbuf volatile * pBuf) {
	volatile cbuf_elem* pNext = pBuf->pWrite + 1;
	if (pNext == pBuf->pEnd)
		return   pBuf->pStart;
	return pNext;
}

//
// check free space
//

size_t cbuf_avail_r(cbuf volatile * pBuf) {
	size_t len = pBuf->pEnd - pBuf->pStart;
	return (pBuf->pWrite - pBuf->pRead + len) % len;
}

size_t cbuf_avail_w(cbuf volatile * pBuf) {
	size_t len = pBuf->pEnd - pBuf->pStart;
	return (pBuf->pRead - pBuf->pWrite - 1 + len) % len;
}

//
// access current position
//

volatile cbuf_elem cbuf_get(cbuf volatile * pBuf) {
	return *pBuf->pRead;
}

void cbuf_set(cbuf volatile * pBuf, cbuf_elem value) {
	*pBuf->pWrite = value;
}

//
// advance current position
//

bool cbuf_push(cbuf volatile * pBuf) {
	if (cbuf_avail_w(pBuf)) {
		pBuf->pWrite = nextwrite(pBuf);
		return true;
	}
	return false;
}

bool cbuf_pop(cbuf volatile * pBuf) {
	if (cbuf_avail_r(pBuf)) {
		pBuf->pRead = nextread(pBuf);
		return true;
	}
	return false;
}


//
// initialize buffer
//

void cbuf_init(cbuf volatile * pBuf, cbuf_elem volatile * pArray, size_t capacity) {
	pBuf->pStart      = pArray;
	pBuf->pEnd        = pArray + capacity;
	pBuf->pWrite      = pArray;
	pBuf->pRead       = pArray;
}
