#pragma once

#include "header.h"

// move types

typedef enum {
	WALL_LONG  = 0,
	WALL_SHORT = 2,
	WALL_MASK = 0x2,
} walltype;

typedef enum {
	WM_TOWALL  = 0,
	WM_TOSHOOT = 1,
	WM_MASK = 0x1,
} wallmove;

typedef enum {
	SM_FORWARD  = 0,
	SM_BACKWARD = 1,
	SM_MASK = 0x1,
} shootmove;

// moves

extern void mov_init(void);

extern void mov_wall(uint8_t which);
extern void mov_shoot(uint8_t which);
extern void mov_turn(bool standard);

extern void mov_backalign(void);
extern void mov_unbackalign(void);
extern void mov_unfwdalign(void);

extern void mov_superman(void);

extern int16_t headingNow();
extern int16_t headingRight();
