#pragma once

#include "header.h"

// handler prototypes

typedef int16_t (*measurefn)(void);
typedef int16_t (*setpointfn)(void);

typedef int32_t (*lmeasurefn)(void);
typedef bool    (*stopfn)(intptr_t params);
typedef void    (*initfn)(void);

// data structures

// dynamic regulator callbacks
typedef struct {
	measurefn  doMeasurement;
	setpointfn getSetpoint;
} RegHooks;

// static regulator parameters
typedef struct {
	int32_t LimitI;
	int16_t Kp;
	int16_t Ki;
	int16_t Kd;
	int16_t Kdd;
	int16_t Divisor;
} RegParam;

// configuration of speed generator
typedef struct {
	// Function hooks
	lmeasurefn doMeasurement;
	stopfn     shallWeStop;
	intptr_t   arg;

	// Start slowdown distance
	int32_t decelStartDistance;

	// Acceleration
	int16_t startAcceleration;
	int16_t stopDeceleration;

	// Speed bounding
	int8_t upperSpeed;
	int8_t lowerSpeed;
} RegSpeedGen;

// working data
typedef struct {
	// dynamic callback
	lmeasurefn doMeasurement;
	stopfn     shallWeStop;
	intptr_t   arg;

	// ramp geometry
	int32_t accelRampEnd;
	int32_t decelRampStart;

	// acceleration details
	int32_t absPart;
	int32_t linPartDecel;
	int32_t linPartAccel;

	// speed bounds
	int8_t upperSpeed;
	int8_t lowerSpeed;
} RegSpeedGenCached;

// regulators

extern void   reg_step(const RegParam* pParams, const RegHooks* pHooks, const RegSpeedGen *pSpeed);
extern void   reg_straight(const RegSpeedGen *pSpeed);

