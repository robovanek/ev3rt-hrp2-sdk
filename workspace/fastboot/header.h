#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include "ev3api.h"

#include "config.h"
#include "app.h"
#include "cbuf.h"
#include "gfx.h"
#include "mainloop.h"
#include "motors.h"
#include "move.h"
#include "os.h"
#include "reg.h"
#include "sensors.h"
#include "sound.h"
