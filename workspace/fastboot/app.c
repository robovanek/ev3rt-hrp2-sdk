#include "header.h"

//
// main entry point
//

void main_task(intptr_t arg) {
	gfx_init();
	snd_init();
	sen_init();
	mot_init();
	mov_init();
	if (gfx_start()) {
		loop_enter();
	}
	os_exit();
}

void color_task(intptr_t arg) {
	sen_loop_enter();
}
