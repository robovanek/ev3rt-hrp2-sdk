#pragma once

#include "header.h"

//
// shooting target
//

typedef enum {
	RING_GREEN  = 0,
	RING_YELLOW = 1,
	RING_RED    = 2,
	RING_BLUE   = 3
} ringpos;

// Init

extern void mot_init(void);

// Custom regulations

extern void mot_freestart(void);
extern void mot_freespeed(int8_t speedR, int8_t speedL);
extern void mot_stop(void);
extern void mot_float(void);
extern void mot_direction(bool fwd);
extern bool mot_forward(void);

// Speed

extern int8_t mot_lspeed(void);
extern int8_t mot_rspeed(void);


// Position

extern int32_t mot_lcount(void);
extern int32_t mot_rcount(void);
extern void mot_resetcount(void);

// Builtin regulation

extern void mot_travelsteps(int32_t steps, int16_t ratio, int8_t speed);
extern void mot_traveltime(int32_t time, int8_t power);

// Waiting

extern void mot_waitfor(void);

//
// Shooting
//

extern void mot_ball_begin(ringpos initial);
extern void mot_ball_goto(ringpos new);
extern void mot_ball_put(void);

