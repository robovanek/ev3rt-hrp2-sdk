#include "header.h"

//
// Local functions
//

#ifndef REG_AVOID_SIDETOUCH
// steering regulator
static int16_t getSideTouch(void);
static int16_t getSetpoint(void);
#endif

// logging
static void logwall(uint8_t which);
static void logshoot(uint8_t which);

//
// HueHueHue enhancement
//
static void    getDistance_Init_Tacho();
static int32_t getDistance_Tacho     ();

#ifndef REG_AVOID_SONIC
static void    getDistance_Init_Sonic();
static int32_t getDistance_Sonic     ();
static int32_t getDistance_Backsonic ();
#endif

static void getStop_Init_Trip();
static bool getStop_TripProcess(bool reality, RELTIM delay);

static bool getStop_FrontTouch(intptr_t unused);
static bool getStop_Tacho(intptr_t limit);
static bool getStop_TachoBackTrip(intptr_t limit);
#ifdef REG_REG_AVAIL_BACKTOUCH
static bool getStop_BackTouch (intptr_t unused);
#endif

#ifndef REG_AVOID_SONIC
static bool getStop_Sonic(intptr_t limit);
static bool getStop_Backsonic(intptr_t limit);
#endif

static void sensorInit();

static void    headingSet();


//
// Local state tracking variables
//

static struct tachostate_t {
	int32_t tachoL0;
	int32_t tachoR0;
} tachostate;

#ifndef REG_AVOID_SONIC
static struct sonicstate_t {
	int32_t sonicLast;
	int16_t sonic0;
} sonicstate;
#endif

static struct tripstate_t {
	SYSTIM timestamp;
	bool tripped;
} tripstate;

#ifdef REG_AVAIL_GYRO
static struct gyrostate_t{
	int16_t expected;
} gyrostate;
#endif

//
// Local data tables
//

#ifndef REG_AVOID_SIDETOUCH
static RegParam wallPd = {
	.Kp      = REG_WALL_KP,
	.Ki      = REG_WALL_KI,
	.Kd      = REG_WALL_KD,
	.Kdd     = REG_WALL_KDD,
	.LimitI  = REG_WALL_ILIM,
	.Divisor = REG_WALL_DIV
};

static RegHooks wallHooks = {
	.doMeasurement = &getSideTouch,
	.getSetpoint   = &getSetpoint
};
#endif

static RegSpeedGen wallCurves[] = {
	// corner to long wall
	[WM_TOWALL + WALL_LONG] = {
		.doMeasurement      = &getDistance_Tacho,
		.shallWeStop        = &getStop_FrontTouch,
		.arg                = 0,
		
		.decelStartDistance = REG_WALLLONG_TOWALL_DECELSTART,
		.startAcceleration  = REG_WALLLONG_TOWALL_ACCEL,
		.stopDeceleration   = REG_WALLLONG_TOWALL_DECEL,
		.upperSpeed         = REG_WALLLONG_TOWALL_UPPER,
		.lowerSpeed         = REG_WALLLONG_TOWALL_LOWER,
	},
	// corner to short wall
	[WM_TOWALL + WALL_SHORT] = {
		.doMeasurement       = &getDistance_Tacho,
		.shallWeStop         = &getStop_FrontTouch,
		.arg                 = 0,
		
		.decelStartDistance  = REG_WALLSHORT_TOWALL_DECELSTART,
		.startAcceleration   = REG_WALLSHORT_TOWALL_ACCEL,
		.stopDeceleration    = REG_WALLSHORT_TOWALL_DECEL,
		.upperSpeed          = REG_WALLSHORT_TOWALL_UPPER,
		.lowerSpeed          = REG_WALLSHORT_TOWALL_LOWER,
	},
	// shooting near long wall
	[WM_TOSHOOT + WALL_LONG] = {
#ifndef REG_AVOID_SONIC
		.doMeasurement      = &getDistance_Sonic,
		.shallWeStop        = &getStop_Sonic,
#else
		.doMeasurement      = &getDistance_Tacho,
		.shallWeStop        = &getStop_Tacho,
#endif
		.arg                = REG_WALLLONG_TOSHOOT_STOP,
		.decelStartDistance = REG_WALLLONG_TOSHOOT_DECELSTART,
		.startAcceleration  = REG_WALLLONG_TOSHOOT_ACCEL,
		.stopDeceleration   = REG_WALLLONG_TOSHOOT_DECEL,
		.upperSpeed         = REG_WALLLONG_TOSHOOT_UPPER,
		.lowerSpeed         = REG_WALLLONG_TOSHOOT_LOWER,
	},
	// shooting near short wall
	[WM_TOSHOOT + WALL_SHORT] = {
#ifndef REG_AVOID_SONIC
		.doMeasurement      = &getDistance_Sonic,
		.shallWeStop        = &getStop_Sonic,
#else
		.doMeasurement      = &getDistance_Tacho,
		.shallWeStop        = &getStop_Tacho,
#endif
		.arg                = REG_WALLSHORT_TOSHOOT_STOP,
		.decelStartDistance = REG_WALLSHORT_TOSHOOT_DECELSTART,
		.startAcceleration  = REG_WALLSHORT_TOSHOOT_ACCEL,
		.stopDeceleration   = REG_WALLSHORT_TOSHOOT_DECEL,
		.upperSpeed         = REG_WALLSHORT_TOSHOOT_UPPER,
		.lowerSpeed         = REG_WALLSHORT_TOSHOOT_LOWER,
	},
};


static RegSpeedGen shootCurves[] = {
	[WALL_LONG + SM_FORWARD] = {
#ifndef REG_AVOID_SONIC
		.doMeasurement      = &getDistance_Sonic,
		.shallWeStop        = &getStop_Sonic,
#else
		.doMeasurement      = &getDistance_Tacho,
		.shallWeStop        = &getStop_Tacho,
#endif
		.arg                = REG_SHOOTFWD_LONG_STOP,
		.decelStartDistance = REG_SHOOTFWD_LONG_DECELSTART,
		.startAcceleration  = REG_SHOOTFWD_LONG_ACCEL,
		.stopDeceleration   = REG_SHOOTFWD_LONG_DECEL,
		.upperSpeed         = REG_SHOOTFWD_LONG_UPPER,
		.lowerSpeed         = REG_SHOOTFWD_LONG_LOWER,
	},
	[WALL_SHORT  + SM_FORWARD] = {
#ifndef REG_AVOID_SONIC
		.doMeasurement      = &getDistance_Sonic,
		.shallWeStop        = &getStop_Sonic,
#else
		.doMeasurement      = &getDistance_Tacho,
		.shallWeStop        = &getStop_Tacho,
#endif
		.arg                = REG_SHOOTFWD_SHORT_STOP,
		.decelStartDistance = REG_SHOOTFWD_SHORT_DECELSTART,
		.startAcceleration  = REG_SHOOTFWD_SHORT_ACCEL,
		.stopDeceleration   = REG_SHOOTFWD_SHORT_DECEL,
		.upperSpeed         = REG_SHOOTFWD_SHORT_UPPER,
		.lowerSpeed         = REG_SHOOTFWD_SHORT_LOWER,
	},
	[WALL_LONG  + SM_BACKWARD] = {
#ifndef REG_AVOID_SONIC
		.doMeasurement      = &getDistance_Backsonic,
#else
		.doMeasurement      = &getDistance_Tacho,
#endif
#ifdef REG_AVAIL_BACKTOUCH
		.shallWeStop        = &getStop_BackTouch,
#else
	#ifndef REG_AVOID_SONIC
		.shallWeStop        = &getStop_Backsonic,
	#else
		.shallWeStop        = &getStop_TachoBackTrip,
	#endif
#endif
		.arg                = REG_SHOOTBWD_LONG_TRIP,
		.decelStartDistance = REG_SHOOTBWD_LONG_DECELSTART,
		.startAcceleration  = REG_SHOOTBWD_LONG_ACCEL,
		.stopDeceleration   = REG_SHOOTBWD_LONG_DECEL,
		.upperSpeed         = REG_SHOOTBWD_LONG_UPPER,
		.lowerSpeed         = REG_SHOOTBWD_LONG_LOWER,
	},
	[WALL_SHORT + SM_BACKWARD] = {
#ifndef REG_AVOID_SONIC
		.doMeasurement      = &getDistance_Backsonic,
#else
		.doMeasurement      = &getDistance_Tacho,
#endif
#ifdef REG_AVAIL_BACKTOUCH
		.shallWeStop        = &getStop_BackTouch,
#else
	#ifndef REG_AVOID_SONIC
		.shallWeStop        = &getStop_Backsonic,
	#else
		.shallWeStop        = &getStop_TachoBackTrip,
	#endif
#endif
		.arg                = REG_SHOOTBWD_SHORT_TRIP,
		.decelStartDistance = REG_SHOOTBWD_SHORT_DECELSTART,
		.startAcceleration  = REG_SHOOTBWD_SHORT_ACCEL,
		.stopDeceleration   = REG_SHOOTBWD_SHORT_DECEL,
		.upperSpeed         = REG_SHOOTBWD_SHORT_UPPER,
		.lowerSpeed         = REG_SHOOTBWD_SHORT_LOWER,
	},
};

////////////////////////
// TRAVELLED DISTANCE //
////////////////////////

void sensorInit() {
	getDistance_Init_Tacho();
#ifndef REG_AVOID_SONIC
	getDistance_Init_Sonic();
#endif
	getStop_Init_Trip();
}

void    getDistance_Init_Tacho() {
	tachostate.tachoL0 = mot_lcount();
	tachostate.tachoR0 = mot_rcount();
	getStop_Init_Trip();
}

int32_t getDistance_Tacho    () {
	int32_t tachoL  = mot_lcount();
	int32_t tachoR  = mot_rcount();
	int32_t dTachoL = tachoL - tachostate.tachoL0;
	int32_t dTachoR = tachoR - tachostate.tachoR0;
	int32_t avg     = (dTachoL + dTachoR) / 2;
	if (avg < 0)
		avg = -avg; // we do not want absolute position
	return avg;
}

#ifndef REG_AVOID_SONIC
void    getDistance_Init_Sonic() {
	int32_t now = sen_back();
	if (now == 255)
		now = US_SPACE;
	
	sonicstate.sonic0         = now;
	sonicstate.sonicLast      = now;
	getStop_Init_Trip();
}

int32_t getDistance_Sonic    () {
	int32_t now = sen_back();
	if (now != 255) {
		sonicstate.sonicLast = (US_FILTER_NOW  * now  +
		                        US_FILTER_PAST * sonicstate.sonicLast) / US_FILTER_DEN;
	}
	return sonicstate.sonicLast;
}

int32_t getDistance_Backsonic() {
	int32_t distance = getDistance_Sonic();
	int32_t delta = -(distance - sonicstate.sonic0);
	return delta > 0 ? delta : 0;
}
#endif

////////////////
// STOP STATE //
////////////////

bool getStop_FrontTouch(intptr_t unused) {
	return sen_front();
}

#ifdef REG_AVAIL_BACKTOUCH
bool getStop_BackTouch (intptr_t unused) {
	return getStop_TripProcess(sen_backtouch(), REG_BACKALIGN_DELAY);
}
#endif

bool getStop_Tacho(intptr_t limit) {
	return getDistance_Tacho() > limit;
}

bool getStop_TachoBackTrip(intptr_t limit) {
	int32_t dist = getDistance_Tacho();
	return getStop_TripProcess(dist > limit, REG_SHOOTBWD_TRIPTIME);
}

#ifndef REG_AVOID_SONIC
bool getStop_Sonic(intptr_t limit) {
	return getDistance_Sonic() > limit;
}

bool getStop_Backsonic(intptr_t limit) {
	int32_t dist = getDistance_Sonic();
	
	return getStop_TripProcess(dist < limit, REG_SHOOTBWD_TRIPTIME);
}
#endif

////////////////
// TIME LIMIT //
////////////////

void getStop_Init_Trip() {
	tripstate.tripped   = false;
	tripstate.timestamp = 0;
}

bool getStop_TripProcess(bool reality, RELTIM delay) {
	SYSTIM now;
	get_tim(&now);
	
	if (tripstate.tripped) {
		return (now - tripstate.timestamp) > delay;
	} else {
		if (reality) {
			tripstate.tripped   = true;
			tripstate.timestamp = now;
		}
		return false;
	}
}

//
// steering regulator
//

#ifndef REG_AVOID_SIDETOUCH
int16_t getSideTouch(void) {
	if (sen_side()) {
		return REG_PV_PRESSED;
	} else {
		return REG_PV_RELEASED;
	}
}

int16_t getSetpoint(void) {
	return REG_SETPOINT;
}
#endif

/////////////
// HEADING //
/////////////

void headingSet() {
#ifdef REG_AVAIL_GYRO
	gyrostate.expected = sen_gyro();
#endif
}

int16_t headingError() {
#ifdef REG_AVAIL_GYRO
	return gyrostate.expected - sen_gyro();
#else
	return 0;
#endif
}

int16_t headingNow() {
#ifdef REG_AVAIL_GYRO
	return sen_gyro();
#else
	return 0;
#endif
}

int16_t headingRight() {
#ifdef REG_AVAIL_GYRO
	return gyrostate.expected;
#else
	return 0;
#endif
}

//
// Move drivers
//

// wall follower
void mov_wall(uint8_t which) {
	logwall(which);
	
	sensorInit();
#ifndef REG_AVOID_SIDETOUCH
	reg_step(&wallPd, &wallHooks, &wallCurves[which]);
#else
	reg_straight(&wallCurves[which]);
#endif
}

// free move
void mov_shoot(uint8_t which) {
	logshoot(which);
	
	if ((which & SM_MASK) == SM_BACKWARD)
		mot_direction(false);

	sensorInit();
	reg_straight(&shootCurves[which]);

	if ((which & SM_MASK) == SM_BACKWARD)
		mot_direction(true);
}

// wall press
void mov_backalign(void) {
	gfx_msgbox("BACKALIGN");

	mot_freespeed(REG_BACKALIGN_SPEED, REG_BACKALIGN_SPEED);
	mot_freestart();
#ifdef REG_AVAIL_BACKTOUCH
	while (!sen_backtouch()) {
		dly_tsk(REG_DELAY);
	}
#endif
	dly_tsk(REG_BACKALIGN_DELAY);
	mot_stop();
	//mot_traveltime(REG_BACKALIGN_DELAY, REG_BACKALIGN_SPEED);
	//mot_waitfor();
}

void mov_unbackalign(void) {
	gfx_msgbox("UNBACKALIGN");
	
	mot_travelsteps(REG_POSTALIGN_STEPS, 0, REG_POSTALIGN_SPEED);
	mot_waitfor();
}

void mov_unfwdalign(void) {
	gfx_msgbox("UNFWDALIGN");
	
	dly_tsk(REG_POSTHIT_PREDELAY);
	mot_travelsteps(REG_POSTHIT_STEPS, 0, REG_POSTHIT_SPEED);
	mot_waitfor();
	dly_tsk(REG_POSTHIT_POSTDELAY);
}

// free turn
void mov_turn(bool standard) {
	gfx_msgbox("TURN");

#ifndef REG_AVAIL_GYRO
	mot_travelsteps(REG_TURN_STEPS, standard ? REG_TURN_RATIO : -REG_TURN_RATIO, REG_TURN_SPEED);
	mot_waitfor();
#else
	headingSet();

	const int16_t angle = standard ? +90 : -90;
	int16_t change;
	int16_t fix = (angle - 0);
	int16_t sigfix = standard ? +1 : -1;
	int16_t absfix = fix * sigfix;

	bool first = true;
	do {
		if (first) {
			first = false;
		} else {
			mov_unbackalign();
		}
		mot_travelsteps(absfix * REG_TURN_DPD_NUM / REG_TURN_DPD_DEN, sigfix * REG_TURN_RATIO, REG_TURN_SPEED);
		mot_waitfor();
		
		change = headingNow() - headingRight();
		fix = (angle - change);
		sigfix = fix >= 0 ? +1 : -1;
		absfix = fix * sigfix;
	} while (absfix > REG_GYRO_MAXERROR);
#endif
}

void mov_superman(void) {
	dly_tsk(500);
	mot_travelsteps(281*4, REG_TURN_RATIO, REG_SUPERSPEED);
	mot_waitfor();
	dly_tsk(500);
}

void mov_init(void) {
	headingSet();
}

void logwall(uint8_t which) {
	char *msg = "ERROR";
	switch (which) {
		case WALL_LONG  + WM_TOWALL:
			msg = "LONG-WM_WALL";
			break;
		case WALL_LONG  + WM_TOSHOOT:
			msg = "LONG-WM_SHOOT";
			break;
		case WALL_SHORT + WM_TOWALL:
			msg = "SHORT-WM_WALL";
			break;
		case WALL_SHORT + WM_TOSHOOT:
			msg = "SHORT-WM_SHOOT";
			break;
	}
	gfx_msgbox(msg);
}
void logshoot(uint8_t which) {
	char *msg = "ERROR";
	switch (which) {
		case WALL_LONG  + SM_FORWARD:
			msg = "LONG-SM_FORWARD";
			break;
		case WALL_LONG  + SM_BACKWARD:
			msg = "LONG-SM_BACKWARD";
			break;
		case WALL_SHORT + SM_FORWARD:
			msg = "SHORT-SM_FORWARD";
			break;
		case WALL_SHORT + SM_BACKWARD:
			msg = "SHORT-SM_BACKWARD";
			break;
	}
	gfx_msgbox(msg);
}
