#include "header.h"

//
// typedefs
//

typedef struct {
	uint16_t chroma;
	uint16_t hue;
} colorinfo;

typedef enum {
	STATE_NONE,
	STATE_TRANSITION,
	STATE_COLOR
} colorstate;

//
// function declarations
//

static uint16_t satu10(uint16_t val);
static uint16_t lincurve(uint16_t val, uint16_t an, uint16_t ad, uint16_t b);
static uint16_t rgbmin(void);
static uint16_t rgbmax(void);
static void rgbsample(void);
static void rgbcurve(void);
static void rgb2hc(void);
static ballcolor hc2ball(void);
static ballcolor hccheck(uint16_t hue_min, uint16_t hue_max);
static ballcolor ballsample(void);
static bool sen_ball_push(ballcolor which);
#ifdef BALL_DEBUG
static void printlog(ballcolor which);
#endif

//
// local data
//
static rgb_raw_t rgb;
static colorinfo hc;
static colorstate stm;
static cbuf_elem volatile bq_arr[MAX_BALLS];
static volatile cbuf bq;
static volatile uint8_t bq_avail; 


//
// Ball identification
//


void sen_loop_enter(void) {
	ballcolor ball;
	while (true) {
		ball = ballsample();

		// state machine
		switch (stm) {
			// only darkness
			case STATE_NONE:
				// what ball?
				switch (ball) {
					case BALL_NONE:
						// noop, already here
						break;
					default:
						stm = STATE_TRANSITION;
						break;
				}
				break;
			// something measured
			case STATE_TRANSITION:
				// what ball?
				switch (ball) {
					case BALL_NONE:
						stm = STATE_NONE;
						break;
					case BALL_UNCERTAIN:
						// noop, already here
						break;
					default:
						sen_ball_push(ball);
						stm = STATE_COLOR;
						break;
				}
				break;
			// clearly measured color
			case STATE_COLOR:
				// what ball?
				switch (ball) {
					case BALL_NONE:
						stm = STATE_NONE;
						break;
					default:
						// noop, schmitt triggering
						break;
				}
				break;
		}

		dly_tsk(BALL_DELAY);
	}
}

ballcolor sen_ball_pop(void) {
	if (bq_avail) {
		--bq_avail;
		cbuf_pop(&bq);
		return cbuf_get(&bq);
	} else {
		return BALL_NONE;
	}
}

bool sen_ball_push(ballcolor which) {
	if (cbuf_avail_w(&bq)) {
		cbuf_push(&bq);
		cbuf_set(&bq, which);
		++bq_avail;
		
		return true;
	} else {
		return false;
	}
}

int8_t    sen_ball_avail(void) {
	return cbuf_avail_r(&bq);
}

// init code

void sen_init(void) {
	// initialize async balls
	stm = STATE_NONE;
	cbuf_init(&bq, bq_arr, sizeof(bq_arr) / sizeof(bq_arr[0]) );
	bq_avail = 0;
	
	// configure ports
	ev3_sensor_config(SEN_COLOR,       COLOR_SENSOR);
	ev3_sensor_config(SEN_TOUCH_FRONT, TOUCH_SENSOR);
	
#ifndef REG_AVOID_SONIC
	ev3_sensor_config(SEN_SONIC,       ULTRASONIC_SENSOR);
#endif
	
#ifndef REG_AVOID_SIDETOUCH
	ev3_sensor_config(SEN_TOUCH_SIDE,  TOUCH_SENSOR);
#endif

#ifdef REG_AVAIL_BACKTOUCH
	ev3_sensor_config(SEN_TOUCH_BACK,  TOUCH_SENSOR);
#endif

#ifdef REG_AVAIL_GYRO
	ev3_sensor_config(SEN_GYRO, GYRO_SENSOR);
#endif

	dly_tsk(SEN_SETUP_DELAY1);
	
	// configure sensors
	ballsample();
	sen_front();
#ifndef REG_AVOID_SIDETOUCH
	sen_side();
#endif
#ifdef REG_AVAIL_BACKTOUCH
	sen_backtouch();
#endif

#ifndef REG_AVOID_SONIC
	sen_back();
#endif

#ifdef REG_AVAIL_GYRO
	sen_gyro_calibrate();
	sen_gyro();
#endif

	// finish async thread init
	dly_tsk(SEN_SETUP_DELAY2);
	act_tsk(COLOR_TASK);

}

// self-reliant sensors

bool sen_front(void) {
	return ev3_touch_sensor_is_pressed(SEN_TOUCH_FRONT);
}

#ifndef REG_AVOID_SIDETOUCH
bool sen_side(void) {
	return ev3_touch_sensor_is_pressed(SEN_TOUCH_SIDE);
}
#endif

#ifdef REG_AVAIL_BACKTOUCH
bool sen_backtouch(void) {
	return ev3_touch_sensor_is_pressed(SEN_TOUCH_BACK);
}
#endif

#ifndef REG_AVOID_SONIC
int32_t sen_back(void) {
	return ev3_ultrasonic_sensor_get_distance(SEN_SONIC);
}
#endif


#ifdef REG_AVAIL_GYRO
int16_t sen_gyro(void) {
	return ev3_gyro_sensor_get_angle(SEN_GYRO);
}

void    sen_gyro_calibrate(void) {
	ev3_gyro_sensor_get_rate(SEN_GYRO);
	ev3_gyro_sensor_get_angle(SEN_GYRO);
}
#endif

//
// color analysis
//

ballcolor ballsample(void) {
	// sample RGB values from color sensor
	rgbsample();
	// perform linear adjustment (y = ax + b)
	rgbcurve();
	// convert RGB to HC (Hue-Chroma)
	rgb2hc();
	// identify the ball color by checking HC
	ballcolor color = hc2ball();
#ifdef BALL_DEBUG
	printlog(color);
#endif
	return color;
}


//
// HC analysis
//

ballcolor hc2ball(void) {
	if (hc.chroma < BALL_ALL_CHROMA1) {
		return BALL_NONE;

	} else if (hc.chroma < BALL_ALL_CHROMA2) {
		return BALL_UNCERTAIN;

	} else if (hccheck(BALL_RED_HUE1,    BALL_RED_HUE2))   {
		return BALL_RED;
		
	} else if (hccheck(BALL_GREEN_HUE1,  BALL_GREEN_HUE2)) {
		return BALL_GREEN;

	} else if (hccheck(BALL_BLUE_HUE1,   BALL_BLUE_HUE2))   {
		return BALL_BLUE;

	} else if (hccheck(BALL_YELLOW_HUE1, BALL_YELLOW_HUE2)) {
		return BALL_YELLOW;
	}

	return BALL_UNCERTAIN;
}

ballcolor hccheck(uint16_t hue_min,
                  uint16_t hue_max) {
	if (hue_min < hue_max) {
		return hue_min <= hc.hue && hc.hue <= hue_max;
	} else {
		return hue_min <= hc.hue || hc.hue <= hue_max;
	}
}

//
// HC sampling
//

void rgbsample(void) {
	ev3_color_sensor_get_rgb_raw(SEN_COLOR, &rgb);
}

void rgbcurve(void) {
	rgb.r = lincurve(rgb.r, BALLCOR_RED_AN,   BALLCOR_RED_AD,   BALLCOR_RED_B);
	rgb.g = lincurve(rgb.g, BALLCOR_GREEN_AN, BALLCOR_GREEN_AD, BALLCOR_GREEN_B);
	rgb.b = lincurve(rgb.b, BALLCOR_BLUE_AN,  BALLCOR_BLUE_AD,  BALLCOR_BLUE_B);
}

void rgb2hc(void) {
	uint16_t max = rgbmax();
	uint16_t min = rgbmin();
	hc.chroma = max - min;


	if (hc.chroma == 0) {
		hc.hue = 0;

	} else  if (max == rgb.r) {
		hc.hue = 60 * (rgb.g - rgb.b) / hc.chroma % 360;

	} else if (max == rgb.g) {
		hc.hue = 60 * (rgb.b - rgb.r) / hc.chroma + 120;

	} else {// max == rgb.b
		hc.hue = 60 * (rgb.r - rgb.g) / hc.chroma + 240;
	}
}

uint16_t rgbmin(void) {
	uint16_t min = rgb.r;
	if (min > rgb.g)
		min = rgb.g;
	if (min > rgb.b)
		min = rgb.b;
	return min;
}

uint16_t rgbmax(void) {
	uint16_t max = rgb.r;
	if (max < rgb.g)
		max = rgb.g;
	if (max < rgb.b)
		max = rgb.b;
	return max;
}

//
// generic math functions
//

uint16_t lincurve(uint16_t color, uint16_t a_num, uint16_t a_den, uint16_t b) {
	return satu10(a_num * color / a_den + b);
}

uint16_t satu10(uint16_t num) {
	if (num & 0x8000) {
		return 0;
	} else if (num >= 0x03FF) {
		return 0x03FF;
	} else {
		return num;
	}
}

#ifdef BALL_DEBUG
//
// debug module
//

void int2arr(uint16_t val, char *arr) {
	arr[4] = ',';
	for (int8_t i = 3; i >= 0; --i) {
		if (val != 0 || i == 3) {
			char c = '0' + (val % 10);
			arr[i] = c;
			val = val / 10;
		} else {
			arr[i] = ' ';
		}
	}
}

void ball2arr(ballcolor val, char *arr) {
	char *str;
	switch (val) {
		default:
		case BALL_NONE:
			str = "none";
			break;
		case BALL_UNCERTAIN:
			str = "unknown";
			break;
		case BALL_BLUE:
			str = "blue";
			break;
		case BALL_GREEN:
			str = "green";
			break;
		case BALL_RED:
			str = "red";
			break;
		case BALL_YELLOW:
			str = "yellow";
			break;
	}
	strcpy(arr, str);
}

static char msg[5 * 4 + 7 + 5 + 1];

static char *r = msg + 0;
static char *g = msg + 5;
static char *b = msg + 10;
static char *h = msg + 15;
static char *c = msg + 20;
static char *i = msg + 25;

void printlog(ballcolor which) {
	int2arr(rgb.r, r);
	int2arr(rgb.g, g);
	int2arr(rgb.b, b);
	int2arr(hc.hue, h);
	int2arr(hc.chroma, c);
	ball2arr(which, i);
	syslog(LOG_NOTICE, msg);
}
#endif
