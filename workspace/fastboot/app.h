#pragma once

#include <stdint.h>


#define MAIN_STACK_SIZE  4096
#define MAIN_PRIORITY  (TMIN_APP_TPRI + 1)
extern void main_task (intptr_t arg);

#define COLOR_STACK_SIZE 4096
#define COLOR_PRIORITY (TMIN_APP_TPRI + 0)
extern void color_task(intptr_t arg);
