#pragma once

//
// Graphics subsystem
//

//#define GFX_TITLE "FastBoot      FTW"
#define GFX_TITLE "FastBoot  .... mV"


//
// Sound subsystem
//

#define SND_VOLUME 10


#define REG_AVOID_SONIC
//#define REG_AVOID_SIDETOUCH
//#define REG_AVAIL_BACKTOUCH
#define REG_AVAIL_GYRO
//#define CORNER_BACKALIGN
//#define REG_SYNCED_FORWARD

//
// Sensor subsystem
//

// sensor ports
#define S(x) (x - 1)
#define SEN_COLOR        S(4)
#define SEN_SONIC        S(3)
#define SEN_GYRO         S(3)
#define SEN_TOUCH_FRONT  S(2)
#define SEN_TOUCH_SIDE   S(1)
#define SEN_TOUCH_BACK   S(1)

#undef BALL_DEBUG

// linear correction of Red component
#define BALLCOR_RED_AN   1024
#define BALLCOR_RED_AD   240
#define BALLCOR_RED_B    -9

// linear correction of Green component
#define BALLCOR_GREEN_AN 1024
#define BALLCOR_GREEN_AD 230
#define BALLCOR_GREEN_B  -9

// linear correction of Blue component
#define BALLCOR_BLUE_AN  1024
#define BALLCOR_BLUE_AD  140
#define BALLCOR_BLUE_B   -15

// ball detection chroma threshold
#define BALL_ALL_CHROMA1 50
// ball identification chroma threshold
#define BALL_ALL_CHROMA2 150

// min-max hue for red ball
#define BALL_RED_HUE1    (5 - 25 + 360)
#define BALL_RED_HUE2    (5 + 25)
// min-max hue for green ball
#define BALL_GREEN_HUE1  (110 - 25)
#define BALL_GREEN_HUE2  (110 + 25)
// min-max hue for blue ball
#define BALL_BLUE_HUE1   (195 - 25)
#define BALL_BLUE_HUE2   (195 + 25)
// min-max hue for yellow ball
#define BALL_YELLOW_HUE1 (55 - 25)
#define BALL_YELLOW_HUE2 (55 + 25)

// color loop timing
#define BALL_DELAY 5
#define SEN_SETUP_DELAY1 0
#define SEN_SETUP_DELAY2 100

#define US_FILTER_NOW    25
#define US_FILTER_PAST   75	
#define US_FILTER_DEN    100


//
// Motor subsystem
//

// motor ports
#define M(x) (1 << (x - 1))
#define MOT_LEFT         M(1)
#define MOT_RIGHT        M(4)
#define MOT_SHOOT        M(3)

////
// shoter

// motor direction
#define MOT_SHOOTER_POLARITY   1

// delay after shooting
#define MOT_SHOOT_DELAY      1000

// params
#define MOT_SHOOT_INOUT   50
#define MOT_SHOOT_DEGREES 300
#define MOT_SHOOT_SPEED   75

#define MOT_TURN_DEGPERRING 33
#define MOT_TURN_SPEED 10

//
// regulated moves
//

// loop dt
#define REG_DELAY 10
#define REG_DELAY_SYNC 10

// touch sensor PID parameters
#define REG_WALL_KP      40 /* touch press */
#define REG_WALL_KI      1 /* integral    */
#define REG_WALL_KD      0  /* heading     */
#define REG_WALL_KDD     0  /* wheel ratio */
#define REG_WALL_DIV     100
#define REG_WALL_ILIM    150

#define REG_PV_PRESSED   10
#define REG_PV_RELEASED  -10
#define REG_SETPOINT     0

#define REG_GYRO_MAXERROR 20
#define REG_TURN_DPD_NUM  270
#define REG_TURN_DPD_DEN   90

// shortcuts for speed parameters
#define ACCEL_TACHO         10  /* ??? */
#define DECEL_TACHO         10  /* ??? */
#define ACCEL_SONIC         50  /* ??? */
#define DECEL_SONIC         50  /* ??? */
#define MAXSPEED            50  /* pct */
#define MINSPEED            15  /* pct */
#define DECELLEN_TACHO      220 /* deg */
#define DECELLEN_SONIC      40  /* cm */
#define DECELLEN_SONIC_WALL 20  /* cm */
#define US_SPACE            4   /* cm */
#define TACHO_RESERVE       5   /* cm */
#define TACHO_NUM           (10 * 360) /* WHEEL_DEN * 360 */
#define TACHO_DEN           (176) /* WHEEL_NUM */
#define SHOOTING_EXTRA      1
#define REG_SUPERSPEED 20


// tacho walls
#define REG_WALLSHORT_TOWALL_DECELSTART   ((28 * 4 - TACHO_RESERVE) * TACHO_NUM / TACHO_DEN - DECELLEN_TACHO)
#define REG_WALLSHORT_TOWALL_ACCEL        ACCEL_TACHO
#define REG_WALLSHORT_TOWALL_DECEL        DECEL_TACHO
#define REG_WALLSHORT_TOWALL_UPPER        MAXSPEED
#define REG_WALLSHORT_TOWALL_LOWER        MINSPEED

#define REG_WALLLONG_TOWALL_DECELSTART    ((28 * 2 + 14 - TACHO_RESERVE) * TACHO_NUM / TACHO_DEN - DECELLEN_TACHO)
//#define REG_WALLLONG_TOWALL_DECELSTART 7200
#define REG_WALLLONG_TOWALL_ACCEL         ACCEL_TACHO
#define REG_WALLLONG_TOWALL_DECEL         DECEL_TACHO
#define REG_WALLLONG_TOWALL_UPPER         MAXSPEED
#define REG_WALLLONG_TOWALL_LOWER         MINSPEED


#ifndef REG_AVOID_SONIC

/////////////////////////////
// Sonic follow parameters //
/////////////////////////////


// sonic walls
#define REG_WALLSHORT_TOSHOOT_STOP        ((28 * 2 + 14) * 1 + US_SPACE)
#define REG_WALLSHORT_TOSHOOT_DECELSTART  (REG_WALLSHORT_TOSHOOT_STOP - DECELLEN_SONIC)
#define REG_WALLSHORT_TOSHOOT_ACCEL       ACCEL_SONIC
#define REG_WALLSHORT_TOSHOOT_DECEL       DECEL_SONIC
#define REG_WALLSHORT_TOSHOOT_UPPER       MAXSPEED
#define REG_WALLSHORT_TOSHOOT_LOWER       MINSPEED

#define REG_WALLLONG_TOSHOOT_STOP         ((28 * 4) * 1 + US_SPACE)
#define REG_WALLLONG_TOSHOOT_DECELSTART   (REG_WALLLONG_TOSHOOT_STOP - DECELLEN_SONIC)
#define REG_WALLLONG_TOSHOOT_ACCEL        ACCEL_SONIC
#define REG_WALLLONG_TOSHOOT_DECEL        DECEL_SONIC
#define REG_WALLLONG_TOSHOOT_UPPER        MAXSPEED
#define REG_WALLLONG_TOSHOOT_LOWER        MINSPEED

// move to the target parameters
#define REG_SHOOTFWD_SHORT_STOP        (28 * 2 + US_SPACE - SHOOTING_EXTRA)
#define REG_SHOOTFWD_SHORT_DECELSTART  (REG_SHOOTFWD_SHORT_STOP - DECELLEN_SONIC)
#define REG_SHOOTFWD_SHORT_ACCEL       ACCEL_SONIC
#define REG_SHOOTFWD_SHORT_DECEL       DECEL_SONIC
#define REG_SHOOTFWD_SHORT_UPPER       MAXSPEED
#define REG_SHOOTFWD_SHORT_LOWER       MINSPEED

#define REG_SHOOTFWD_LONG_STOP        (14 * 1 + US_SPACE - SHOOTING_EXTRA)
#define REG_SHOOTFWD_LONG_DECELSTART  (REG_SHOOTFWD_LONG_STOP - DECELLEN_SONIC)
#define REG_SHOOTFWD_LONG_ACCEL       ACCEL_SONIC
#define REG_SHOOTFWD_LONG_DECEL       DECEL_SONIC
#define REG_SHOOTFWD_LONG_UPPER       MAXSPEED
#define REG_SHOOTFWD_LONG_LOWER       MINSPEED

// move back to wall parameters
#define REG_SHOOTBWD_SHORT_DECELSTART  (28 * 2 + US_SPACE - SHOOTING_EXTRA - DECELLEN_SONIC_WALL)
#define REG_SHOOTBWD_SHORT_ACCEL       ACCEL_SONIC
#define REG_SHOOTBWD_SHORT_DECEL       DECEL_SONIC
#define REG_SHOOTBWD_SHORT_UPPER       MAXSPEED
#define REG_SHOOTBWD_SHORT_LOWER       MINSPEED

#define REG_SHOOTBWD_LONG_DECELSTART  (14 * 1 + US_SPACE - SHOOTING_EXTRA - DECELLEN_SONIC_WALL)
#define REG_SHOOTBWD_LONG_ACCEL       ACCEL_SONIC
#define REG_SHOOTBWD_LONG_DECEL       DECEL_SONIC
#define REG_SHOOTBWD_LONG_UPPER       MAXSPEED
#define REG_SHOOTBWD_LONG_LOWER       MINSPEED

#define REG_SHOOTBWD_TRIP        8
#define REG_SHOOTBWD_TRIPTIME    1000

#define REG_SHOOTBWD_LONG_TRIP   REG_SHOOTBWD_TRIP
#define REG_SHOOTBWD_SHORT_TRIP  REG_SHOOTBWD_TRIP

#else

/////////////////////////////
// Tacho follow parameters //
/////////////////////////////

#define REG_TRIPDIST (4 * TACHO_NUM / TACHO_DEN)


#define REG_WALLSHORT_TOSHOOT_STOP        ((28 * 2 + 14) * TACHO_NUM / TACHO_DEN)
#define REG_WALLSHORT_TOSHOOT_DECELSTART  (REG_WALLSHORT_TOSHOOT_STOP - DECELLEN_TACHO)
#define REG_WALLSHORT_TOSHOOT_ACCEL       ACCEL_TACHO
#define REG_WALLSHORT_TOSHOOT_DECEL       DECEL_TACHO
#define REG_WALLSHORT_TOSHOOT_UPPER       MAXSPEED
#define REG_WALLSHORT_TOSHOOT_LOWER       MINSPEED

#define REG_WALLLONG_TOSHOOT_STOP         ((28 * 4) * TACHO_NUM / TACHO_DEN)
#define REG_WALLLONG_TOSHOOT_DECELSTART   (REG_WALLLONG_TOSHOOT_STOP - DECELLEN_TACHO)
#define REG_WALLLONG_TOSHOOT_ACCEL        ACCEL_TACHO
#define REG_WALLLONG_TOSHOOT_DECEL        DECEL_TACHO
#define REG_WALLLONG_TOSHOOT_UPPER        MAXSPEED
#define REG_WALLLONG_TOSHOOT_LOWER        MINSPEED

// move to the target parameters
#define REG_SHOOTFWD_SHORT_STOP        ((28 * 2 - SHOOTING_EXTRA) * TACHO_NUM / TACHO_DEN)
#define REG_SHOOTFWD_SHORT_DECELSTART  (REG_SHOOTFWD_SHORT_STOP - DECELLEN_TACHO)
#define REG_SHOOTFWD_SHORT_ACCEL       ACCEL_TACHO
#define REG_SHOOTFWD_SHORT_DECEL       DECEL_TACHO
#define REG_SHOOTFWD_SHORT_UPPER       MAXSPEED
#define REG_SHOOTFWD_SHORT_LOWER       MINSPEED

#define REG_SHOOTFWD_LONG_STOP        ((14 * 1 - SHOOTING_EXTRA) * TACHO_NUM / TACHO_DEN)
#define REG_SHOOTFWD_LONG_DECELSTART  (REG_SHOOTFWD_LONG_STOP - DECELLEN_TACHO)
#define REG_SHOOTFWD_LONG_ACCEL       ACCEL_TACHO
#define REG_SHOOTFWD_LONG_DECEL       DECEL_TACHO
#define REG_SHOOTFWD_LONG_UPPER       MAXSPEED
#define REG_SHOOTFWD_LONG_LOWER       MINSPEED

// move back to wall parameters
#define REG_SHOOTBWD_SHORT_TRIP        ((28 * 2 - SHOOTING_EXTRA) * TACHO_NUM / TACHO_DEN - REG_TRIPDIST)
#define REG_SHOOTBWD_SHORT_DECELSTART  ((28 * 2 - SHOOTING_EXTRA) * TACHO_NUM / TACHO_DEN - DECELLEN_TACHO)
#define REG_SHOOTBWD_SHORT_ACCEL       ACCEL_TACHO
#define REG_SHOOTBWD_SHORT_DECEL       DECEL_TACHO
#define REG_SHOOTBWD_SHORT_UPPER       MAXSPEED
#define REG_SHOOTBWD_SHORT_LOWER       MINSPEED

#define REG_SHOOTBWD_LONG_TRIP        ((14 * 1 - SHOOTING_EXTRA) * TACHO_NUM / TACHO_DEN - REG_TRIPDIST)
#define REG_SHOOTBWD_LONG_DECELSTART  ((14 * 1 - SHOOTING_EXTRA) * TACHO_NUM / TACHO_DEN - DECELLEN_TACHO)
#define REG_SHOOTBWD_LONG_ACCEL       ACCEL_TACHO
#define REG_SHOOTBWD_LONG_DECEL       DECEL_TACHO
#define REG_SHOOTBWD_LONG_UPPER       MAXSPEED
#define REG_SHOOTBWD_LONG_LOWER       MINSPEED

#define REG_SHOOTBWD_TRIPTIME    1000

#endif


#define REG_ULTRAMEGA 100000


// post-bump travelforward
#define REG_POSTALIGN_STEPS  60
#define REG_POSTALIGN_SPEED  -10

#ifdef REG_AVAIL_BACKTOUCH

// bump travelback
#define REG_BACKALIGN_DELAY  500
#define REG_BACKALIGN_SPEED  -15

#else

// bump travelback
#define REG_BACKALIGN_DELAY  1000
#define REG_BACKALIGN_SPEED  -15

#endif

// post-hit travelback
#define REG_POSTHIT_STEPS    -10
#define REG_POSTHIT_SPEED     10
#define REG_POSTHIT_PREDELAY  500
#define REG_POSTHIT_POSTDELAY 400

// 90 turn parameters
#define REG_TURN_STEPS       270
#define REG_TURN_SPEED       20
#define REG_TURN_RATIO      -200
