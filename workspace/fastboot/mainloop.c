#include "header.h"

static void shooting(walltype length);
static ringpos ball2ring(ballcolor ball);

//
// main program "storyline"
//

void loop_enter(void) {
	walltype length = WALL_SHORT;
	
	while(true) {
		mov_wall(length + WM_TOWALL);
		mov_unfwdalign();
		mov_turn(true);
#ifdef CORNER_BACKALIGN
		mov_backalign();
#endif
		
		mov_wall(length + WM_TOSHOOT);
		mov_turn(true);
		mov_backalign();
		
		mov_shoot(length + SM_FORWARD);
		
		//if (length == WALL_LONG) {
		//	mov_superman();
		//}
		
		shooting(length);
		mov_shoot(length + SM_BACKWARD);
		mov_unbackalign();
		mov_turn(false);
		
		if (length == WALL_LONG)
			length = WALL_SHORT;
		else
			length = WALL_LONG;
	}
}

//
// shooting
//

void shooting(walltype length) {
	static ballcolor old = BALL_NONE;
	
	mot_ball_begin(RING_GREEN);
	while (true) {
		if (length == WALL_LONG && sen_ball_avail() == 1) {
			break;
		}
		ballcolor ball = sen_ball_pop();
		if (ball == BALL_NONE) {
			break;
		}
		if (old == ball) {
			continue;
		} else {
			old = ball;
		}
		mot_ball_goto(ball2ring(ball));
		mot_ball_put();
	}
	mot_ball_goto(RING_GREEN);
}

ringpos ball2ring(ballcolor ball) {
	switch (ball) {
		default:
		case BALL_NONE:
		case BALL_UNCERTAIN:
			return RING_GREEN; // simply wut; failsave
		case BALL_BLUE:
			return RING_BLUE;
		case BALL_GREEN:
			return RING_GREEN;
		case BALL_RED:
			return RING_RED;
		case BALL_YELLOW:
			return RING_YELLOW;
	}
}
