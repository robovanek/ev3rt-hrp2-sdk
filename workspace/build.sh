#!/bin/sh

set -x
set -e
if [ "$#" -ne "1" ]; then
  echo "Usage: $0 [filename]" 1>&2
  exit 1
fi

cd "$(dirname "$0")"

export PATH="/usr/lib/ccache:$PATH"

#make -j3 clean
make -j2 "img=$1"
#make -j2 "app=$1"
