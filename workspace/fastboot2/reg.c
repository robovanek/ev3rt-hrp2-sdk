#include "header.h"

//
// Local stuff
//

////////
// local functions
//

// speed generator
static int8_t reg_speedgen(void);
static int8_t reg_speedgen_max(void);
static void   reg_speedgen_configure(const RegSpeedGen *pDetails);
static int8_t  genRamp(SlopeParam const *param, int32_t delta);
static int8_t  genSteady(void);
static int8_t  genClamp(int32_t speed);
static inline int32_t fastSqrt(int32_t hue);
static inline int32_t fastCos(int32_t hue);
static void   reg_setoffset(void);

//
// Steering regulator
//

// thread unsafe, but I don't care; we save stack space
static RegSpeedGenCached spdData;

#ifndef REG_AVOID_SIDETOUCH
void reg_step(const RegParam* pParams, const RegHooks* pHooks, const RegSpeedGen *pSpeed, bool brake) {
	int8_t  basespeed, maxspeed;
	int16_t measurement, setpoint, error, d, dd, action;
	int16_t lspeed, rspeed;
	int32_t integral = 0;

//#ifdef REG_AVAIL_GYRO
//	int16_t angleNow, angleError;
//	int16_t angleOld = headingRight();
//#else
	d = 0;
//#endif

	reg_speedgen_configure(pSpeed);

	// first push
	basespeed = reg_speedgen();
	mot_freespeed(basespeed, basespeed);
	mot_freestart();

	// loop
	while(true) {
		// get speeds
		basespeed   = reg_speedgen();
		maxspeed    = reg_speedgen_max();
		
		// check stop condition
		if (basespeed <= 0) {
			break;
		}
		
		// get P
		measurement = pHooks->doMeasurement();
		setpoint    = pHooks->getSetpoint  ();
		error      = -(setpoint - measurement);
		

		// get DD
		lspeed = mot_lspeed();
		rspeed = mot_rspeed();
		dd = -(lspeed - rspeed);


		// get D
//#ifdef REG_AVAIL_GYRO
//		angleNow = headingNow();
//		angleError = angleOld - angleNow;
//		d = fastCos(angleError);
//#endif

		// get I
		integral += error;
		if (integral > pParams->LimitI) {
			integral = pParams->LimitI;
		} else if (integral < -pParams->LimitI) {
			integral = -pParams->LimitI;
		}
		

		// sum it all
		action = (pParams->Kp  * error    +
		          pParams->Ki  * integral +
		          pParams->Kd  * d / 1000 +
		          pParams->Kdd * dd) / pParams->Divisor;


		// wheel steering
		lspeed = basespeed + action /* * basespeed / maxspeed */;
		rspeed = basespeed - action /* * basespeed / maxspeed */;

		// push & sleep
		mot_freespeed(rspeed, lspeed);
		gfx_printf(0, 3, "L: %hhd", lspeed);
		gfx_printf(0, 4, "R: %hhd", rspeed);
		dly_tsk(REG_DELAY);
	}
	// stop
	if (brake)
		mot_stop();
	else
		mot_float();
}
#else

void reg_step(const RegParam* pParams, const RegHooks* pHooks, const RegSpeedGen *pSpeed, bool brake) {
	reg_straight(pSpeed, brake);
}

#endif

//
// Straight regulator
//
#ifndef REG_SYNCED_FORWARD
void reg_straight(const RegSpeedGen *pSpeed, bool brake, bool reverse) {
	int8_t spd;
	
	reg_speedgen_configure(pSpeed);
	
	// first push
	spd = reg_speedgen();
	if (reverse)
		spd *= -1;
	mot_freespeed(spd, spd);
	mot_freestart();

	while(true) {
		// sleep
		dly_tsk(REG_DELAY);

		// fetch
		spd = reg_speedgen();

		// stop condition
		if (spd <= 0) {
			break;
		}
		if (reverse)
			spd *= -1;
		// push
		mot_freespeed(spd, spd);
	}

	// stop
	if (brake)
		mot_stop();
	else
		mot_float();
}

#else

void reg_straight(const RegSpeedGen *pSpeed, bool brake) {
	int8_t spd;
	
	reg_speedgen_configure(pSpeed);
	
	// first push
	spd = reg_speedgen();

	while(true) {

		// fetch
		spd = reg_speedgen();

		// stop condition
		if (spd <= 0) {
			break;
		}

		// push
		mot_travelsteps(REG_ULTRAMEGA, 0, spd);
		
		// sleep
		dly_tsk(REG_DELAY_SYNC);
	}

	// stop
	if (brake)
		mot_stop();
	else
		mot_float();
}
#endif


//
// SPEED GENERATOR
//

bool reg_accelend(intptr_t unused) {
	int32_t measurement = spdData.doMeasurementAccel();
	if (measurement < 0)
		measurement *= -1;
	return measurement > spdData.accelRampEnd;
}

// speed limit
static int8_t genClamp(int32_t speed) {
	if (speed < spdData.lowerSpeed) {
		return spdData.lowerSpeed;
	} else if (speed > spdData.upperSpeed) {
		return spdData.upperSpeed;
	} else {
		return (int8_t)speed;
	}
}

static int8_t genRamp(SlopeParam const *param, int32_t delta) {
	int32_t velocity = fastSqrt(param->linPart * delta + param->absPart);
	return genClamp(velocity);
}

// constant velocity
static int8_t genSteady() {
	return spdData.upperSpeed;
}

//
// speed generator
//
int8_t reg_speedgen() {
	int32_t measurement;

	if (spdData.shallWeStop(spdData.arg)) {
		return 0;
	}


	if (spdData.stm == SPD_START) {
		spdData.stm = SPD_ACCEL;
		reg_setoffset();
		gfx_msgbox("-> accel");
	}

	if (spdData.stm == SPD_ACCEL) {
		if (spdData.shallAccelEnd(spdData.arg)) {
			spdData.stm = SPD_STEADY;
			reg_setoffset();
			gfx_msgbox("-> steady");
		} else {
			measurement = spdData.doMeasurementAccel();
			int8_t speed = genRamp(&spdData.accelSlope, measurement - spdData.offsetAccel);
			return speed;
		}
	}

	if (spdData.stm == SPD_STEADY) {
		if (spdData.shallDecelStart(spdData.arg)) {
			spdData.stm    = SPD_DECEL;
			reg_setoffset();
			gfx_msgbox("-> decel");
		} else {
			return genSteady();
		}
	}

	if (spdData.stm == SPD_DECEL) {
		measurement = spdData.doMeasurementDecel();
		return genRamp(&spdData.decelSlope, measurement - spdData.offsetDecel);
	}
	
	return 0;
}

int32_t reg_get_offset_accel(void) {
	return spdData.offsetAccel;
}

int32_t reg_get_offset_decel(void) {
	return spdData.offsetDecel;
}

int8_t reg_speedgen_max(void) {
	return spdData.upperSpeed;
}

void reg_setoffset(void) {
	spdData.offsetAccel = spdData.doMeasurementAccel();
	spdData.offsetDecel = spdData.doMeasurementDecel();
}

// configure 
void   reg_speedgen_configure(const RegSpeedGen *pDetails) {
	spdData.doMeasurementAccel = pDetails->doMeasurementAccel;
	spdData.doMeasurementDecel = pDetails->doMeasurementDecel;
	spdData.shallWeStop        = pDetails->shallWeStop;
	spdData.shallAccelEnd      = pDetails->shallAccelEnd;
	spdData.shallDecelStart    = pDetails->shallDecelStart;
	spdData.arg                = pDetails->arg;

	spdData.upperSpeed         = pDetails->upperSpeed;
	spdData.lowerSpeed         = pDetails->lowerSpeed;

	int32_t upperSquare = spdData.upperSpeed * spdData.upperSpeed;

	if (!pDetails->reverseStart) {
		spdData.accelSlope.absPart = 0;
		spdData.accelSlope.linPart = +2 * pDetails->startAcceleration;
	} else {
		spdData.accelSlope.absPart = 0;
		spdData.accelSlope.linPart = -2 * pDetails->startAcceleration;
	}

	if (!pDetails->reverseEnd) {
		spdData.decelSlope.absPart = upperSquare;
		spdData.decelSlope.linPart = -2 * pDetails->stopDeceleration;
	} else {
		spdData.decelSlope.absPart = upperSquare;
		spdData.decelSlope.linPart = +2 * pDetails->stopDeceleration;
	}

	spdData.stm = SPD_START;
	gfx_msgbox("-> start");

	// hopefully
	spdData.accelRampEnd = upperSquare / spdData.accelSlope.linPart;
	if (spdData.accelRampEnd < 0)
		spdData.accelRampEnd *= -1;
}

// fast sqrt
int32_t fastSqrt(int32_t hue) {
	float n = (float)hue;
	float nhalf;
	uint32_t i;

	nhalf = n * 0.5F;
	i = * (uint32_t*) &n;
	i = 0x5f3759df - ( i >> 1 );
	n = * (float*) &i;
	n = n * ( 1.5F - ( nhalf * n * n ) );

	return (int32_t) (1/n);
}

int32_t fastCos(int32_t hue) {
	bool neg = false;
	while (hue > 90) {
		hue -= 180;
		neg = !neg;
	}
	while (hue < -90) {
		hue += 180;
		neg = !neg;
	}
	int32_t result = 1000 - hue * hue * 126924 / 1000000; // hue huerino
	return neg ? -result : result;
}
