#include "header.h"

#pragma once

// ball colors

typedef enum {
	BALL_NONE,
	BALL_UNCERTAIN,
	BALL_BLUE,
	BALL_GREEN,
	BALL_RED,
	BALL_YELLOW
} ballcolor;

typedef struct {
	uint16_t chroma;
	uint16_t hue;
} colorinfo;

typedef enum {
	STATE_NONE,
	STATE_TRANSITION,
	STATE_COLOR
} colorstate;

typedef struct {
	colorstate stm;
	ballcolor  color;
} detectstate;

// init
extern void sen_init(void);

#ifdef SEN_ENABLE_TOUCH_SIDE
extern bool sen_touch_side(void);
#endif

#ifdef SEN_ENABLE_TOUCH_BACK
extern bool sen_touch_back(void);
#endif

#ifdef SEN_ENABLE_SONIC_FRONT
extern int16_t sen_sonic_front(void);
#endif

#ifdef SEN_ENABLE_GYRO
extern int16_t sen_gyro(void);
#endif

// smart ball sensing
extern const detectstate *sen_ball_detect_state(void);
extern       bool         sen_ball_detect_tick (void);
