#include "lulz.h"
#include "platform_interface_layer.h"

#ifdef SEN_ENABLE_TOUCH_BACK_MOTOR


static uint8_t *pButton;

void lulz_init(void) {
	brickinfo_t brickinfo;
	char buf[2];
	
	buf[0] = 0xE0;
	buf[1] = 1 << SEN_TOUCH_BACK_MOTOR;
	motor_command(&buf, sizeof(buf));
	
	fetch_brick_info(&brickinfo);
	pButton = brickinfo.motor_lulz;
}

bool lulz_get(void) {
	return (*pButton) & (1 << SEN_TOUCH_BACK_MOTOR);
}

#endif
