#include "header.h"

void snd_init(void) {
	ev3_speaker_set_volume(SND_VOLUME);
}

void snd_beep_ok(void) {
	ev3_speaker_play_tone(800, 100);
}

void snd_beep_error(void) {
	ev3_speaker_play_tone(500, 200);
}

void snd_beep_ball(ballcolor color) {
	int freq;
	switch (color) {
		default:
		case BALL_NONE:
		case BALL_UNCERTAIN:
			freq = 1000;
			break;
		case BALL_BLUE:
			freq = 440;
			break;
		case BALL_GREEN:
			freq = 587;
			break;
		case BALL_RED:
			freq = 494;
			break;
		case BALL_YELLOW:
			freq = 523;
			break;
	}
	ev3_speaker_play_tone(freq, 100);
}
