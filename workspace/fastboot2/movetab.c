#include "movetab.h"


//
// Local data tables
//

static RegParam wallPd = {
	.Kp      = REG_WALL_KP,
	.Ki      = REG_WALL_KI,
	.Kd      = REG_WALL_KD,
	.Kdd     = REG_WALL_KDD,
	.LimitI  = REG_WALL_ILIM,
	.Divisor = REG_WALL_DIV
};

static RegHooks wallHooks = {
	.doMeasurement = &getSideTouch,
	.getSetpoint   = &getSetpoint
};


static TouchSonic_t limits[] = {
	[WALL_LONG] = {
		.move_decel = 35,
		.move_stop  = 17,
		.us_delay   = 4000,
	},
	[WALL_SHORT] = {
		.move_decel = 35,
		.move_stop  = 17,
		.us_delay   = 3000,
	},
	[WALL_HALF_START] = {
		.move_decel = 35,
		.move_stop  = 17,
		.us_delay   = 0, // start 
	},
	[WALL_HALF_END] = {
		.move_decel = (28 +  0) * TACHO_NUM / TACHO_DEN,
		.move_stop  = (28 + 14) * TACHO_NUM / TACHO_DEN,
		.us_delay   = 0, // not used
	},
	[SHOOT_GOTO] = {
		.move_decel = (28 * 2) * TACHO_NUM / TACHO_DEN,
		.move_stop  = 0, // touch sensor
		.us_delay   = 3000,
	},
};

static bool brakes[] = {
	[WALL_LONG]       = false,
	[WALL_SHORT]      = false,
	[WALL_HALF_START] = false,
	[WALL_HALF_END]   = true,
	[SHOOT_GOTO]      = true,
};

static int32_t delays[] = {
	[WALL_LONG]       = 0,
	[WALL_SHORT]      = 0,
	[WALL_HALF_START] = 0,
	[WALL_HALF_END]   = 0,
	[SHOOT_GOTO]      = 0,
};

static RegSpeedGen moveCurves[] = {
	[WALL_LONG] = {
		.doMeasurementAccel = getDistance_Tacho,
		.doMeasurementDecel = getDistance_Sonic,

		.shallWeStop        = getStop_MotoSonic,
		.shallAccelEnd      = reg_accelend,
		.shallDecelStart    = getDecel_MotoSonic,
		.arg                = (intptr_t) &limits[WALL_LONG],

		.startAcceleration  = ACCEL_TACHO,
		.stopDeceleration   = DECEL_SONIC,
		.reverseStart       = false,
		.reverseEnd         = true,
		.upperSpeed         = MAXSPEED,
		.lowerSpeed         = MINSPEED,
	},

	[WALL_SHORT] = {
		.doMeasurementAccel = getDistance_Tacho,
		.doMeasurementDecel = getDistance_Sonic,

		.shallWeStop        = getStop_MotoSonic,
		.shallAccelEnd      = reg_accelend,
		.shallDecelStart    = getDecel_MotoSonic,
		.arg                = (intptr_t) &limits[WALL_SHORT],

		.startAcceleration  = ACCEL_TACHO,
		.stopDeceleration   = DECEL_SONIC,
		.reverseStart       = false,
		.reverseEnd         = true,
		.upperSpeed         = MAXSPEED,
		.lowerSpeed         = MINSPEED,
	},

	[WALL_HALF_START] = {
		.doMeasurementAccel = getDistance_Tacho,
		.doMeasurementDecel = getDistance_Sonic,

		.shallWeStop        = getStop_MotoSonic,
		.shallAccelEnd      = reg_accelend,
		.shallDecelStart    = getDecel_MotoSonic,
		.arg                = (intptr_t) &limits[WALL_HALF_START],

		.startAcceleration  = ACCEL_TACHO,
		.stopDeceleration   = DECEL_SONIC,
		.reverseStart       = false,
		.reverseEnd         = true,
		.upperSpeed         = MAXSPEED,
		.lowerSpeed         = MINSPEED,
	},
	[WALL_HALF_END] = {
		.doMeasurementAccel = getDistance_Tacho,
		.doMeasurementDecel = getDistance_Tacho,

		.shallWeStop        = getStop_MotoTacho,
		.shallAccelEnd      = reg_accelend,
		.shallDecelStart    = getDecel_MotoTacho,
		.arg                = (intptr_t) &limits[WALL_HALF_END],

		.startAcceleration  = ACCEL_TACHO,
		.stopDeceleration   = DECEL_TACHO,
		.reverseStart       = false,
		.reverseEnd         = false,
		.upperSpeed         = MAXSPEED,
		.lowerSpeed         = MINSPEED,
	},
	[SHOOT_GOTO] = {
		.doMeasurementAccel = getDistance_Tacho,
		.doMeasurementDecel = getDistance_Tacho,

		.shallWeStop        = getStop_BackTouch,
		.shallAccelEnd      = reg_accelend,
		.shallDecelStart    = getDecel_MotoTacho,
		.arg                = (intptr_t) &limits[SHOOT_GOTO],

		.startAcceleration  = ACCEL_TACHO,
		.stopDeceleration   = DECEL_TACHO,
		.reverseStart       = false,
		.reverseEnd         = false,
		.upperSpeed         = MAXSPEED,
		.lowerSpeed         = MINSPEED,
	},
};


void mov_do(movetype instr) {
	logmove(instr);
	
	sensorInit();
	switch(instr) {
		case WALL_LONG:
		case WALL_SHORT:
		case WALL_HALF_START:
		case WALL_HALF_END:
			reg_step(&wallPd, &wallHooks, &moveCurves[instr], brakes[instr]);
			if (delays[instr])
				dly_tsk(delays[instr]);
			break;
		case SHOOT_GOTO:
			reg_straight(&moveCurves[instr], brakes[instr], true);
			if (delays[instr])
				dly_tsk(delays[instr]);
			break;
		case TURN_FWDALIGN:
			mot_travelsteps_brake(REG_FWDALIGN_STEPS, 0, REG_FWDALIGN_SPEED, true);
			dly_tsk(REG_FWDALIGN_DELAY);
			break;
		case SHOOT_CORRECT_BWD:
			mot_travelsteps_brake(REG_BWDCORR_STEPS, 0, REG_CORR_SPEED, true);
			dly_tsk(REG_BWDCORR_DELAY);
			break;
		case SHOOT_CORRECT_FWD:
			mot_travelsteps_brake(REG_FWDCORR_STEPS, 0, REG_CORR_SPEED, true);
			dly_tsk(REG_FWDCORR_DELAY);
			break;
		case TURN_NORMAL:
			mot_travelsteps_brake(REG_TURN_STEPS90, 100, REG_TURN_SPEED, false);
			mot_waitfor();
			break;
		case TURN_AROUND:
			mot_travelsteps_brake(REG_TURN_STEPS90, 100, REG_TURN_SPEED, true);
			mot_waitfor();
			mot_travelsteps_brake(REG_TURN_STEPS180, 200, REG_TURN_SPEED, true);
			mot_waitfor();
			dly_tsk(REG_TURN_DELAY180);
			break;
		case SHOOT_PUT:
			mot_ball_put();
			dly_tsk(REG_PUT_DELAY);
			break;
	}
}
