#pragma once

#include "header.h"

// handler prototypes

typedef int16_t (*measurefn)(void);
typedef int16_t (*setpointfn)(void);

typedef int32_t (*lmeasurefn)(void);
typedef bool    (*predicate)(intptr_t params);
typedef void    (*initfn)(void);

// data structures

typedef enum {
	SPD_START,
	SPD_ACCEL,
	SPD_STEADY,
	SPD_DECEL
} spdgen_mode;

// dynamic regulator callbacks
typedef struct {
	measurefn  doMeasurement;
	setpointfn getSetpoint;
} RegHooks;

// static regulator parameters
typedef struct {
	int32_t LimitI;
	int16_t Kp;
	int16_t Ki;
	int16_t Kd;
	int16_t Kdd;
	int16_t Divisor;
} RegParam;

// slope parameter
typedef struct {
	int32_t absPart;
	int32_t linPart;
} SlopeParam;

// configuration of speed generator
typedef struct {
	// Function hooks
	lmeasurefn doMeasurementAccel;
	lmeasurefn doMeasurementDecel;
	predicate  shallWeStop;
	predicate  shallAccelEnd;
	predicate  shallDecelStart;
	intptr_t   arg;

	// Acceleration
	int16_t startAcceleration;
	int16_t stopDeceleration;
	bool reverseStart;
	bool reverseEnd;

	// Speed bounding
	int8_t upperSpeed;
	int8_t lowerSpeed;
} RegSpeedGen;

// working data
typedef struct {
	// dynamic callback
	lmeasurefn doMeasurementAccel;
	lmeasurefn doMeasurementDecel;
	predicate  shallWeStop;
	predicate  shallAccelEnd;
	predicate  shallDecelStart;
	intptr_t   arg;

	// state machine
	spdgen_mode stm;
	int32_t offsetAccel;
	int32_t offsetDecel;

	int32_t accelRampEnd;

	// acceleration details
	SlopeParam accelSlope;
	SlopeParam decelSlope;

	// speed bounds
	int8_t upperSpeed;
	int8_t lowerSpeed;
} RegSpeedGenCached;

// regulators

extern void    reg_step(const RegParam* pParams, const RegHooks* pHooks, const RegSpeedGen *pSpeed, bool brake);
extern void    reg_straight(const RegSpeedGen *pSpeed, bool brake, bool reverse);
extern int32_t reg_get_offset_accel(void);
extern int32_t reg_get_offset_decel(void);
extern bool    reg_accelend(intptr_t unused);
