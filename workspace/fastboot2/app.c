#include "header.h"

//
// main entry point
//

void main_task(intptr_t arg) {
	gfx_init();
	gfx_msgbox("gfx init");
	snd_init();
	gfx_msgbox("snd init");
	sen_init();
	gfx_msgbox("sen init");
	mot_init();
	gfx_msgbox("mot init");
	mov_init();
	gfx_msgbox("mov init");
	if (gfx_start()) {
		gfx_msgbox("gfx start");
		ev3_sta_cyc(ROUTER_CYC);
		loop_enter();
	}
	os_exit();
}

void router_tick (intptr_t arg) {
	route_do_tick();
}
