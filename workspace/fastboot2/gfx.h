#pragma once

#include "header.h"

extern void gfx_init(void);
extern void gfx_string(int32_t x, int32_t y, char *msg);
extern void gfx_printf(int32_t x, int32_t y, char *msg, ...);
extern void gfx_clear(int32_t y);
extern void gfx_msgbox(char *str);
extern void gfx_msgbox_clear(void);
extern bool gfx_start(void);
extern void gfx_led(ledcolor_t color);
