#pragma once

#include "header.h"

extern void mov_init(void);


// steering regulator
extern int16_t getSideTouch(void);
extern int16_t getSetpoint(void);

//
// HueHueHue enhancement
//
extern void sensorInit();

extern int32_t getDistance_Tacho    (void);
extern int32_t getDistance_Sonic    (void);
extern int32_t getDistance_Backsonic(void);

extern bool getStop_TripProcess(bool reality, RELTIM delay);
extern bool getStop_Tacho(intptr_t limit);
extern bool getStop_TachoBackTrip(intptr_t limit);
extern bool getStop_BackTouch (intptr_t unused);
extern bool getStop_FrontSonic(intptr_t limit);

typedef struct {
	int32_t move_decel;
	int32_t move_stop;
	int32_t us_delay;
} TouchSonic_t;

extern bool getStop_MotoSonic(intptr_t strukt);
extern bool getDecel_MotoSonic(intptr_t strukt);
extern bool getStop_MotoTacho(intptr_t strukt);
extern bool getDecel_MotoTacho(intptr_t strukt);

// logging
extern void logmove(uint8_t which);
