#pragma once

extern void snd_init(void);
extern void snd_beep_ok(void);
extern void snd_beep_error(void);
extern void snd_beep_ball(ballcolor color);
