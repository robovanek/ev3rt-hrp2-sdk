#include "header.h"

/////////////////
// MOTOR LOCAL //
/////////////////

#define MSK_STEER ((1 << MOT_LEFT) | (1 << MOT_RIGHT))
#define MSK_BALL  (1 << MOT_BALL)
#define MSK_ALL   (MSK_STEER | MSK_BALL)

static void mot_ball_init(void);

static ringpos position;


//////////
// INIT //
//////////

void mot_init(void) {
	motor_type_t types[4] = {NONE_MOTOR, NONE_MOTOR, NONE_MOTOR, NONE_MOTOR};
	types[MOT_LEFT]  = LARGE_MOTOR;
	types[MOT_RIGHT] = LARGE_MOTOR;
	types[MOT_BALL] = MEDIUM_MOTOR;
	if (ev3_motor_config_all(types) != E_OK)
		snd_beep_error();
	ev3_motor_ex_reset_counts(MSK_STEER);
	mot_ball_init();
}


////////////////////
// UNLIMITED MOVE //
////////////////////

void mot_freestart(void) {
	if (ev3_motor_ex_reset_counts(MSK_STEER) != E_OK)
		snd_beep_error();
	if (ev3_motor_ex_start(MSK_STEER) != E_OK)
		snd_beep_error();
}

void mot_freespeed(int8_t speedR, int8_t speedL) {
	if (ev3_motor_ex_setspeed(1 << MOT_LEFT,  speedL) != E_OK)
		snd_beep_error();
	if (ev3_motor_ex_setspeed(1 << MOT_RIGHT, speedR) != E_OK)
		snd_beep_error();
}

void mot_stop(void) {
	if (ev3_motor_ex_stop(MSK_STEER, true) != E_OK)
		snd_beep_error();
}

void mot_float(void) {
	if (ev3_motor_ex_stop(MSK_STEER, false) != E_OK)
		snd_beep_error();
}

//////////////////////
// SPEED & POSITION //
//////////////////////

int8_t mot_lspeed(void) {
	int val;
	if (ev3_motor_get_velocity(MOT_LEFT, &val) != E_OK)
		snd_beep_error();
	return val;
}

int8_t mot_rspeed(void) {
	int val;
	if (ev3_motor_get_velocity(MOT_RIGHT, &val) != E_OK)
		snd_beep_error();
	return val;
}

int32_t mot_lcount(void) {
	return ev3_motor_get_counts(MOT_LEFT);
}

int32_t mot_rcount(void) {
	return ev3_motor_get_counts(MOT_RIGHT);
}

void mot_resetcount(void) {
	if (ev3_motor_ex_reset_counts(MSK_STEER) != E_OK)
		snd_beep_error();
}


/////////////////
// SYNCED MOVE //
/////////////////

void mot_travelsteps(uint32_t steps, int16_t ratio, int8_t speed) {
	mot_travelsteps_brake(steps, ratio, speed, true);
}

void mot_travelsteps_brake(uint32_t steps, int16_t ratio, int8_t speed, bool brake) {
	ev3_motor_ex_syncparams_t params;
	params.speed      = speed;
	params.turn_ratio = ratio;
	params.length     = steps;
	params.brake      = brake;
	if (ev3_motor_ex_sync_step(MOT_LEFT, MOT_RIGHT, &params) != E_OK)
		snd_beep_error();
}

////////////////
// TIMED MOVE //
////////////////

void mot_traveltime(uint32_t time, int8_t power) {
	ev3_motor_ex_moveparams_t params;
	params.speed    = power;
	params.rampup   = 0;
	params.sustain  = time;
	params.rampdown = 0;
	params.brake    = true;
	if (ev3_motor_ex_unreg_time(MSK_STEER, &params) != E_OK)
		snd_beep_error();
}

/////////////////////////
// WAIT FOR COMPLETION //
/////////////////////////

void mot_waitfor(void) {
	ev3_motor_ex_poll(MSK_STEER);
}


//////////////
// SHOOTING //
//////////////

void mot_ball_goto(ringpos new) {
	int8_t sign = 1;
	int32_t diff = new - position;
	if (diff == 0) {
		goto aligned;
	}
	if (diff < 0) {
		diff = -diff;
		sign = -1;
	}
	
	ev3_motor_ex_moveparams_t params;
	params.speed    = MOT_BALL_SPEED * sign;
	params.rampup   = 0;
	params.sustain  = diff;
	params.rampdown = 0;
	params.brake    = true;
    
	ev3_motor_ex_reg_step(MSK_BALL, &params);
aligned:
	position = new;
}

bool mot_ball_goto_finished() {
	return !ev3_motor_ex_running(MSK_BALL);
}

// rotate the carriage
void mot_ball_put(void) {
	// noop
}

void mot_ball_init(void) {
	// set correct polarity
	ev3_motor_ex_stop(MSK_BALL, true);
	ev3_motor_ex_reset_counts(MSK_BALL);
	position = MOT_RING_INIT;
}
