#include "header.h"

//
// main program "storyline"
//

static movetype moves[] = {
	WALL_HALF_START,
	TURN_NORMAL,
	
	WALL_LONG,
	TURN_NORMAL,
	
//	WALL_SHORT,
//	TURN_NORMAL,
	
//	WALL_LONG,
//	TURN_NORMAL,
	
	WALL_HALF_END,
	TURN_AROUND,
	
	TURN_FWDALIGN,
	SHOOT_GOTO,
	SHOOT_PUT,
	
	SHOOT_CORRECT_BWD,
	SHOOT_CORRECT_FWD,
	SHOOT_CORRECT_BWD,
	SHOOT_CORRECT_FWD,
	SHOOT_CORRECT_BWD,
	SHOOT_CORRECT_FWD
};


void loop_enter(void) {
	movetype *ip  = moves;
	movetype *end = moves + sizeof(moves) / sizeof(moves[0]);
	for (; ip != end; ++ip) {
		mov_do(*ip);
	}
}
