#include "header.h"


static void getDistance_Init_Tacho(void);
static void getDistance_Init_Sonic(void);
static void getStop_Init_Trip(void);

//
// Local state tracking variables
//

static struct tachostate_t {
	int32_t tachoL0;
	int32_t tachoR0;
} tachostate;

static struct sonicstate_t {
	int32_t sonicLast;
	int16_t sonic0;
} sonicstate;

static struct tripstate_t {
	SYSTIM timestamp;
	bool tripped;
} tripstate;

static struct gyrostate_t {
	int16_t expected;
} gyrostate;


////////////////////////
// TRAVELLED DISTANCE //
////////////////////////

void sensorInit(void) {
	getDistance_Init_Tacho();
	getDistance_Init_Sonic();
	getStop_Init_Trip();
}

void    getDistance_Init_Tacho(void) {
	tachostate.tachoL0 = mot_lcount();
	tachostate.tachoR0 = mot_rcount();
}

int32_t getDistance_Tacho    (void) {
	int32_t tachoL  = mot_lcount();
	int32_t tachoR  = mot_rcount();
	int32_t dTachoL = tachoL - tachostate.tachoL0;
	int32_t dTachoR = tachoR - tachostate.tachoR0;
	int32_t avg     = (dTachoL + dTachoR) / 2;
	if (avg < 0)
		avg = -avg; // we do not want absolute position
	return avg;
}

void    getDistance_Init_Sonic(void) {
	int32_t now = sen_sonic_front();
	if (now == 255)
		now = US_SPACE;
	
	sonicstate.sonic0         = now;
	sonicstate.sonicLast      = now;
	getStop_Init_Trip();
}

int32_t getDistance_Sonic    (void) {
	int32_t now = sen_sonic_front();
	if (now != 255) {
		sonicstate.sonicLast = (US_FILTER_NOW  * now  +
		                        US_FILTER_PAST * sonicstate.sonicLast) / US_FILTER_DEN;
	}
	return sonicstate.sonicLast;
}

int32_t getDistance_Backsonic(void) {
	int32_t distance = getDistance_Sonic();
	int32_t delta = -(distance - sonicstate.sonic0);
	return delta > 0 ? delta : 0;
}

////////////////
// STOP STATE //
////////////////

bool getStop_BackTouch (intptr_t unused) {
	return getStop_TripProcess(sen_touch_back(), REG_BACKALIGN_DELAY);
}

bool getStop_Tacho(intptr_t limit) {
	return getDistance_Tacho() > limit;
}

bool getStop_TachoBackTrip(intptr_t limit) {
	int32_t dist = getDistance_Tacho();
	return getStop_TripProcess(dist > limit, REG_SHOOTBWD_TRIPTIME);
}

bool getStop_Sonic(intptr_t limit) {
	return getDistance_Sonic() > limit;
}

bool getStop_Backsonic(intptr_t limit) {
	return getDistance_Sonic() < limit;
}


bool getStop_MotoSonic(intptr_t strukt) {
	TouchSonic_t const *pLim = (TouchSonic_t const *) strukt;
	
	if (!getStop_TripProcess(true, pLim->us_delay)) {
		return false;
	}
	return getDistance_Sonic() < pLim->move_stop;
}

bool getDecel_MotoSonic(intptr_t strukt) {
	TouchSonic_t const *pLim = (TouchSonic_t const *) strukt;
	return getDistance_Sonic() < pLim->move_decel;
}

bool getStop_MotoTacho(intptr_t strukt) {
	TouchSonic_t const *pLim = (TouchSonic_t const *) strukt;
	return getDistance_Tacho() > pLim->move_stop;
}

bool getDecel_MotoTacho(intptr_t strukt) {
	TouchSonic_t const *pLim = (TouchSonic_t const *) strukt;
	return getDistance_Tacho() > pLim->move_decel;
}

////////////////
// TIME LIMIT //
////////////////

void getStop_Init_Trip(void) {
	tripstate.tripped   = false;
	tripstate.timestamp = 0;
}

bool getStop_TripProcess(bool reality, RELTIM delay) {
	SYSTIM now;
	get_tim(&now);
	
	if (tripstate.tripped) {
		return (now - tripstate.timestamp) > delay;
	} else {
		if (reality) {
			tripstate.tripped   = true;
			tripstate.timestamp = now;
		}
		return false;
	}
}

//
// steering regulator
//

int16_t getSideTouch(void) {
	if (sen_touch_side()) {
		return REG_PV_PRESSED;
	} else {
		return REG_PV_RELEASED;
	}
}

int16_t getSetpoint(void) {
	return REG_SETPOINT;
}


//
// Move drivers
//

void mov_init(void) {
	// noop
}

void logmove(uint8_t which) {
	char *msg = "ERROR";
	switch (which) {
		case WALL_LONG:
			msg = "LONG WALL";
			break;
		case WALL_SHORT:
			msg = "SHORT WALL";
			break;
		case WALL_HALF_START:
			msg = "START HALF";
			break;
		case WALL_HALF_END:
			msg = "END HALF";
			break;
		case SHOOT_GOTO:
			msg = "GOTO SHOOT";
			break;
		case SHOOT_PUT:
			msg = "FLUSH BALLS";
			break;
		case SHOOT_CORRECT_FWD:
			msg = "CORRECT FWD";
			break;
		case SHOOT_CORRECT_BWD:
			msg = "CORRECT BWD";
			break;
		case TURN_NORMAL:
			msg = "TURN 90°";
			break;
		case TURN_AROUND:
			msg = "TURN 180°";
			break;
		case TURN_FWDALIGN:
			msg = "WALL ALIGN";
			break;
	}
	gfx_msgbox(msg);
}
