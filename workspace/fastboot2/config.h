#pragma once

//
// Graphics subsystem
//

//#define GFX_TITLE "FastBoot      FTW"
#define GFX_TITLE "FastBoot2 .... mV"


//
// Sound subsystem
//

#define SND_VOLUME 10

//
// Sensor subsystem
//

// ports
#define S(x) (x - 1)
#define M(x) (x - 1)



#define MOT_RIGHT            M(1)
#define MOT_BALL             M(2)
#define MOT_LEFT             M(3)
#define SEN_TOUCH_BACK_MOTOR M(4)

#define SEN_COLOR            S(1)
#define SEN_TOUCH_SIDE       S(2)
#define SEN_SONIC            S(3)
#define SEN_GYRO             S(4)
#define SEN_TOUCH_BACK       S(4)

#define SEN_ENABLE_TOUCH_SIDE
#define SEN_ENABLE_TOUCH_BACK
#define SEN_ENABLE_TOUCH_BACK_MOTOR
#define SEN_ENABLE_SONIC_FRONT
//#define SEN_ENABLE_GYRO

//#define REG_AVOID_SIDETOUCH
//#define REG_SYNCED_FORWARD

#undef BALL_DEBUG

// linear correction of Red component
#define BALLCOR_RED_AN   1024
#define BALLCOR_RED_AD   240
#define BALLCOR_RED_B    -9

// linear correction of Green component
#define BALLCOR_GREEN_AN 1024
#define BALLCOR_GREEN_AD 230
#define BALLCOR_GREEN_B  -9

// linear correction of Blue component
#define BALLCOR_BLUE_AN  1024
#define BALLCOR_BLUE_AD  140
#define BALLCOR_BLUE_B   -15

// ball detection chroma threshold
#define BALL_ALL_CHROMA1 50
// ball identification chroma threshold
#define BALL_ALL_CHROMA2 150

// min-max hue for red ball
#define BALL_RED_HUE1    (5 - 25 + 360)
#define BALL_RED_HUE2    (5 + 25)
// min-max hue for green ball
#define BALL_GREEN_HUE1  (110 - 25)
#define BALL_GREEN_HUE2  (110 + 25)
// min-max hue for blue ball
#define BALL_BLUE_HUE1   (195 - 25)
#define BALL_BLUE_HUE2   (195 + 25)
// min-max hue for yellow ball
#define BALL_YELLOW_HUE1 (55 - 25)
#define BALL_YELLOW_HUE2 (55 + 25)

// color loop timing
#define BALL_DELAY 5
#define SEN_SETUP_DELAY1 0
#define SEN_SETUP_DELAY2 100

#define US_FILTER_NOW    50
#define US_FILTER_PAST   50
#define US_FILTER_DEN    100


//
// Motor subsystem
//


////
// shoter

#define MOT_RING_BLUE    -20
#define MOT_RING_GREEN   0
#define MOT_RING_YELLOW  +50
#define MOT_RING_RED     +70
#define MOT_RING_INIT    0
#define MOT_BALL_SPEED   50
#define ROUTER_DELAY 5
#define SHOOT_CORRECT_DELAY 5000
#define SHOOT_CORRECT_DELTA 1000

//
// regulated moves
//

// loop dt
#define REG_DELAY 10
#define REG_DELAY_SYNC 10

// touch sensor PID parameters
#define REG_WALL_KP      60 /* touch press */
#define REG_WALL_KI      0 /* integral    */
#define REG_WALL_KD      0  /* heading     */
#define REG_WALL_KDD     0  /* wheel ratio */
#define REG_WALL_DIV     100
#define REG_WALL_ILIM    150

#define REG_PV_PRESSED   -13
#define REG_PV_RELEASED  20
#define REG_SETPOINT     0

#define REG_GYRO_MAXERROR 20
#define REG_TURN_DPD_NUM  270
#define REG_TURN_DPD_DEN   90

// shortcuts for speed parameters
#define ACCEL_TACHO         10  /* ??? */
#define DECEL_TACHO         10  /* ??? */
#define ACCEL_SONIC         50  /* ??? */
#define DECEL_SONIC         50  /* ??? */
#define MAXSPEED            50  /* pct */
#define MINSPEED            15  /* pct */
#define TACHO_NUM           (10 * 360) /* WHEEL_DEN * 360 */
#define TACHO_DEN           (176) /* WHEEL_NUM */

#define US_SPACE 150


#define REG_FWDALIGN_STEPS 100
#define REG_FWDALIGN_SPEED (MAXSPEED/2)
#define REG_FWDALIGN_DELAY 500
#define REG_CORR_SPEED     (MAXSPEED/2)
#define REG_BWDCORR_STEPS  (-14 * TACHO_NUM / TACHO_DEN)
#define REG_BWDCORR_DELAY  1000
#define REG_FWDCORR_STEPS  (18 * TACHO_NUM / TACHO_DEN)
#define REG_FWDCORR_DELAY  5000
#define REG_PUT_DELAY      5000

#define REG_ULTRAMEGA 100000


#define REG_BACKALIGN_DELAY    0
#define REG_BACKALIGN_SPEED    0
#define REG_SHOOTBWD_TRIPTIME  0



// 90 turn parameters
#define REG_TURN_STEPS90     470
#define REG_TURN_STEPS180    500
#define REG_TURN_DELAY180    500
#define REG_TURN_SPEED       20
#define REG_TURN_RATIO      -200
