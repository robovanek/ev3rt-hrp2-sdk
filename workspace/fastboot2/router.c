#include "router.h"

typedef enum {
	IDLE,
	GOTO,
	WAITING,
	WAITING_GOTO
} route_state;

static ringpos ball2ring(ballcolor ball);
static route_state rtstate;

void route_do_tick(void) {
	const detectstate *col_st  = sen_ball_detect_state();
	bool               col_chg = sen_ball_detect_tick();
	ringpos            dest;

	if (col_chg) {
		snd_beep_ball(col_st->color);
		switch(rtstate) {
			case IDLE:
			case GOTO:
				rtstate = GOTO;
				break;
			case WAITING:
			case WAITING_GOTO:
				rtstate = WAITING_GOTO;
				break;
		}
	}

	switch(rtstate) {
		case GOTO:
			dest = ball2ring(col_st->color);
			mot_ball_goto(dest);
			rtstate = WAITING;
			break;
		case WAITING:
			if (mot_ball_goto_finished()) {
				rtstate = IDLE;
			}
			break;
		case WAITING_GOTO:
			if (mot_ball_goto_finished()) {
				rtstate = GOTO;
			}
			break;
		case IDLE:
			break;
	}
}

ringpos ball2ring(ballcolor ball) {
	switch (ball) {
		default:
		case BALL_NONE:
		case BALL_UNCERTAIN:
			return MOT_RING_GREEN; // simply wut; failsave
		case BALL_BLUE:
			return MOT_RING_BLUE;
		case BALL_GREEN:
			return MOT_RING_GREEN;
		case BALL_RED:
			return MOT_RING_RED;
		case BALL_YELLOW:
			return MOT_RING_YELLOW;
	}
}
