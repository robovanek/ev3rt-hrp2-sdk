#pragma once

#include "header.h"

#ifdef SEN_ENABLE_TOUCH_SIDE_MOTOR
extern void lulz_init(void);
extern bool lulz_get(void);
#endif
