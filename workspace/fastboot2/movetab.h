#pragma once

#include "header.h"

// move types

typedef enum {
	WALL_LONG,
	WALL_SHORT,
	WALL_HALF_START,
	WALL_HALF_END,
	SHOOT_GOTO,
	SHOOT_PUT,
	SHOOT_CORRECT_BWD,
	SHOOT_CORRECT_FWD,
	TURN_NORMAL,
	TURN_AROUND,
	TURN_FWDALIGN
} movetype;

// moves


extern void mov_do(movetype which);

