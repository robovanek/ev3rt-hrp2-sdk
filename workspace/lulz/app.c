#include "ev3api.h"
#include "app.h"
#include "platform_interface_layer.h"


void main_task(intptr_t exinf) {
	ev3_lcd_set_font(EV3_FONT_MEDIUM);
	
	brickinfo_t brickinfo;
	fetch_brick_info(&brickinfo);
	uint8_t *pButton = brickinfo.motor_lulz;
	
	/*
	ev3_motor_ex_moveparams_t params = {
		.speed    = 50,
		.rampup   = 20,
		.sustain  = 100,
		.rampdown = 20,
		.brake    = true
	};
	if (ev3_motor_config(EV3_PORT_A, LARGE_MOTOR) != E_OK) 
		ev3_lcd_draw_string("1", 10, 20);

	if (ev3_motor_ex_reg_step(EV3_PORTBIT_A, &params) != E_OK) 
		ev3_lcd_draw_string("2", 20, 20);

	if (ev3_motor_ex_poll(EV3_PORTBIT_A) != E_OK) 
		ev3_lcd_draw_string("3", 30, 20);

	ER ercd = ev3_motor_stop(EV3_PORT_A, false);
	*/
	
	char buf[2];
	buf[0] = 0xE0;
	buf[1] = 0x01;
	motor_command(&buf, sizeof(buf));
	
	while(1) {
		ev3_lcd_draw_string(" ", 0, 0);
		if (*pButton & 0x01) {
			ev3_lcd_draw_string("1", 0, 0);
		} else {
			ev3_lcd_draw_string("0", 0, 0);
		}
		dly_tsk(100);
	}
}
