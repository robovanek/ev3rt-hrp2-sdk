#ifndef _APP_H_
#define _APP_H_

#define STACK_SIZE 4096

#define RATIO_NUMERATOR   24
#define RATIO_DENOMINATOR 40
#define MOTOR EV3_PORT_C

#define SHOOT_ANGLE 50

#define RAT_RWD(x) (RATIO_NUMERATOR   * (x) / RATIO_DENOMINATOR)
#define RAT_FWD(x) (RATIO_DENOMINATOR * (x) / RATIO_NUMERATOR)

extern void setup_task(intptr_t unused);
extern void info_task(intptr_t unused);

#endif//_APP_H_
