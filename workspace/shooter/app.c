#include "ev3api.h"
#include "app.h"
#include "syssvc/syslog.h"

typedef enum {
	POS_NORMAL,
	POS_EJECTED
} arm_position_t;

static volatile
bool_t reset_minimax = true;

static volatile
arm_position_t o_st = POS_NORMAL;

static
void button_pressed(intptr_t n_st) {
	if (o_st != n_st) {
		o_st = n_st;
		snd_dtq(EVENTS, o_st);
	}
}

static
void cnt_reset(intptr_t unused) {
	reset_minimax = true;
}

static
void shoot(void) {
	ev3_motor_move_speed(MOTOR, +100, EV3_MOVE_TACHO, RAT_FWD(0), RAT_FWD(SHOOT_ANGLE), RAT_FWD(0), true);
	ev3_motor_poll(MOTOR);
}

static
void park(void) {
	ev3_motor_move_speed(MOTOR,  -20, EV3_MOVE_TACHO, RAT_FWD(5), RAT_FWD(SHOOT_ANGLE - 10), RAT_FWD(5), true);
	ev3_motor_poll(MOTOR);
}

void setup_task(intptr_t unused) {
	ev3_button_set_on_clicked(LEFT_BUTTON,  button_pressed, POS_NORMAL);
	ev3_button_set_on_clicked(RIGHT_BUTTON, button_pressed, POS_EJECTED);
	ev3_button_set_on_clicked(ENTER_BUTTON, cnt_reset, 0);
	ev3_motor_config(MOTOR, MEDIUM_MOTOR);
	ev3_motor_stop(MOTOR, true);
	ev3_motor_reset_counts(MOTOR);
	act_tsk(INFO_TASK);

	intptr_t armpos;
	ER ercd;
	while(1) {
		ercd = rcv_dtq(EVENTS, &armpos);

		switch (ercd) {
			case E_ID:
			case E_NOEXS:
			case E_DLT:
			case E_PAR:
				return;
			case E_RLWAI:
			case E_TMOUT:
				continue;
		}

		if (armpos == POS_EJECTED) {
			shoot();
		} else {
			park();
		}
	}
}

static
void int2str(int8_t pwr, char *data) {
	if (pwr < 0) {
		pwr = -pwr;
		data[0] = '-';
	} else {
		data[0] = '+';
	}
	
	data[1] = '0' + (pwr / 100) % 10;
	data[2] = '0' + (pwr /  10) % 10;
	data[3] = '0' + (pwr /   1) % 10;
}

static
void update_minimax(int8_t pwr, int8_t *p_min, int8_t *p_max) {
	if (reset_minimax) {
		reset_minimax = false;
		*p_max = SCHAR_MIN;
		*p_min = SCHAR_MAX;
	}
	if (*p_max < pwr)
		*p_max = pwr;
	if (*p_min > pwr)
		*p_min = pwr;
}

void info_task(intptr_t unused) {
	int32_t w, h;
	int8_t pwr, max = 0, min = 0;
	
	char data[15];
	data[ 4] = ',';
	data[ 9] = ',';
	data[14] = '\0';
	
	ev3_lcd_set_font(EV3_FONT_MEDIUM);
	ev3_font_get_size(EV3_FONT_MEDIUM, &w, &h);

	while (1) {
		ev3_motor_get_velocity(MOTOR, &pwr);
		update_minimax(pwr, &min, &max);

		int2str(pwr, data +  0);
		int2str(min, data +  5);
		int2str(max, data + 10);
		
		syslog(LOG_NOTICE, "info tick: %s", data);
		
		tslp_tsk(10);
	}
}
