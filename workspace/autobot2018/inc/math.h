#pragma once

#include <stdint.h>

extern int32_t wheels2ang(int32_t left, int32_t right);
extern void   param2wheels(int32_t middle, int32_t angle, int8_t *left, int8_t *right);
