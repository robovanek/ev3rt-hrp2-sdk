#pragma once

#include "math.h"

typedef struct {
	int32_t angspeed;
	int32_t linspeed;
} oblock_t;

extern oblock_t outmem;

extern void output_init();
extern void output_start();
extern void output_stop();
extern void output_apply();
