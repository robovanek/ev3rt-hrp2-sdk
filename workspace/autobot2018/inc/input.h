#pragma once

typedef struct { 
	int16_t light;
	int     wsleft, wsright;
} irawblock_t;


typedef struct {
	int32_t midspeed;
	int32_t midrotation;
	int32_t light;
} idatablock_t;

typedef struct {
	int16_t light_min;
	int16_t light_max;
} izeroblock_t;

typedef struct {
	irawblock_t  raw;
	izeroblock_t zero;
	idatablock_t data;
} input_io_t;

extern input_io_t inmem;

extern void input_init();
extern void input_sample();

typedef enum {
	ACTIVATE_NONE,
	ACTIVATE_STOP,
	ACTIVATE_ENTER,
	ACTIVATE_LEFT,
	ACTIVATE_RIGHT,
} packme actbtn;

extern actbtn input_buttons();
