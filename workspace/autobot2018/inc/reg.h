#pragma once
#include "math.h"
#include "filter.h"


typedef struct {
	int32_t error;
	int32_t derivative;
	int32_t integral;
	diff_filt_t df;
} rstate_t;

typedef struct {
	int32_t setpoint;
	int32_t coef_p;
	int32_t coef_i;
	int32_t coef_d;
	int32_t den;
	int32_t coef_i_bound;
	int32_t coef_i_forget;
	int8_t  sign;
} rparam_t;

typedef struct {
	int32_t current;
	int32_t fix;
} rout_t;

typedef struct {
	int32_t val;
	int32_t curO;
} rin_t;

typedef struct {
	rparam_t p;
	rstate_t s;
	rin_t    i;
	rout_t   o;
} reg_t;

typedef struct {
	reg_t veh;
} rstack_t;

extern void reg_process  (reg_t *r);
extern void reg_reset    (rstack_t *pmem);
extern void reg_reset_one(reg_t    *pmem);

inline void reg_push_turn(reg_t *p) {
	outmem.angspeed = p->o.current + p->o.fix;
}
