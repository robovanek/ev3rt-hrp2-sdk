#pragma once

extern void gfx_init(void);
extern void gfx_string(int32_t x, int32_t y, char *msg);
extern void gfx_printf(int32_t x, int32_t y, char *msg, ...);
extern void gfx_clear(int32_t y);
extern void gfx_msgbox(const char *str);
extern void gfx_msgbox_clear(void);
extern void gfx_start(const char *msg);
extern void gfx_led(ledcolor_t color);
extern void gfx_batt_tick();
