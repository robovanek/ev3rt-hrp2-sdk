#pragma once
#include "math.h"

typedef struct {
	int32_t diff_z[4];
	int32_t var_z1;
} diff_filt_t;

extern int32_t dfilt(int32_t var_z0, diff_filt_t *p);
