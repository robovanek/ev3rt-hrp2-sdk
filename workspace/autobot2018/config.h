#pragma once
#define  packme __attribute__ ((__packed__))

// THR
#define   BATT_DELAY_MSEC  2000
#define    REG_DELAY_MSEC    10

// SND
#define SND_VOLUME 10

// PORTS
#define S(x) (x - 1)
#define M(x) (x - 1)

// OUT
#define MOT_RIGHT M(2)
#define MOT_LEFT  M(3)
#define MOT_SIGN  (+1)

// IN
#define SEN_COLOR S(2)


// something like trackwidth / wheelradius
// GEOMETRY
#define WHEELS2ANG_NUM (100)
#define WHEELS2ANG_DEN (-34)
#define ANG2WHEELS_NUM (-17)
#define ANG2WHEELS_DEN (100)
#define TRACKWIDTH   115
#define WHEELRADIUS   34

// CALIB
#define LIGHTRANGE_MIN     20
#define HOVER_ANGSPEED -15000
#define HOVER_DELAY         5


// DEBUG
#define DEBUG_UART   0
#define DEBUG_BT     1
#define DEBUG_LCD    2
#define DEBUG_STDOUT 3
#define DEBUG_STDERR 4
#define DEBUG_IO     DEBUG_LCD

