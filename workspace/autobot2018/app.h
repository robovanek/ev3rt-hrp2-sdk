#pragma once

#include <stdint.h>

#define MAIN_STACK_SIZE  4096
#define MAIN_PRIORITY  (TMIN_APP_TPRI + 0)

extern void main_task  (intptr_t arg);
extern void batt_task  (intptr_t arg);
