#include "header.h"

//
// main entry point
//

void main_task(intptr_t arg) {
	gfx_init();
	gfx_led(LED_RED);
	ev3_sta_cyc(BATT_CYC);
	snd_init();
	log_init();
	output_init();
	input_init();
	ctrl_init();

	//gfx_start("calibrate");
	gfx_led(LED_RED);
	ctrl_calibrate_loop();

	gfx_start("start");
	gfx_led(LED_GREEN);
	ctrl_loop();

	os_exit();
}

void batt_task(intptr_t arg) {
	gfx_batt_tick();
}
