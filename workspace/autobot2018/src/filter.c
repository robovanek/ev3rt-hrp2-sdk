#include "header.h"

int32_t dfilt(int32_t var_z0, diff_filt_t *p) {
	int32_t diff;
	int32_t acc;
	
	// simple differentiator
	diff = var_z0 - p->var_z1;
	p->var_z1 = var_z0;
	// FIR smoother
	memmove(p->diff_z + 1, p->diff_z + 0, sizeof(p->diff_z) - sizeof(p->diff_z[0]));
	p->diff_z[0] = diff;
	acc = p->diff_z[0] *  7;
	acc = p->diff_z[1] * 10;
	acc = p->diff_z[2] *  7;
	acc = p->diff_z[3] *  3;
	acc /= 9;
	return acc;
}
