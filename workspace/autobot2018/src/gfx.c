#include "header.h"


static int32_t fnt_width, fnt_height, top_height;

static int32_t stry(int32_t row);
static int32_t strx(int32_t col);
static void setfont(lcdfont_t font);

void gfx_led(ledcolor_t color) {
	ev3_led_set_color(color);
}

// ER ev3_font_get_size(lcdfont_t font, int32_t *p_width, int32_t *p_height);
// ER ev3_lcd_draw_string(const char *str, int32_t x, int32_t y);
// ER ev3_lcd_draw_line(int32_t x0, int32_t y0, int32_t x1, int32_t y1);
// ER ev3_lcd_fill_rect(int32_t x, int32_t y, int32_t w, int32_t h, lcdcolor_t color);
//#define EV3_LCD_WIDTH  (178) //!< \~English Width of LCD screen   \~Japanese LCD画面の幅
//#define EV3_LCD_HEIGHT (128) //!< \~English Height of LCD screen  \~Japanese LCD画面の高さ

void gfx_init() {
	setfont(EV3_FONT_MEDIUM);
	top_height = fnt_height + 5;
	gfx_batt_tick();
}



void gfx_string(int32_t x, int32_t y, char *msg) {
	ev3_lcd_draw_string(msg, strx(x), stry(y));
}

void gfx_printf(int32_t x, int32_t y, char *msg, ...) {
	va_list args;
	
	char buffer[32];
	
	va_start(args, msg);
	vsnprintf(buffer, sizeof buffer, msg, args);
	va_end(args);
	
	gfx_string(x, y, buffer);
}

void gfx_clear(int32_t y) {
	int32_t y0 = stry(y);
	int32_t y1 = stry(y + 1) - 1;
	ev3_lcd_fill_rect(0, y0, EV3_LCD_WIDTH, y1, EV3_LCD_WHITE);
}

void gfx_msgbox(const char *str) {
	static const int32_t border = 5;
	setfont(EV3_FONT_SMALL);
	gfx_clear(2);
	gfx_clear(3);
	gfx_clear(4);
	
	int32_t len = strlen(str);
	int32_t pixw = len * fnt_width;
	int32_t dx = (EV3_LCD_WIDTH - pixw) / 2;
	
	int32_t xf = dx;
	int32_t yf = stry(3);
	
	
	int32_t x0 = xf                  - border;
	int32_t x1 = xf + pixw           + border;
	int32_t y0 = yf                  - border;
	int32_t y1 = yf + fnt_height - 1 + border;
	
	ev3_lcd_draw_string(str, xf, yf);
	ev3_lcd_draw_line(x0, y0, x1, y0);
	ev3_lcd_draw_line(x0, y1, x1, y1);
	ev3_lcd_draw_line(x0, y0, x0, y1);
	ev3_lcd_draw_line(x1, y0, x1, y1);
	setfont(EV3_FONT_MEDIUM);
}

void gfx_msgbox_clear(void) {
	setfont(EV3_FONT_SMALL);
	gfx_clear(2);
	gfx_clear(3);
	gfx_clear(4);
	setfont(EV3_FONT_MEDIUM);
}

void gfx_start(const char *msg) {
	char buf[20];
	snprintf(buf, sizeof(buf), "=> %s <=", msg);
	
	gfx_led(LED_ORANGE);
	gfx_msgbox(buf);
	snd_beep_ok();
	while (!ev3_button_is_pressed(ENTER_BUTTON)) {
		tslp_tsk(1);
	}
	gfx_msgbox_clear();
	gfx_led(LED_OFF);
}

int32_t stry(int32_t row) {
	return row * fnt_height + top_height;
}

int32_t strx(int32_t col) {
	return col * fnt_width;
}

void gfx_batt_tick() {
	char data[18];
	int mv = ev3_battery_voltage_mV();
	int ma = ev3_battery_current_mA();

	snprintf(data, sizeof(data), "%4d mA  %5d mV", ma, mv);
	
	ev3_lcd_draw_string(data, 4, 0);
	int32_t y = fnt_height + 2;
	ev3_lcd_draw_line(0, y, EV3_LCD_WIDTH, y);
}

void setfont(lcdfont_t font) {
	ev3_lcd_set_font(font);
	ev3_font_get_size(font, &fnt_width, &fnt_height);
}
