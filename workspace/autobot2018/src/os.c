#include "header.h"

// let's fly away
// what about nasal demons, huehuehue

void os_exit(void) {
	return os_fail("Bye bye...");
}

void os_fail(const char *msg) {
	gfx_msgbox(msg);
	snd_beep_error();
	while (true) {
		tslp_tsk(1);
	}
}
