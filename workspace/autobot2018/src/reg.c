#include "header.h"


void reg_integrate(reg_t *r) {
	int32_t sum = r->s.integral * r->p.coef_i_forget / 10  + r->s.error / 10;

	if (sum < -r->p.coef_i_bound) {
		sum = -r->p.coef_i_bound;
	} else
	if (sum > +r->p.coef_i_bound) {
		sum = +r->p.coef_i_bound;
	}
	r->s.integral = sum;
}

void reg_differentiate(reg_t *r) {
	r->s.derivative = dfilt(r->s.error, &r->s.df);
}

void reg_process(reg_t *r) {
	r->s.error = r->p.setpoint - r->i.val;
	r->s.error *= r->p.sign;
	
	reg_integrate(r);
	reg_differentiate(r);
	
	int32_t eff;
	eff  = r->p.coef_p   * r->s.error;
	eff += r->p.coef_i   * r->s.integral;
	eff += r->p.coef_d   * r->s.derivative;
	eff /= r->p.den;
	
	r->o.current = r->i.curO;
	r->o.fix     = eff;
}

void reg_reset_one(reg_t *p) {
	p->s.integral = 0;
}

void reg_reset(rstack_t *p) {
	reg_reset_one(&p->veh);
}
