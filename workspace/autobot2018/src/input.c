#include "header.h"

input_io_t inmem;

void init_safe(sensor_port_t port, sensor_type_t type);
void input_fetch();
void input_scale();
int32_t light_normalize(int16_t raw);

void init_safe(sensor_port_t port, sensor_type_t type) {
	char msg[15];
	ER ercd = ev3_sensor_config(port, type);
	
	while(ercd != E_OK) {
		snprintf(msg, sizeof(msg), "s[%d] E=%d", port, ercd);
		gfx_msgbox(msg);
		snd_beep_error();
		tslp_tsk(500);
		ercd = ev3_sensor_config(port, type);
	}
}

void input_init() {
	init_safe(SEN_COLOR, COLOR_SENSOR);
	input_sample();
}


void input_fetch() {
	int16_t light;
	int     wsleft, wsright;
	ER er = 0;

	er      += ev3_motor_get_velocity(MOT_LEFT,  &wsleft);
	er      += ev3_motor_get_velocity(MOT_RIGHT, &wsright);
	light    = ev3_color_sensor_get_reflect(SEN_COLOR);

	inmem.raw.wsleft  = wsleft * MOT_SIGN;
	inmem.raw.wsright = wsright * MOT_SIGN;
	inmem.raw.light   = light;

	if (er != 0) {
		snd_beep_error();
	}
}

void input_scale() {
	inmem.data.midspeed    = (inmem.raw.wsleft + inmem.raw.wsright) * 500;
	inmem.data.midrotation = wheels2ang(inmem.raw.wsleft, inmem.raw.wsright);
	inmem.data.light = light_normalize(inmem.raw.light);
}

void input_sample() {
	input_fetch();
	input_scale();
}

int32_t light_normalize(int16_t raw) {
	int32_t max_sh = inmem.zero.light_max - inmem.zero.light_min;
	int32_t val_sh = raw                  - inmem.zero.light_min;
	int32_t ratio = val_sh * 1000 / max_sh;
	// fixup table later
	return ratio;
}

actbtn input_buttons() {
	if (ev3_button_is_pressed(BACK_BUTTON))
		return ACTIVATE_STOP;
	if (ev3_button_is_pressed(ENTER_BUTTON))
		return ACTIVATE_ENTER;
	if (ev3_button_is_pressed(LEFT_BUTTON))
		return ACTIVATE_LEFT;
	if (ev3_button_is_pressed(RIGHT_BUTTON))
		return ACTIVATE_RIGHT;
	return ACTIVATE_NONE;
}
