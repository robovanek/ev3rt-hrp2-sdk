#include "header.h"

int32_t wheels2ang(int32_t left, int32_t right) {
	return (right - left) * 1000 * WHEELS2ANG_NUM / WHEELS2ANG_DEN;
}

void param2wheels(int32_t middle,
                  int32_t angle,
                  int8_t *left,
                  int8_t *right) {
	int32_t diff = (angle * ANG2WHEELS_NUM) / ANG2WHEELS_DEN;
	int32_t l = (middle - diff + 500) / 1000;
	int32_t r = (middle + diff + 500) / 1000;

	if (l > +100)
		l = +100;
	if (l < -100)
		l = -100;

	if (r > +100)
		r = +100;
	if (r < -100)
		r = -100;

	*left  = l;
	*right = r;
}

