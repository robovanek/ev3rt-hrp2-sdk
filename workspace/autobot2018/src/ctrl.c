#include "header.h"

/* WORKING
rstack_t normal = {
	.veh = {
		.p = {
			.setpoint      =   500,
			.coef_p        =   140,
			.coef_i        =     0,
			.coef_d        =   600,
			.den           =     1,
			.sign          =    -1,
			.coef_i_bound  = 0x7FFFFFFF,
			.coef_i_forget =          9,
		}
	},
};
const int32_t linspeed = 40;
*/
/* GUUD 2
rstack_t normal = {
	.veh = {
		.p = {
			.setpoint      =   500,
			.coef_p        =   180,
			.coef_i        =     0,
			.coef_d        =  2000,
			.den           =     1,
			.sign          =    -1,
			.coef_i_bound  = 0x00FFFFFF,
			.coef_i_forget =          9,
		}
	},
};
	*/
/* RACE 35 s na kolo
rstack_t normal = {
	.veh = {
		.p = {
			.setpoint      =   500,
			.coef_p        =   180,
			.coef_i        =     0,
			.coef_d        =  1300,
			.den           =     1,
			.sign          =    -1,
			.coef_i_bound  = 0x00FFFFFF,
			.coef_i_forget =          9,
		}
	},
};*/
/* RACE++ 26 s + stable
rstack_t normal = {
	.veh = {
		.p = {
			.setpoint      =   550,
			.coef_p        =   180,
			.coef_i        =     0,
			.coef_d        =  1200,
			.den           =     1,
			.sign          =    -1,
			.coef_i_bound  = 0x00FFFFFF,
			.coef_i_forget =          9,
		}
	},
};*/
/*

rstack_t normal = {
	.veh = {
		.p = {
			.setpoint      =   500,
			.coef_p        =   200,
			.coef_i        =     0,
			.coef_d        =  2000,
			.den           =     1,
			.sign          =    -1,
			.coef_i_bound  = 0x00FFFFFF,
			.coef_i_forget =          9,
		}
	},
};
*//*
rstack_t normal = {
	.veh = {
		.p = {
			.setpoint      =   500,
			.coef_p        =   320,
			.coef_i        =     0,
			.coef_d        =  2000,
			.den           =     1,
			.sign          =    -1,
			.coef_i_bound  = 0x00FFFFFF,
			.coef_i_forget =          9,
		}
	},
};*//*
rstack_t normal = {
	.veh = {
		.p = {
			.setpoint      =   500,
			.coef_p        =   360,
			.coef_i        =     0,
			.coef_d        =  2000,
			.den           =     1,
			.sign          =    -1,
			.coef_i_bound  = 0x00FFFFFF,
			.coef_i_forget =          9,
		}
	},
};*/

rstack_t normal = {
	.veh = {
		.p = {
			.setpoint      =   500,
			.coef_p        =    80,
			.coef_i        =     0,
			.coef_d        =   100,
			.den           =     1,
			.sign          =    -1,
			.coef_i_bound  = 0x00FFFFFF,
			.coef_i_forget =          9,
		}
	},
};
const int32_t linspeed = 60;

void ctrl_init() {
}

void lpass_init() {
	inmem.zero.light_max = SHRT_MIN;
	inmem.zero.light_min = SHRT_MAX;
}

void lpass_sample() {
	int16_t l = inmem.raw.light;
	//printf("%d\n", l);
	if (inmem.zero.light_min > l) {
		inmem.zero.light_min = l;
	}
	if (inmem.zero.light_max < l) {
		inmem.zero.light_max = l;
	}
}

bool_t lpass_finished() {
	int16_t now = inmem.raw.light;
	int16_t min = inmem.zero.light_min;
	int16_t max = inmem.zero.light_max;
	int16_t avg = (max + min) / 2;
	int32_t dif = max - min;
	
	return now > avg && dif > LIGHTRANGE_MIN;
}

void ctrl_calibrate_loop() {
	// we're on max
	// start hovering to determine min
	// after we get to average between min and max, stop
	// then stay at the line side

	lpass_init();

	outmem.linspeed = 0;
	outmem.angspeed = HOVER_ANGSPEED;

	output_start();
	do {
		input_sample();
		lpass_sample();
		tslp_tsk(HOVER_DELAY);

	} while (!lpass_finished());
	output_stop();

	fprintf(rmt, "COL MIN: %d\n",  inmem.zero.light_min);
	fprintf(rmt, "COL MAX: %d\n",  inmem.zero.light_max);
}

void ctrl_loop() {
	rstack_t *rmem = &normal;

	output_start();

	rmem->veh.i.curO = 0;
	outmem.linspeed = linspeed * 1000;

	while (true) {
		input_sample();

		rmem->veh.i.val = inmem.data.light;
		reg_process(&rmem->veh);
		reg_push_turn(&rmem->veh);
		output_apply();

		//static int i = 0;
		//if (i > 10) {
		//	i = 0;
			//fprintf(rmt, "P %8ld  I %8ld  D %8ld  L %8ld  O %8ld\n",
			//    rmem->veh.p.coef_p * rmem->veh.s.error      / rmem->veh.p.den,
			//    rmem->veh.p.coef_i * rmem->veh.s.integral   / rmem->veh.p.den,
			//    rmem->veh.p.coef_d * rmem->veh.s.derivative / rmem->veh.p.den,
			//    inmem.data.light,
			//    rmem->veh.o.fix);
		//} else {
		//	i++;
		//}

		tslp_tsk(REG_DELAY_MSEC);
	}

	output_stop();
}
