#include "header.h"


#define MASK_LEFT  (1 << MOT_LEFT)
#define MASK_RIGHT (1 << MOT_RIGHT)
#define MASK_STEER (MASK_LEFT | MASK_RIGHT)
#define MASK_ALL   (MASK_STEER)

oblock_t outmem;

void output_init() {
	char msg[20];
	motor_type_t types[4] = {NONE_MOTOR, NONE_MOTOR, NONE_MOTOR, NONE_MOTOR};
	types[MOT_LEFT]  = LARGE_MOTOR;
	types[MOT_RIGHT] = LARGE_MOTOR;
	ER er;
	while ((er = ev3_motor_config_all(types)) != E_OK) {
		snd_beep_error();
		snprintf(msg, sizeof(msg), "motinit fail %d", er);
		gfx_msgbox(msg);
		tslp_tsk(500);
	}
	er = ev3_motor_ex_reset_counts(MASK_ALL);
	if (er != E_OK) {
		snd_beep_error();
		gfx_msgbox("reset tacho failed");
	}
}

void output_start() {
	output_apply();
	ER er = ev3_motor_ex_start(MASK_STEER);
	if (er != E_OK) {
		snd_beep_error();
	}
}

void output_stop() {
	ER er = ev3_motor_ex_stop(MASK_STEER, true);
	if (er != E_OK) {
		snd_beep_error();
	}
}

void output_apply() {
	int8_t left, right;
	ER er = 0;

	param2wheels(outmem.linspeed,
	             outmem.angspeed,
	             &left, &right);
	er += ev3_motor_ex_setspeed(MASK_LEFT,  left  * MOT_SIGN);
	er += ev3_motor_ex_setspeed(MASK_RIGHT, right * MOT_SIGN);

	if (er != 0) {
		snd_beep_error();
	}
}
