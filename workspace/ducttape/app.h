#pragma once

#include <stdint.h>


#define MAIN_STACK_SIZE  16384
#define MAIN_PRIORITY  (TMIN_APP_TPRI + 0)

#ifdef __cplusplus
extern "C" {
#endif

extern void main_task (intptr_t arg);

#ifdef __cplusplus
}
#endif
