#!/bin/bash

# https://stackoverflow.com/a/29613573
# SYNOPSIS
#  quoteSubst <text>
quoteSubst() {
  IFS= read -d '' -r < <(sed -e ':a' -e '$!{N;ba' -e '}' -e 's/[&/\]/\\&/g; s/\n/\\&/g' <<<"$1")
  printf %s "${REPLY%$'\n'}"
}

cd "$( dirname "${BASH_SOURCE[0]}" )"

cp ./Makefile.base ./Makefile.inc
cp ./app.base ./app.cfg

# MAKE CXX OBJECTS
find -L . -iregex '^.*\.cpp$' |
    egrep -o '[^/]+\.cpp' |
    sed -E 's/^/    /;s/\.cpp/\.o \\/' |
    grep -v app.o |
    while read -r line
do
    replacement="$(quoteSubst "$line")"
    sed -e "s/##CXXOBJ##/$replacement\n##CXXOBJ##/g" -i ./Makefile.inc
done
sed -e "s/##CXXOBJ##/\n/g" -i ./Makefile.inc

# MAKE C OBJECTS
find -L . -iregex '^.*\.c$' |
    egrep -o '[^/]+\.c' |
    sed -E 's/^/    /;s/\.c/\.o \\/' |
    while read -r line
do
    replacement="$(quoteSubst "$line")"
    sed  -e "s/##COBJ##/$replacement\n##COBJ##/g" -i ./Makefile.inc
done
sed -e "s/##COBJ##/\n/g" -i ./Makefile.inc

# CONFIG OBJECTS
find -L . -iregex '^.*\.c$' -o -iregex '^.*\.cpp$' |
    sed -E 's|^.*?/(.*)$|\1|; s/\.c.*/.o");/; s/^/ATT_MOD("/' |
    while read -r line
do
    replacement="$(quoteSubst "$line")"
    sed -e "s|//CFGOBJ//|$replacement\n//CFGOBJ//|g" -i ./app.cfg
done
sed -e "s|//CFGOBJ//|\n|g" -i ./app.cfg
