//
// Created by kuba on 21.9.18.
//

#include <string.h>
#include "store.hpp"

static roboconfino::lockable elem_mtx;

roboconfino::element::element()
        : //m_lock(create_mutex()),
        m_value("0"),
        m_type(cf::type::boolean),
        m_master_version(0) {}

roboconfino::element::element(cf::type type, std::string &&dfl)
        : //m_lock(create_mutex()),
        m_value(std::move(dfl)),
        m_type(type),
        m_master_version(0) {}

int roboconfino::element::store(std::string &&value) {
    if (!elem_mtx)
        elem_mtx = roboconfino::create_mutex();
    scoped_lock guard(elem_mtx);
    m_master_version++;
    m_value = std::move(value);
    return m_master_version;
}

roboconfino::cf::type roboconfino::element::get_type() const {
    return m_type;
}

bool roboconfino::element::update(int &currentVersion, std::string &newValue) const {
    if (!elem_mtx)
        elem_mtx = roboconfino::create_mutex();
    scoped_lock guard(elem_mtx);
    if (m_master_version <= currentVersion) {
        return false;
    }
    newValue = m_value;
    currentVersion = m_master_version;
    return true;
}

std::string roboconfino::element::load(int &version) const {
    if (!elem_mtx)
        elem_mtx = roboconfino::create_mutex();
    scoped_lock guard(elem_mtx);
    version = m_master_version;
    return m_value;
}

roboconfino::element_ref roboconfino::store::get(const std::string &name, cf::type type, std::string initializer) {
    if (!elem_mtx)
        elem_mtx = roboconfino::create_mutex();
    scoped_lock guard(elem_mtx);
    auto it = m_store.find(name);
    if (it == m_store.end()) {
        element_ref newRef = std::make_shared<element>(type, std::move(initializer));
        m_store.insert(std::make_pair(name, newRef));
        return newRef;
    } else {
        return it->second;
    }
}

roboconfino::element_ref roboconfino::store::get(const std::string &name) {
    return get(name, cf::type::boolean, "0");
}

std::vector<std::string> roboconfino::store::get_names() const {
    std::vector<std::string> names;
    if (!elem_mtx)
        elem_mtx = roboconfino::create_mutex();
    scoped_lock guard(elem_mtx);
    for (auto &&pair : m_store) {
        names.push_back(pair.first);
    }
    return names;
}
