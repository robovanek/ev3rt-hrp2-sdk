//
// Created by kuba on 7.10.18.
//
#ifdef ROBOCONFINO_PC

#include <integration.hpp>
#include <semaphore.h>
#include <config.hpp>

namespace roboconfino {
    class posix_mutex : public lockable_impl {
    public:
        posix_mutex() : m_mutex() {
            pthread_mutex_init(&m_mutex, nullptr);
        }

        ~posix_mutex() {
            pthread_mutex_destroy(&m_mutex);
        }

        posix_mutex(const posix_mutex &other) = delete;

        posix_mutex(posix_mutex &&other) = delete;

        posix_mutex &operator=(const posix_mutex &other) = delete;

        posix_mutex &operator=(posix_mutex &&other) = delete;

        void lock() override {
            pthread_mutex_lock(&m_mutex);
        }

        void unlock() override {
            pthread_mutex_unlock(&m_mutex);
        }

    private:
        pthread_mutex_t m_mutex;
    };

    class posix_semaphore : public semaphore_impl {
    public:
        posix_semaphore() : m_sem() {
            sem_init(&m_sem, 0, 0);
        }

        ~posix_semaphore() {
            sem_destroy(&m_sem);
        }

        posix_semaphore(const posix_semaphore &other) = delete;

        posix_semaphore(posix_semaphore &&other) = delete;

        posix_semaphore &operator=(const posix_semaphore &other) = delete;

        posix_semaphore &operator=(posix_semaphore &&other) = delete;

        void give() override {
            sem_post(&m_sem);
        }

        void take() override {
            sem_wait(&m_sem);
        }

        int probe() override {
            int count = 0;
            sem_getvalue(&m_sem, &count);
            return count;
        }

    private:
        sem_t m_sem;
    };

    // to be implemented by program
    lockable create_critical() {
        return std::make_unique<posix_mutex>();
    }

    lockable create_mutex() {
        return std::make_unique<posix_mutex>();
    }

    semaphore create_semaphore() {
        return std::make_unique<posix_semaphore>();
    }

    // get tick counter for debug timestamps
    int64_t get_tick() {
        timespec time = {};
        clock_gettime(CLOCK_MONOTONIC, &time);
        int64_t msec = time.tv_nsec / (1000 * 1000) + time.tv_sec * (1000);
        return msec;
    }

    bool read_file(const std::string &path, std::vector<char> &data) {
        std::vector<cf::byte> temp(STDIO_READ_LEN, 0);
        data.clear();
        FILE *fp = fopen(path.c_str(), "rb");
        if (fp == nullptr) {
            return false;
        }
        for (;;) {
            size_t now = fread(temp.data(), sizeof(char), STDIO_READ_LEN, fp);
            data.insert(data.end(), temp.begin(), temp.begin() + now);
            if (now < STDIO_READ_LEN) {
                break;
            }
        }
        fclose(fp);
        return true;
    }

    bool write_file(const std::string &path, const char *data, size_t length) {
        FILE *fp = fopen(path.c_str(), "wb");
        if (fp == nullptr) {
            return false;
        }
        size_t written = 0;
        while (written < length) {
            const char *pos = data + written;
            size_t remaining = length - written;
            written += fwrite(pos, sizeof(char), std::min(STDIO_WRITE_LEN, remaining), fp);
        }
        fclose(fp);
        return true;
    }

    int start_thread(thread_entrypoint entry, int priority, intptr_t arg) {
        return -1;
    }

    void stop_thread(int id) {

    }
}
#endif
