//
// Created by kuba on 22.9.18.
//

#include "integration.hpp"

roboconfino::scoped_lock::scoped_lock(roboconfino::lockable &sect)
        : m_lock(sect.get()) {
    m_lock->lock();
}

roboconfino::scoped_lock::~scoped_lock() {
    m_lock->unlock();
}
