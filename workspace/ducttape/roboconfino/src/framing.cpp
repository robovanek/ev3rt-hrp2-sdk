//
// Created by kuba on 7.10.18.
//

#include "framing.hpp"

#ifdef __clang__
#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnusedValue"
#endif
namespace roboconfino {

    InputQueue::InputQueue(InputBarrier &next) : m_next(&next) {}

    std::vector<cf::byte> InputQueue::take(size_t count) {
        size_t avail = available();
        count = std::min(count, avail);

        std::vector<cf::byte> retval;
        retval.reserve(count);
        for (size_t i = 0; i < count; i++) {
            retval.push_back(m_queue.front());
            m_queue.pop();
        }
        return retval;
    }

    size_t InputQueue::available() const {
        return m_queue.size();
    }

    void InputQueue::reset() {
        std::queue<uint8_t>().swap(m_queue);
    }

    void InputQueue::pushData(std::vector<uint8_t> &received) {
        for (auto c : received) {
            m_queue.push(c);
        }
        m_next->newData(available());
    }

    InputBarrier::InputBarrier(InputQueue &queue, MessageReader &read, MessageWriter &write)
            : m_queue(&queue), m_next_read(&read), m_next_write(&write) {}

    void InputBarrier::reset() {
        m_state = ReadState::ReceivingCommand;
        m_length = 0;
    }

    void InputBarrier::newData(size_t total) {
        while (true) {
            ReadState newState = m_state;
            size_t newLength = m_length;

            switch (m_state) {
                case ReadState::ReceivingCommand: {
                    if (total < 2) {
                        return;
                    }
                    total -= 2;
                    roboconfino::convert_u16 len;
                    memcpy(len.array, m_queue->take(2).data(), 2);
                    len.loadFromArray();

                    if (len.integer == 0) {
                        newState = ReadState::ReceivingCommand;
                        newLength = 0;
                        m_next_write->receivedAck();
                    } else {
                        newState = ReadState::ReceivingData;
                        newLength = len.integer;
                    }
                }
                    break;
                case ReadState::ReceivingData: {
                    if (total < m_length) {
                        return;
                    }
                    total -= m_length;
                    newState = ReadState::ReceivingCommand;
                    newLength = 0;
                    m_next_read->receivedPacket(m_queue->take(m_length));
                }
                    break;
            }

            m_state = newState;
            m_length = newLength;
        }
    }

    MessageReader::MessageReader(MessageWriter &write, GenericChannel &chan)
            : m_next_write(&write), m_next_chan(&chan) {}

    void MessageReader::reset() {
        m_reassembly.clear();
        m_size = 0;
        m_state = ReassemblyState::ReadingHeader;
    }

    void MessageReader::receivedPacket(const std::vector<cf::byte> &data) {
        m_reassembly.insert(m_reassembly.end(), data.begin(), data.end());
        roboconfino::convert_u16 ack(0);
        m_next_chan->writeDirect(ack.array, 2);

        while (true) {
            ReassemblyState newState = m_state;
            size_t newSize = m_size;

            switch (m_state) {
                case ReassemblyState::ReadingHeader: {
                    if ((size_t) m_reassembly.size() < roboconfino::datagram_tlv::header_length) {
                        return;
                    }
                    newState = ReassemblyState::ReadingData;
                    newSize = datagram_tlv::take_length(m_reassembly.data())
                              + datagram_tlv::header_length;
                }
                    break;
                case ReassemblyState::ReadingData: {
                    if ((size_t) m_reassembly.size() < m_size) {
                        return;
                    }
                    newState = ReassemblyState::ReadingHeader;
                    newSize = 0;

                    std::vector<cf::byte> mid(m_reassembly.begin(), m_reassembly.begin() + m_size);
                    m_next_chan->receivedMessage(mid);
                    m_next_write->receivedMessage(mid);
                    m_reassembly.erase(m_reassembly.begin(), m_reassembly.begin() + m_size);
                }
                    break;
            }

            m_state = newState;
            m_size = newSize;
        }
    }

    MessageWriter::MessageWriter(GenericChannel &writer)
            : m_next_channel(&writer) {
    }

    void MessageWriter::reset() {
        m_state = WriteState::WaitingForUser;
        m_current.clear();
        std::queue<std::vector<cf::byte>>().swap(m_incoming);
    }

    void MessageWriter::writeMessage(std::vector<cf::byte> data) {
        size_t len = data.size() - datagram_tlv::header_length;
        datagram_tlv::install_length(data, static_cast<datagram_tlv::message_length>(len));

        if (m_state == WriteState::WaitingForUser) {
            m_current = data;
            sendNext();
        } else {
            m_incoming.push(std::move(data));
        }
    }

    void MessageWriter::receivedMessage(const std::vector<cf::byte> &data) {
        if (m_incoming.empty()) {
            m_state = WriteState::WaitingForUser;
        } else {
            m_current = m_incoming.front();
            m_incoming.pop();
            sendNext();
        }
    }

    void MessageWriter::receivedAck() {
        if (m_current.empty()) {
            m_state = WriteState::WaitingForReceive;
        } else {
            sendNext();
        }
    }

    void MessageWriter::sendNext() {
        size_t now = std::min(m_current.size(), static_cast<size_t>(BT_FRAME_MAX));

        roboconfino::convert_u16 len((uint16_t) now);
        m_next_channel->writeDirect(len.array, 2);
        m_next_channel->writeDirect(m_current.data(), now);
        m_current.erase(m_current.begin(), m_current.begin() + now);

        m_state = WriteState::WaitingForAck;
    }
}

#ifdef __clang__
#pragma clang diagnostic pop
#endif
