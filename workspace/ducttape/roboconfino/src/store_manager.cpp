//
// Created by kuba on 22.9.18.
//
#include "converter.hpp"
#include "store_manager.hpp"

roboconfino::store_manager::store_manager(store &store)
        : m_store(&store) {
    debug("exporter init");
}

std::string roboconfino::store_manager::do_export() {
    std::vector<std::string> lines;

    auto &&names = m_store->get_names();
    for (auto &&name : names) {
        element_ref elem = m_store->get(name);
        file_line output{name, elem};
        if (output.valid) {
            write(std::move(output), lines);
        }
    }
    size_t total = 0;
    for (auto &str : lines) {
        total += str.size();
    }
    total += lines.size();
    std::string complete(total, 0);
    auto outIt = complete.begin();
    for (auto &str : lines) {
        outIt = std::copy(str.begin(), str.end(), outIt);
        *(outIt++) = '\n';
    }
    return complete;
}

void roboconfino::store_manager::do_import(const std::string &str) {
    auto lnBeg = str.begin();
    auto lnEnd = lnBeg;

    do {
        lnBeg = lnEnd + 1;
        lnEnd = std::find(lnBeg, str.end(), '\n');
        std::string line(lnBeg, lnEnd);
        // process line

        file_line e = read(line);
        if (e.valid) {
            m_store->get(e.name, type_from_name(e.type), "")->store(std::move(e.value));
        }
    } while (lnEnd != str.end());
}

void roboconfino::store_manager::write(roboconfino::store_manager::file_line &&what, std::vector<std::string> &where) {
    if (what.valid) {
        where.push_back(what.type + ":" + what.name + ":" + what.value);
    }
}

roboconfino::store_manager::file_line roboconfino::store_manager::read(const std::string &line) {
    if (line.length() == 0) {
        return file_line{};
    }
    size_t split1 = line.find_first_of(':');
    size_t split2 = line.find_first_of(':', split1 + 1);
    if (split1 == std::string::npos || split2 == std::string::npos) {
        return file_line{};
    } else {
        std::string &&type = line.substr(0, split1);
        std::string &&key = line.substr(split1 + 1,
                                        split2 - split1 - 1);
        std::string &&value = line.substr(split2 + 1);
        return file_line{type, key, value};
    }
}

roboconfino::store_manager::file_line::file_line()
        : type("invalid"), name("error"), value("0"), valid(false) {

}

roboconfino::store_manager::file_line::file_line(const std::string &type, const std::string &name,
                                                 const std::string &value)
        : type(type), name(name), value(value), valid(false) {
    valid = type_from_name(type) != cf::type::invalid;
}

roboconfino::store_manager::file_line::file_line(const std::string &name, const roboconfino::element_ref &ref)
        : type(type_to_name(ref->get_type())), name(name), value(ref->load()) {
    valid = ref->get_type() != cf::type::invalid;
}
