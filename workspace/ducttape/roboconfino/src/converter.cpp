//
// Created by kuba on 5.10.18.
//

#include "converter.hpp"

namespace roboconfino {

    const char *type_to_name(cf::type t) {
        switch (t) {
            default:
                return "invalid";
            case cf::type::boolean:
                return "boolean";
            case cf::type::integer:
                return "integer";
            case cf::type::floating:
                return "floating";
            case cf::type::string:
                return "string";
            case cf::type::binary:
                return "binary";
        }
    }

    cf::type type_from_name(std::string name) {
        std::transform(name.begin(), name.end(), name.begin(), ::tolower);
        if (name == "boolean") {
            return cf::type::boolean;
        } else if (name == "integer") {
            return cf::type::integer;
        } else if (name == "floating") {
            return cf::type::floating;
        } else if (name == "string") {
            return cf::type::string;
        } else if (name == "binary") {
            return cf::type::binary;
        } else {
            return cf::type::invalid;
        }
    }
}
