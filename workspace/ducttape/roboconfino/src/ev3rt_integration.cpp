//
// Created by kuba on 7.10.18.
//

#ifndef ROBOCONFINO_PC
#define ROBOCONFINO_BRICK
#endif

#ifdef ROBOCONFINO_BRICK

#include <t_stddef.h>
#include <t_stdlib.h>
#include "integration.hpp"
#include "kernel.h"
#include "config.hpp"
#include "module_cfg.h"

namespace roboconfino {

    static ID user_mutexes[8] = {
            USER_MTX1,
            USER_MTX2,
            USER_MTX3,
            USER_MTX4,
            USER_MTX5,
            USER_MTX6,
            USER_MTX7,
            USER_MTX8,
    };
    static ID mtx_counter = 0;
    static constexpr ID no_mtx = sizeof(user_mutexes) / sizeof(user_mutexes[0]);

    static ID user_semaphores[8] = {
            USER_SEM1,
            USER_SEM2,
            USER_SEM3,
            USER_SEM4,
            USER_SEM5,
            USER_SEM6,
            USER_SEM7,
            USER_SEM8,
    };
    static ID sem_counter = 0;
    static constexpr ID no_sem = sizeof(user_semaphores) / sizeof(user_semaphores[0]);


    class ev3rt_mutex : public lockable_impl {
    public:
        ev3rt_mutex() : m_object(0) {
            /*
            T_CMTX param;
            param.ceilpri = 0;
            param.mtxatr = TA_NULL;
            ER_ID sth = acre_mtx(&param);
            if (sth < 0) {
                debug("ERROR: failed to create mutex: ");
                debug(itron_strerror(sth));
                debug("\n");
            }
            m_object = sth;
            */
            debug("creating mutex");
            int mtx_idx = mtx_counter++;
            if (mtx_idx >= no_mtx) {
                debug("ERROR: out of mutexes.\n");
                m_object = user_mutexes[no_mtx - 1];
            } else {
                m_object = user_mutexes[mtx_idx];
            }
        }

        ~ev3rt_mutex() {
            //del_mtx(m_object);
        }

        ev3rt_mutex(const ev3rt_mutex &other) = delete;

        ev3rt_mutex(ev3rt_mutex &&other) = delete;

        ev3rt_mutex &operator=(const ev3rt_mutex &other) = delete;

        ev3rt_mutex &operator=(ev3rt_mutex &&other) = delete;

        void lock() override {
            //loc_mtx(m_object);
        }

        void unlock() override {
            //unl_mtx(m_object);
        }

    private:
        ID m_object;
    };

    class ev3rt_critical : public lockable_impl {
    public:
        void lock() override {
            loc_cpu();
        }

        void unlock() override {
            unl_cpu();
        }
    };

    class ev3rt_semaphore : public semaphore_impl {
    public:
        ev3rt_semaphore() : m_object(0) {
            /*
            T_CSEM param;

            ER_ID sth = acre_sem(&param);
            if (sth < 0) {
                debug("ERROR: failed to create semaphore\n");
            }
            m_object = sth;
             */

            int sem_idx = sem_counter++;
            if (sem_idx >= no_sem) {
                debug("ERROR: out of semaphores.\n");
                m_object = user_semaphores[no_sem - 1];
            } else {
                m_object = user_semaphores[sem_idx];
            }
        }

        ~ev3rt_semaphore() {
            //del_sem(m_object);
        }

        ev3rt_semaphore(const ev3rt_semaphore &other) = delete;

        ev3rt_semaphore(ev3rt_semaphore &&other) = delete;

        ev3rt_semaphore &operator=(const ev3rt_semaphore &other) = delete;

        ev3rt_semaphore &operator=(ev3rt_semaphore &&other) = delete;

        void give() override {
            sig_sem(m_object);
        }

        void take() override {
            wai_sem(m_object);
        }

        int probe() override {
            T_RSEM state;
            ref_sem(m_object, &state);
            return state.semcnt;
        }

    private:
        ID m_object;
    };

    // to be implemented by program
    lockable create_critical() {
        return std::make_unique<ev3rt_critical>();
    }

    lockable create_mutex() {
        return std::make_unique<ev3rt_mutex>();
    }

    semaphore create_semaphore() {
        return std::make_unique<ev3rt_semaphore>();
    }

    // get tick counter for debug timestamps
    int64_t get_tick() {
        SYSTIM time;
        get_tim(&time);
        return time;
    }

    void do_sleep(unsigned int msec) {
        dly_tsk(msec);
    }

    bool read_file(const std::string &path, std::vector<char> &data) {
        std::vector<char> temp(STDIO_READ_LEN, 0);
        data.clear();
        FILE *fp = fopen(path.c_str(), "rb");
        if (fp == nullptr) {
            return false;
        }
        for (;;) {
            size_t now = fread(temp.data(), sizeof(char), STDIO_READ_LEN, fp);
            data.insert(data.end(), temp.begin(), temp.begin() + now);
            if (now < STDIO_READ_LEN) {
                break;
            }
        }
        fclose(fp);
        return true;
    }

    bool write_file(const std::string &path, const char *data, size_t length) {
        FILE *fp = fopen(path.c_str(), "wb");
        if (fp == nullptr) {
            return false;
        }
        size_t written = 0;
        while (written < length) {
            const char *pos = data + written;
            size_t remaining = length - written;
            written += fwrite(pos, sizeof(char), std::min(STDIO_WRITE_LEN, remaining), fp);
        }
        fclose(fp);
        return true;
    }

    int start_thread(thread_entrypoint entry, int priority, intptr_t arg) {
        T_CTSK param = {};
        param.tskatr = TA_NULL;
        param.exinf = arg;
        param.task = entry;
        param.itskpri = priority;
        param.stk = nullptr;
        param.stksz = BRICKLOOP_STACK_USER;
        param.sstk = nullptr;
        param.sstksz = BRICKLOOP_STACK_KERNEL;
        return acre_tsk(&param);
    }

    void stop_thread(int id) {
        ter_tsk(id);
        del_tsk(id);
    }

}

#endif
