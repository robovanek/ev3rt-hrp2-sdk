//
// Created by kuba on 21.9.18.
//

#include "protocol.hpp"
#include <cassert>
#include <cstring>

namespace roboconfino {
    template<typename T>
    static T load_uint(const datagram_tlv::message_data &src, size_t idx) {
        if (src.size() < (idx + sizeof(T))) {
            return 0;
        }
        T copy;
        std::memcpy(&copy, src.data() + idx, sizeof(T));
        return copy;
    }

    template<typename T>
    static void write_uint(datagram_tlv::message_data &cont, T value) {
        cont.reserve(cont.size() + sizeof(T));

        uint8_t tmp[sizeof(T)];
        memcpy(tmp, &value, sizeof(T));

        for (size_t i = 0; i < sizeof(T); i++) {
            cont.push_back(tmp[i]);
        }
    }

    template<typename T>
    static datagram_tlv::message_data write_uint(datagram_tlv::message_data &&cont, T value) {
        write_uint(cont, value);
        return std::move(cont);
    }

    static std::string load_string(const datagram_tlv::message_data &src, size_t idx) {
        const char *cstr = reinterpret_cast<const char *>(src.data() + idx);
        return std::string{cstr};
    }

    static void write_string(datagram_tlv::message_data &cont, const std::string &str) {
        cont.reserve(cont.size() + str.length() + 1);

        auto &&out = std::back_inserter(cont);
        std::copy(str.cbegin(), str.cend(), out);

        *out++ = '\0';
    }

    static datagram_tlv::message_data write_string(datagram_tlv::message_data &&cont, const std::string &str) {
        write_string(cont, str);
        return std::move(cont);
    }


    datagram_base::message_data datagram_base::serialize() const {
        size_t base = known_size();
        message_data known = serialize_known();
        known.resize(base + m_payload.size());
        std::copy(m_payload.cbegin(), m_payload.cend(), known.begin() + base);
        return known;
    }

    bool datagram_base::deserialize(const datagram_base::message_data &data) {
        size_t base = known_size();
        if (data.size() < base) {
            return false;
        }
        size_t extra = data.size() - base;
        deserialize_known(data);
        m_payload.resize(extra);
        std::copy(data.begin() + base, data.end(), m_payload.begin());
        return true;
    }

    datagram_tlv::message_data datagram_tlv::serialize_known() const {
        message_data &&result = datagram_base::serialize_known();
        write_uint(result, m_tag);
        write_uint(result, m_length);
        return result;
    }

    void datagram_tlv::deserialize_known(const message_data &data) {
        datagram_base::deserialize_known(data);

        size_t base = datagram_base::known_size();
        m_tag = /*    */ load_uint<message_tag>(data, base);
        m_length = /* */ load_uint<message_length>(data, base + sizeof(m_tag));
    }

    datagram_tlv::datagram_tlv() : datagram_base(), m_tag(0), m_length(0), m_original_tag(INVALID_TAG) {}

    datagram_tlv::datagram_tlv(message_tag tag)
            : datagram_base(), m_tag(tag), m_length(0), m_original_tag(tag) {}

    void datagram_tlv::install_length(datagram_base::message_data &buffer,
                                      datagram_tlv::message_length value) {
        return install_length(buffer.data(), value);
    }

    void datagram_tlv::install_length(cf::byte *buffer,
                                      datagram_tlv::message_length value) {
        memcpy(buffer + sizeof(m_tag), &value, sizeof(message_length));
    }

    datagram_tlv::message_length datagram_tlv::take_length(const datagram_base::message_data &buffer) {
        return take_length(buffer.data());
    }

    datagram_tlv::message_length datagram_tlv::take_length(const cf::byte *buffer) {
        message_length len;
        memcpy(&len, buffer + sizeof(m_tag), sizeof(message_length));
        return len;
    }

    bool datagram_tlv::check_correct_tag() const {
        return m_original_tag == INVALID_TAG || m_tag == m_original_tag;
    }

    datagram_base::datagram_base() = default;

    first_index::first_index(message_tag tag)
            : datagram_tlv(tag), m_idx(0) {}

    datagram_base::message_data first_index::serialize_known() const {
        return write_uint(datagram_tlv::serialize_known(), m_idx);;
    }

    void first_index::deserialize_known(const datagram_base::message_data &data) {
        datagram_tlv::deserialize_known(data);
        m_idx = load_uint<index_type>(data, datagram_tlv::known_size());
    }

    first_string::first_string(message_tag which)
            : datagram_tlv(which), m_name("") {}

    size_t first_string::known_size() const {
        return datagram_tlv::known_size() + m_name.length() + 1;
    }

    datagram_base::message_data first_string::serialize_known() const {
        return write_string(datagram_tlv::serialize_known(), m_name);
    }

    void first_string::deserialize_known(const datagram_base::message_data &raw) {
        datagram_tlv::deserialize_known(raw);
        m_name = load_string(raw, datagram_tlv::known_size());
    }

    second_string::second_string(message_tag tag)
            : first_string(tag), m_value("0") {}

    size_t second_string::known_size() const {
        return first_string::known_size() + m_value.length() + 1;
    }

    datagram_base::message_data second_string::serialize_known() const {
        return write_string(first_string::serialize_known(), m_value);
    }

    void second_string::deserialize_known(const datagram_base::message_data &raw) {
        first_string::deserialize_known(raw);
        m_value = load_string(raw, first_string::known_size());
    }

    template<typename DstT>
    void visit_helper(const datagram_tlv::message_data &raw, datagram_visitor *pVisit) {
        DstT t;
        if (!t.deserialize(raw) || !t.check_correct_tag()) {
            return (*pVisit)(datagram_visitor::fail_reason::bad_cast);
        } else {
            return (*pVisit)(t);
        }
    }

#define SWITCH_TYPE(raw, T) case T::main_tag: return visit_helper<T>((raw), this);

    void datagram_visitor::apply(const datagram_base::message_data &raw) {
        datagram_tlv tlv;

        if (!tlv.deserialize(raw)) {
            return (*this)(fail_reason::bad_cast);
        }
        switch ((cf_ver0_tags) tlv.tag()) {
            SWITCH_TYPE(raw, err)
            SWITCH_TYPE(raw, ack)
            SWITCH_TYPE(raw, request_serial)
            SWITCH_TYPE(raw, configure)
            SWITCH_TYPE(raw, file_to_brick)
            SWITCH_TYPE(raw, file_to_pc)
            SWITCH_TYPE(raw, get_option_all)
            SWITCH_TYPE(raw, get_option)
            SWITCH_TYPE(raw, set_option)
            SWITCH_TYPE(raw, rpc_request)
            SWITCH_TYPE(raw, rpc_list)
            SWITCH_TYPE(raw, reply_string)
            SWITCH_TYPE(raw, reply_bytes)
            default:
                return (*this)(fail_reason::unknown_tag);
        }
    }
}
