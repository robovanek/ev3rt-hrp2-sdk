//
// Created by kuba on 21.9.18.
//

#include <cstdarg>
#include <sstream>
#include <utility>
#include <roboconfino/include/serial.hpp>


#include "config.hpp"
#include "serial.hpp"
#include "integration.hpp"

static roboconfino::lockable ser_mtx;

namespace roboconfino {
    std::string vformatn(const char *format, va_list va) {
        va_list args1, args2;
        va_copy(args1, va);
        va_copy(args2, va);

        int bytes = std::vsnprintf(nullptr, 0, format, args1) + 1;
        if (bytes < 0) {
            return "std::vsnprintf returned negative value";
        }
        va_end(args1);

        std::string result(bytes, 0);
        std::vsnprintf(&result.front(), result.size(), format, args2);
        result.erase(result.size() - 1);
        va_end(args2);

        return result;
    }

    std::string formatn(const char *format, ...) {
        va_list va;
        va_start(va, format);
        std::string result = vformatn(format, va);
        va_end(va);
        return result;
    }

    serial_logger::serial_logger(std::string name,
                                 serial_buffer &backend)
            : m_component(std::move(name)), m_backend(&backend) {}

    void serial_logger::debug(const std::string &text) {
        return m_backend->log(severity_debug, m_component, text, time());
    }

    void serial_logger::info(const std::string &text) {
        return m_backend->log(severity_info, m_component, text, time());
    }

    void serial_logger::note(const std::string &text) {
        return m_backend->log(severity_note, m_component, text, time());
    }

    void serial_logger::warn(const std::string &text) {
        return m_backend->log(severity_warn, m_component, text, time());
    }

    void serial_logger::error(const std::string &text) {
        return m_backend->log(severity_error, m_component, text, time());
    }

    void serial_logger::fatal(const std::string &text) {
        return m_backend->log(severity_fatal, m_component, text, time());
    }

    void serial_logger::debug(const char *format, ...) {
        if (severity_debug >= m_backend->get_verbosity()) {
            va_list va;
            va_start(va, format);
            debug(vformatn(format, va));
            va_end(va);
        }
    }

    void serial_logger::info(const char *format, ...) {
        if (severity_info >= m_backend->get_verbosity()) {
            va_list va;
            va_start(va, format);
            info(vformatn(format, va));
            va_end(va);
        }
    }

    void serial_logger::note(const char *format, ...) {
        if (severity_note >= m_backend->get_verbosity()) {
            va_list va;
            va_start(va, format);
            note(vformatn(format, va));
            va_end(va);
        }
    }

    void serial_logger::warn(const char *format, ...) {
        if (severity_warn >= m_backend->get_verbosity()) {
            va_list va;
            va_start(va, format);
            warn(vformatn(format, va));
            va_end(va);
        }
    }

    void serial_logger::error(const char *format, ...) {
        if (severity_error >= m_backend->get_verbosity()) {
            va_list va;
            va_start(va, format);
            error(vformatn(format, va));
            va_end(va);
        }
    }

    void serial_logger::fatal(const char *format, ...) {
        if (severity_fatal >= m_backend->get_verbosity()) {
            va_list va;
            va_start(va, format);
            fatal(vformatn(format, va));
            va_end(va);
        }
    }

    std::string serial_logger::time() {
        return std::to_string(get_tick());
    }

    static const char *severity_strings[] = {
            [severity_debug] /**/ = "<debug> ",
            [severity_info]  /**/ = "<info > ",
            [severity_note]  /**/ = "<note > ",
            [severity_warn]  /**/ = "<warn > ",
            [severity_error] /**/ = "<error> ",
            [severity_fatal] /**/ = "<fatal> ",
    };

    void
    serial_buffer::log(severity level, const std::string &component, const std::string &text, const std::string &time) {
        if (level >= m_minimal_level) {
            std::ostringstream ost{};
            ost << "[" << time << "]" << severity_strings[level];
            ost << component << ": " << text << std::endl;
            if (!ser_mtx)
                ser_mtx = roboconfino::create_mutex();
            scoped_lock guard(ser_mtx);
            m_fifo.push(std::move(ost.str()));
        }
    }

    bool serial_buffer::available() const {
        if (!ser_mtx)
            ser_mtx = roboconfino::create_mutex();
        scoped_lock guard(ser_mtx);
        return !m_fifo.empty();
    }

    std::string serial_buffer::take() {
        if (!ser_mtx)
            ser_mtx = roboconfino::create_mutex();
        scoped_lock guard(ser_mtx);
        if (m_fifo.empty()) {
            return "";
        } else {
            std::string next = std::move(m_fifo.front());
            m_fifo.pop();
            return next;
        }
    }

    serial_buffer::serial_buffer()
            : //m_lock(create_mutex()),
            m_minimal_level(DEFAULT_DEBUG), m_fifo() {
        debug("serial buffer init");
    }
}
