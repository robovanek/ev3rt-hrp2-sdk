//
// Created by kuba on 21.9.18.
//

#ifndef ROBOCONFINO_STORE_HPP
#define ROBOCONFINO_STORE_HPP

#include <memory>
#include <unordered_map>
#include "our_types.hpp"
#include "integration.hpp"

namespace roboconfino {
    class element {
    public:
        element();

        element(cf::type type, std::string &&dfl);

        int store(std::string &&value);

        std::string load() const {
            int foo = 0;
            return load(foo);
        }

        std::string load(int &version) const;

        bool update(int &currentVersion, std::string &newValue) const;

        cf::type get_type() const;

    private:
        std::string m_value;
        cf::type m_type;
        int m_master_version;
    };

    typedef std::shared_ptr<element> element_ref;

    class store {
    public:
        store() : m_lock(create_mutex()) {}

        element_ref get(const std::string &name);

        element_ref get(const std::string &name, cf::type type, std::string initializer);

        std::vector<std::string> get_names() const;

        size_t size() const {
            return m_store.size();
        }

    private:
        std::unordered_map<std::string, element_ref> m_store;
        mutable lockable m_lock;
    };
}

#endif //ROBOCONFINO_STORE_HPP
