#ifndef PROTOCOL_TAGGER_H
#define PROTOCOL_TAGGER_H

#include "protocol.hpp"
#include "our_types.hpp"

namespace roboconfino {

    template<typename T>
    struct struct_info {
        static constexpr cf_ver0_tags tag = typename T::main_tag;
    };

    template<uint8_t tag>
    struct tag_info;

#define REGISTER_TAG(T) template<> struct tag_info<T::main_tag> { typedef T type; }
    REGISTER_TAG(err);
    REGISTER_TAG(ack);
    REGISTER_TAG(reply_string);
    REGISTER_TAG(reply_bytes);
    REGISTER_TAG(configure);
    REGISTER_TAG(file_to_brick);
    REGISTER_TAG(file_to_pc);
    REGISTER_TAG(request_serial);
    REGISTER_TAG(get_option);
    REGISTER_TAG(set_option);
    REGISTER_TAG(get_option_all);
    REGISTER_TAG(rpc_request);
    REGISTER_TAG(rpc_list);
#undef REGISTER_TAG
}

#endif // PROTOCOL_TAGGER_H
