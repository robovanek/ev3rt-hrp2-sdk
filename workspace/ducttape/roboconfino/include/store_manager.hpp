//
// Created by kuba on 22.9.18.
//

#ifndef ROBOCONFINO_CONFIG_FILE_HPP
#define ROBOCONFINO_CONFIG_FILE_HPP

#include <fstream>
#include "store.hpp"

namespace roboconfino {
    class store_manager {
    public:
        explicit store_manager(store &store);

        std::string do_export();

        void do_import(const std::string &data);

        struct file_line {
            file_line();

            file_line(const std::string &type, const std::string &name, const std::string &value);

            file_line(const std::string &name, const element_ref &ref);

            file_line(const file_line &entry) = default;

            file_line(file_line &&entry) = default;

            file_line &operator=(const file_line &entry) = default;

            file_line &operator=(file_line &&entry) = default;

            std::string type;
            std::string name;
            std::string value;
            bool valid;
        };

        static void write(file_line &&what, std::vector<std::string> &where);

        static roboconfino::store_manager::file_line read(const std::string &line);

    private:
        store *m_store;
    };
}

#endif //ROBOCONFINO_CONFIG_FILE_HPP
