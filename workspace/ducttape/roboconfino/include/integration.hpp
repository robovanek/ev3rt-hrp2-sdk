//
// Created by kuba on 22.9.18.
//

#ifndef ROBOCONFINO_INTEGRATION_HPP
#define ROBOCONFINO_INTEGRATION_HPP

#include <memory>
#include "our_types.hpp"

namespace roboconfino {
    class lockable_impl {
    public:
        virtual void lock() = 0;

        virtual void unlock() = 0;
    };

    class semaphore_impl {
    public:
        virtual void give() = 0;

        virtual void take() = 0;

        virtual int probe() = 0;
    };

    typedef std::unique_ptr<lockable_impl> lockable;
    typedef std::unique_ptr<semaphore_impl> semaphore;

    class scoped_lock {
    public:
        explicit scoped_lock(lockable &sect);

        scoped_lock(const scoped_lock &other) = delete;

        scoped_lock(scoped_lock &&other) = delete;

        scoped_lock &operator=(const scoped_lock &other) = delete;

        scoped_lock &operator=(scoped_lock &&other) = delete;

        ~scoped_lock();

    private:
        lockable_impl *m_lock;
    };

    extern bool read_file(const std::string &path, std::vector<char> &data);

    extern bool write_file(const std::string &path, const char *data, size_t length);

    // to be implemented by program
    extern lockable create_critical();

    extern lockable create_mutex();

    extern semaphore create_semaphore();

    typedef void (*thread_entrypoint)(intptr_t);

    extern int start_thread(thread_entrypoint entry, int priority, intptr_t arg);

    extern void stop_thread(int id);

    // get tick counter for debug timestamps
    extern int64_t get_tick();

    extern void do_sleep(unsigned int msec);
}

#endif //ROBOCONFINO_INTEGRATION_HPP
