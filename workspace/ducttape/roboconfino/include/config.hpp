//
// Created by kuba on 6.10.18.
//

#ifndef ROBOCONFINO_CONFIG_HPP
#define ROBOCONFINO_CONFIG_HPP

#define BT_FRAME_MAX 1022
#define DEFAULT_DEBUG severity_error
#define STDIO_WRITE_LEN (static_cast<size_t>(4096))
#define STDIO_READ_LEN (static_cast<size_t>(4096))
#define BRICKLOOP_STACK_USER 4096
#define BRICKLOOP_STACK_KERNEL 4096
#define BRICKLOOP_PRIORITY 5

#endif //ROBOCONFINO_CONFIG_HPP
