//
// Created by kuba on 21.9.18.
//

#ifndef ROBOCONFINO_SERIAL_HPP
#define ROBOCONFINO_SERIAL_HPP

#include <string>
#include <memory>
#include <deque>
#include <queue>
#include <cstdarg>
#include "integration.hpp"

namespace roboconfino {

    enum severity {
        severity_debug,
        severity_info,
        severity_note,
        severity_warn,
        severity_error,
        severity_fatal
    };

    class serial_buffer {
    public:
        serial_buffer();

        void log(severity level,
                 const std::string &component,
                 const std::string &text,
                 const std::string &time);

        bool available() const;

        std::string take();

        void set_verbosity(severity minimum) {
            m_minimal_level = minimum;
        }

        severity get_verbosity() const {
            return m_minimal_level;
        }

    private:
        //mutable lockable m_lock;
        severity m_minimal_level;
        std::queue<std::string> m_fifo;
    };

    std::string formatn(const char *format, ...) __attribute__((__format__ (__printf__, 1, 2)));
    std::string vformatn(const char *format, va_list va);

    class serial_logger {
    public:
        serial_logger(std::string name, serial_buffer &backend);

        void debug(const std::string &text);

        void info(const std::string &text);

        void note(const std::string &text);

        void warn(const std::string &text);

        void error(const std::string &text);

        void fatal(const std::string &text);

        void debug(const char *format, ...) __attribute__((__format__ (__printf__, 2, 3)));

        void info(const char *format, ...) __attribute__((__format__ (__printf__, 2, 3)));

        void note(const char *format, ...) __attribute__((__format__ (__printf__, 2, 3)));

        void warn(const char *format, ...) __attribute__((__format__ (__printf__, 2, 3)));

        void error(const char *format, ...) __attribute__((__format__ (__printf__, 2, 3)));

        void fatal(const char *format, ...) __attribute__((__format__ (__printf__, 2, 3)));

        static std::string time();

    private:
        std::string m_component;
        serial_buffer *m_backend;
    };
}

#endif //ROBOCONFINO_SERIAL_HPP
