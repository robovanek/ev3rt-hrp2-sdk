//
// Created by kuba on 21.9.18.
//

#ifndef ROBOCONFINO_FIELD_HPP
#define ROBOCONFINO_FIELD_HPP

#include <memory>
#include <list>
#include <functional>
#include "store.hpp"
#include "our_types.hpp"
#include "converter.hpp"

namespace roboconfino {

    template<typename ParsedT>
    using notify_target = std::function<void(const ParsedT &)>;

    template<typename ParsedT, typename = void>
    struct user_converter;

    template<typename T>
    struct trivial_user_converter {
        typedef T base_type;
        typedef T parsed_type;

        trivial_user_converter() = default;

        static std::string save(const parsed_type &in) {
            return converter<base_type>::save(in);
        }

        static parsed_type load(const std::string &in) {
            return converter<base_type>::load(in);
        }
    };

    template<>
    struct user_converter<cf::integer>
            : public trivial_user_converter<cf::integer> {
        user_converter() = default;
    };

    template<>
    struct user_converter<cf::binary>
            : public trivial_user_converter<cf::binary> {
        user_converter() = default;
    };

    template<>
    struct user_converter<cf::string>
            : public trivial_user_converter<cf::string> {
        user_converter() = default;
    };

    template<>
    struct user_converter<cf::floating>
            : public trivial_user_converter<cf::floating> {
        user_converter() = default;
    };

    template<>
    struct user_converter<cf::boolean>
            : public trivial_user_converter<cf::boolean> {
        user_converter() = default;
    };

    template<typename DerivedT, typename ParsedT, typename BaseT>
    struct base_user_converter {
        typedef BaseT base_type;
        typedef ParsedT parsed_type;

        base_user_converter() = default;

        static std::string save(const parsed_type &in) {
            base_type &&encoded = DerivedT::encode(in);
            return base_conv::save(std::move(encoded));
        }

        static parsed_type load(const std::string &in) {
            base_type &&encoded = base_conv::load(in);
            parsed_type t = DerivedT::decode(std::move(encoded));
            return t;
        }

    private:
        typedef converter<base_type> base_conv;
    };


    template<typename ParsedT, typename = typename std::enable_if<std::is_trivially_constructible<user_converter<ParsedT>>::value>::type>
    class field {
    public:
        field(const std::string &name, store &store)
                : m_name(name),
                  m_master(store.get(name, get_type(), "0")),
                  m_cache(),
                  m_local_version(0),
                  m_notify_idx(0),
                  m_notifies() {
            force_update();
        }

        field(const std::string &name, const std::string &initializer, store &store)
                : m_name(name),
                  m_master(store.get(name, get_type(), initializer)),
                  m_cache(),
                  m_local_version(0),
                  m_notify_idx(0),
                  m_notifies() {
            force_update();
        }

        field(const char *name, std::string &&initializer, store &store)
                : m_name(name),
                  m_master(store.get(name, get_type(), std::move(initializer))),
                  m_cache(),
                  m_local_version(0),
                  m_notify_idx(0),
                  m_notifies() {
            force_update();
        }

        const std::string &get_name() const {
            return m_name;
        }

        ParsedT cached() const {
            return m_cache;
        }

        void update()  {
            std::string nVal;
            if (m_master->update(m_local_version, nVal)) {
                m_cache = user_converter<ParsedT>::load(nVal);
                notify();
            }
        }

        void force_update() {
            std::string &&value = m_master->load(m_local_version);
            m_cache = user_converter<ParsedT>::load(value);
            notify();
        }

        void push(const ParsedT &value) {
            m_cache = value;
            std::string &&str = user_converter<ParsedT>::save(m_cache);
            m_local_version = m_master->store(std::move(str));
            notify();
        }

        field &operator=(const ParsedT &val) {
            push(val);
            return *this;
        }

        constexpr cf::type get_type() {
            return enum_of_type<typename user_converter<ParsedT>::base_type>::value;
        }

        operator ParsedT() const {
            return cached();
        }

        int add_notify(notify_target<ParsedT> tgt) {
            int idx = m_notify_idx++;
            m_notifies.insert(std::make_pair(idx, std::move(tgt)));
            tgt(m_cache);
            return idx;
        }

        void remove_notify(int id) {
            m_notifies.erase(id);
        }

        void notify() {
            for (auto &&pair : m_notifies) {
                pair.second(m_cache);
            }
        }

    private:
        std::string m_name;
        element_ref m_master;
        ParsedT m_cache;

        int m_local_version;
        int m_notify_idx;
        std::unordered_map<int, notify_target<ParsedT>> m_notifies;
    };
}

#endif //ROBOCONFINO_FIELD_HPP
