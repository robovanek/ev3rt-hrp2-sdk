//
// Created by kuba on 21.9.18.
//

#ifndef ROBOCONFINO_PROTOCOL_HPP
#define ROBOCONFINO_PROTOCOL_HPP

/*
 * PROTOCOL DESIGN
 *
 * little endian integers
 *
 * tt       ... uint8_t   ... 8bit tag
 * llllllll ... uint32_t  ... 32bit data length
 * dd...    ... byte[len] ... data
 *
 */

#include <vector>
#include "our_types.hpp"

namespace roboconfino {
    enum cf_ver0_tags : uint8_t {
        // unknown reply
                VER0_REPLY_ERR,
        // acknowledge request
                VER0_REPLY_ACK,
        // common string reply
                VER0_REPLY_STRING,
        // common raw reply
                VER0_REPLY_BYTES,
        // communication configuration; reply ack
                VER0_CONFIGURE,
        // upload file to brick; reply ack
                VER0_FILE_TO_BRICK,
        // download file to computer; reply bytes
                VER0_FILE_TO_PC,
        // serial output from the brick; reply ack
                VER0_SERIAL_OUTPUT,
        // download one option to computer; reply string
                VER0_GET_OPTION,
        // upload one option to computer; reply ack
                VER0_SET_OPTION,
        // download complete configuration to computer; reply string
                VER0_GET_OPTION_ALL,
        // remote call; reply string/err
                VER0_RPC,
        // list remote calls, reply string
                VER0_LIST_RPCS,
    };
#define INVALID_TAG 255
    /////////////
    // CASTING //
    /////////////

    template<typename DstT, typename SrcT>
    bool serialize_cast(const SrcT &src, DstT &dst) {
        std::vector<cf::byte> &&raw = src.serialize();
        return dst.deserialize(raw);
    }

    /////////////////
    // BASE DGRAMS //
    /////////////////

    class datagram_base {
    public:
        typedef std::vector<cf::byte> message_data;

        datagram_base();

        const message_data &payload() const {
            return m_payload;
        }

        datagram_base &payload(const message_data &payload) {
            m_payload = payload;
            return *this;
        }


        size_t total_size() const {
            return known_size() + m_payload.size();
        }

        virtual size_t known_size() const {
            return datagram_base::header_length;
        }

        message_data serialize() const;

        bool deserialize(const message_data &data);

    protected:
        virtual message_data serialize_known() const {
            return message_data{};
        }

        virtual void deserialize_known(const message_data &data) {
        }

    private:
        std::vector<cf::byte> m_payload;
    public:
        static constexpr size_t header_length = 0;
    };

    class datagram_tlv : public datagram_base {
    public:
        typedef uint8_t message_tag;
        typedef uint32_t message_length;

        datagram_tlv();

        explicit datagram_tlv(message_tag tag);

        message_tag tag() const {
            return m_tag;
        }

        datagram_tlv &tag(message_tag tag) {
            m_tag = tag;
            return *this;
        }

        bool check_correct_tag() const;

        message_length length() const {
            return m_length;
        }

        datagram_tlv &length(message_length length) {
            m_length = length;
            return *this;
        }

        static void install_length(message_data &buffer, message_length value);
        static void install_length(cf::byte *buffer, message_length value);

        static message_length take_length(const message_data &buffer);
        static message_length take_length(const cf::byte *buffer);

    protected:
        message_data serialize_known() const override;

        void deserialize_known(const message_data &data) override;

        size_t known_size() const override {
            return datagram_tlv::header_length;
        }

    private:
        message_tag m_tag = 0;
        message_length m_length = 0;
        message_tag m_original_tag = 0;

    public:
        static constexpr size_t header_length = datagram_base::header_length
                                                + sizeof(m_tag) + sizeof(m_length);
    };

    /////////////////////
    // GENERIC FORMATS //
    /////////////////////

    class first_index : public datagram_tlv {
    public:
        typedef uint8_t index_type;

        explicit first_index(message_tag tag);

        index_type index() const {
            return m_idx;
        }

        first_index &index(index_type what) {
            m_idx = what;
            return *this;
        }

    protected:
        size_t known_size() const override {
            return header_length;
        }

        message_data serialize_known() const override;

        void deserialize_known(const message_data &data) override;

    private:
        index_type m_idx;
    public:
        static constexpr size_t header_length = datagram_tlv::header_length + sizeof(m_idx);
    };

    class first_string : public datagram_tlv {
    public:
        typedef std::string name_type;

        explicit first_string(message_tag which);

        const name_type &name() const {
            return m_name;
        }

        first_string &name(const name_type &what) {
            m_name = what;
            return *this;
        }

    protected:
        size_t known_size() const override;

        message_data serialize_known() const override;

        void deserialize_known(const message_data &raw) override;

    private:
        name_type m_name;
    };

    class second_string : public first_string {
    public:
        typedef std::string value_type;

        second_string(message_tag tag);

        const value_type &value() const {
            return m_value;
        }

        second_string &value(const value_type &what) {
            m_value = what;
            return *this;
        }

    protected:
        size_t known_size() const override;

        message_data serialize_known() const override;

        void deserialize_known(const message_data &raw) override;

    private:
        value_type m_value;
    };

    /////////////
    // REPLIES //
    /////////////

    class err : public datagram_tlv {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_REPLY_ERR;

        err() : datagram_tlv(main_tag) {}
    };

    class ack : public datagram_tlv {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_REPLY_ACK;

        ack() : datagram_tlv(main_tag) {}
    };

    class reply_string : public first_string {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_REPLY_STRING;

        reply_string() : first_string(main_tag) {}
    };

    class reply_bytes : public datagram_tlv {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_REPLY_BYTES;

        reply_bytes() : datagram_tlv(main_tag) {}
    };

    /////////////
    // REQUESTS //
    /////////////


    class configure : public first_index {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_CONFIGURE;

        configure() : first_index(main_tag) {}
    };

    class file_to_brick : public first_string {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_FILE_TO_BRICK;

        file_to_brick()
                : first_string(main_tag) {}
    };

    class file_to_pc : public first_string {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_FILE_TO_PC;

        file_to_pc()
                : first_string(main_tag) {}
    };

    class get_option_all : public datagram_tlv {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_GET_OPTION_ALL;

        get_option_all()
                : datagram_tlv(main_tag) {}
    };

    class get_option : public first_string {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_GET_OPTION;

        get_option()
                : first_string(main_tag) {}
    };

    class set_option : public second_string {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_SET_OPTION;

        set_option() : second_string(main_tag) {}
    };

    class request_serial : public datagram_tlv {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_SERIAL_OUTPUT;

        request_serial() : datagram_tlv(main_tag) {}
    };

    class rpc_request : public second_string {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_RPC;

        rpc_request() : second_string(main_tag) {}
    };

    class rpc_list : public datagram_tlv {
    public:
        static constexpr cf_ver0_tags main_tag = VER0_LIST_RPCS;

        rpc_list() : datagram_tlv(main_tag) {}
    };


    class datagram_visitor {
    public:
        enum class fail_reason {
            bad_cast,
            unknown_tag
        };

        virtual void operator()(err &pkt) = 0;

        virtual void operator()(ack &pkt) = 0;

        virtual void operator()(request_serial &out) = 0;

        virtual void operator()(configure &pkt) = 0;

        virtual void operator()(file_to_brick &pkt) = 0;

        virtual void operator()(file_to_pc &pkt) = 0;

        virtual void operator()(get_option_all &pkt) = 0;

        virtual void operator()(get_option &pkt) = 0;

        virtual void operator()(set_option &pkt) = 0;

        virtual void operator()(reply_string &pkt) = 0;

        virtual void operator()(reply_bytes &pkt) = 0;

        virtual void operator()(rpc_request &rpc) = 0;

        virtual void operator()(rpc_list &list) = 0;

        virtual void operator()(fail_reason ko) = 0;

        void apply(const datagram_base::message_data &raw);
    };
}

#endif //ROBOCONFINO_PROTOCOL_HPP
