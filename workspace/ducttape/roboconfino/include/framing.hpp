//
// Created by kuba on 6.10.18.
//

#ifndef ROBOCONFINO_FRAMING_HPP
#define ROBOCONFINO_FRAMING_HPP

#include <vector>
#include <array>
#include <algorithm>
#include <cstring>
#include <queue>
#include "our_types.hpp"
#include "integration.hpp"
#include "protocol.hpp"
#include "config.hpp"

namespace roboconfino {

    struct convert_u16 {
        convert_u16() = default;

        explicit convert_u16(uint16_t value) : integer(value) {
            loadFromInteger();
        }

        uint16_t integer = 0;
        uint8_t array[2] = {0, 0};

        void loadFromArray() {
            std::memcpy(&integer, array, 2);
        }

        void loadFromInteger() {
            std::memcpy(array, &integer, 2);
        }
    };


    class GenericChannel {
    public:
        virtual ~GenericChannel() = default;

        virtual void reset() = 0;

        virtual void sendMessage(const std::vector<cf::byte> &data) = 0;

        virtual void receivedMessage(const std::vector<cf::byte> &data) = 0;

        virtual void writeDirect(const cf::byte *data, size_t len) = 0;
    };

    class InputQueue;

    class InputBarrier;

    class MessageReader;

    class MessageWriter;

    class InputQueue {
    public:
        explicit InputQueue(InputBarrier &next);

        std::vector<cf::byte> take(size_t count);

        size_t available() const;

        void reset();

        void pushData(std::vector<uint8_t> &received);

    private:
        InputBarrier *m_next = nullptr;
        std::queue<cf::byte> m_queue;
    };


    class InputBarrier {
    public:
        explicit InputBarrier(InputQueue &queue, MessageReader &read, MessageWriter &write);

        void reset();

        void newData(size_t total);

    private:
        enum class ReadState {
            ReceivingCommand,
            ReceivingData
        };

        InputQueue *m_queue;
        MessageReader *m_next_read = nullptr;
        MessageWriter *m_next_write = nullptr;

        ReadState m_state = ReadState::ReceivingCommand;
        size_t m_length = 0;
    };

    class MessageReader {
    public:
        MessageReader(MessageWriter &write, GenericChannel &chan);

        void reset();

        void receivedPacket(const std::vector<cf::byte> &data);

    private:
        enum class ReassemblyState {
            ReadingHeader,
            ReadingData
        };

        std::vector<cf::byte> m_reassembly;
        size_t m_size = 0;
        ReassemblyState m_state = ReassemblyState::ReadingHeader;

        MessageWriter *m_next_write = nullptr;
        GenericChannel *m_next_chan = nullptr;
    };

    class MessageWriter {
        enum WriteState {
            WaitingForUser,
            WaitingForAck,
            WaitingForReceive,
        };

    public:
        explicit MessageWriter(GenericChannel &chan);

        void reset();

    public:
        void writeMessage(std::vector<cf::byte> data);

        void receivedMessage(const std::vector<cf::byte> &data);

        void receivedAck();

    private:
        void sendNext();

        std::queue<std::vector<cf::byte>> m_incoming;
        std::vector<cf::byte> m_current;
        WriteState m_state = WriteState::WaitingForUser;
        GenericChannel *m_next_channel;
    };

};

#endif //ROBOCONFINO_FRAMING_HPP
