//
// Created by kuba on 22.9.18.
//

#ifndef ROBOCONFINO_CONVERTER_HPP
#define ROBOCONFINO_CONVERTER_HPP

#include "our_types.hpp"
#include <algorithm>
#include <ios>
#include <iomanip>
#include <sstream>
#include <locale>
#include <clocale>

namespace roboconfino {
    template<typename T>
    struct converter {
    };

    template<>
    struct converter<cf::string> {
        typedef cf::string assoc_type;

        static assoc_type load(const std::string &data) {
            return data;
        }

        static std::string save(const assoc_type &data) {
            return data;
        }
    };

    template<>
    struct converter<cf::integer> {
        typedef cf::integer assoc_type;

        static assoc_type load(const std::string &data) {
            long long result;
            sscanf(data.c_str(), "%lld", &result);
            return result;
        }

        static std::string save(assoc_type data) {
            char buf[32];
            snprintf(buf, sizeof(buf), "%lld", data);
            return std::string(buf);
        }
    };

    template<>
    struct converter<cf::floating> {
        typedef cf::floating assoc_type;

        static assoc_type load(const std::string &data) {
            float output;
            sscanf(data.c_str(), "%f", &output);

            return output;
        }

        static std::string save(assoc_type data) {
            char buf[32];
            snprintf(buf, sizeof(buf), "%f", data);
            return std::string(buf);
        }
    };

    template<>
    struct converter<cf::boolean> {
        typedef cf::boolean assoc_type;

        static assoc_type load(std::string data) {
            std::transform(data.begin(), data.end(), data.begin(), ::tolower);
            return !(data == "false" || data == "0");
        }

        static std::string save(assoc_type data) {
            return data ? "true" : "false";
        }
    };

    template<>
    struct converter<cf::binary> {
        typedef cf::binary assoc_type;

        static assoc_type load(std::string data) {
            size_t len = data.length() / 2;
            assoc_type raw(len, 0);

            for (size_t i = 0; i < len; i++) {
                cf::byte upper = hexvalue(data[2 * i + 0]);
                cf::byte lower = hexvalue(data[2 * i + 1]);
                raw[i] = upper << 4 | lower;
            }

            return raw;
        }

        static std::string save(assoc_type data) {
            std::ostringstream str;
            str << std::hex << std::setfill('0');
            for (cf::byte single : data) {
                str << (int) single;
            }
            return str.str();
        }

    private:
        static cf::byte hexvalue(char ch) {
            if ('0' <= ch && ch <= '9') {
                return static_cast<cf::byte>(ch - '0');
            } else if ('a' <= ch && ch <= 'f') {
                return static_cast<cf::byte>(ch - 'a' + 10);
            } else {
                return 0;
            }
        }
    };

    template<cf::type T>
    struct type_of_enum {
    };

    template<>
    struct type_of_enum<cf::type::string> {
        typedef cf::string type;
    };

    template<>
    struct type_of_enum<cf::type::integer> {
        typedef cf::integer type;
    };

    template<>
    struct type_of_enum<cf::type::floating> {
        typedef cf::floating type;
    };

    template<>
    struct type_of_enum<cf::type::boolean> {
        typedef cf::boolean type;
    };

    template<>
    struct type_of_enum<cf::type::binary> {
        typedef cf::binary type;
    };

    template<typename T>
    struct enum_of_type {
    };

    template<>
    struct enum_of_type<cf::string> {
        static constexpr cf::type value = cf::type::string;
    };

    template<>
    struct enum_of_type<cf::integer> {
        static constexpr cf::type value = cf::type::integer;
    };

    template<>
    struct enum_of_type<cf::floating> {
        static constexpr cf::type value = cf::type::floating;
    };

    template<>
    struct enum_of_type<cf::boolean> {
        static constexpr cf::type value = cf::type::boolean;
    };

    template<>
    struct enum_of_type<cf::binary> {
        static constexpr cf::type value = cf::type::binary;
    };

    extern const char *type_to_name(cf::type t);

    extern cf::type type_from_name(std::string name);
};

#endif //ROBOCONFINO_CONVERTER_HPP
