//
// Created by kuba on 22.9.18.
//

#ifndef ROBOCONFINO_OUR_TYPES_HPP
#define ROBOCONFINO_OUR_TYPES_HPP

#include <cstdint>
#include <vector>
#include <string>

namespace roboconfino {
    namespace cf {
        // byte type
        typedef uint8_t byte;

        // config types
        typedef std::vector<byte> binary;
        typedef std::string string;
        typedef int64_t integer;
        typedef float floating;
        typedef bool boolean;

        // type enum
        enum class type {
            invalid,
            boolean,
            integer,
            floating,
            string,
            binary
        };

        // config id
        typedef int token;
    }
}

extern void debug(const char *str);
extern void debug(const char *fmt, const char *add);
extern void debug(const char *fmt, int add);

#endif //ROBOCONFINO_OUR_TYPES_HPP
