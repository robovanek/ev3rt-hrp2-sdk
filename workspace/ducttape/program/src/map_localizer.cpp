//
// Created by kuba on 26.10.18.
//

#include <program/include/map_localizer.hpp>
#include <program/include/base_names.hpp>

namespace duck {
    odometry::odometry(store &store)
            : radius(wheel_radius, "1.0", store),
              trackwidth(wheel_dist, "1.0", store),
              maxDifference(gyro_maxdiff, "15", store),
              pwr2vel(odom_wheelratio, "1.0", store),
              gyroTrust(gyro_trust, "0.5", store),
              gyroPort(diff_gyro, "4", store),
              minDt(odom_mindt, "10", store),
              m_gyroAng(gyroPort),
              m_gyroRate(gyroPort),
              m_oldL(0),
              m_oldR(0) {
        debug("in odometry ctor");
    }

    // DiffS = [ R/2  * (Wr + Wl) ]
    // DiffO = [ R/2L * (Wr - Wl) ]
    void odometry::update(const tacho_params &state) {
        int64_t nowTime = get_tick();
        if (nowTime - m_lastTime >= minDt) {
            m_lastTime = nowTime;
        } else {
            return;
        }

        maxDifference.update();
        pwr2vel.update();
        gyroTrust.update();
        radius.update();
        trackwidth.update();
        minDt.update();

        // get tacho information
        pos_ang dL(fix16_deg_to_rad(sf16(diffWheel(state.positionLeft, m_oldL))));
        pos_ang dR(fix16_deg_to_rad(sf16(diffWheel(state.positionRight, m_oldR))));
        vel_ang wL(Fix16(fix16_deg_to_rad(sf16(state.velocityLeft))) * pwr2vel.cached());
        vel_ang wR(Fix16(fix16_deg_to_rad(sf16(state.velocityRight))) * pwr2vel.cached());

        // calculate discrete tacho difference
        pos_lin dS(radius * (dR + dL) / 2_ic);
        pos_ang dO((radius * (dR - dL)) / (2_ic * trackwidth));

        // calculate gyro variables
        m_gyro_rot = m_gyroAng.measure();
        m_gyro_add = diffAng(m_gyro_rot, m_oldGyroAng);
        m_gyro_w = m_gyroRate.measure();

        // calculate odometry variables
        m_odom_add = dO;
        m_odom_w = ((wR - wL) * radius) / (2_ic * trackwidth);
        m_odom_v = ((wR + wR) * radius) / 2_ic;

        // synthesize rotation
        m_synth_w = m_gyro_w * gyroTrust + m_odom_w * (1_ic - gyroTrust);
        m_synth_rot = m_synth_rot + (m_gyro_add * gyroTrust + m_odom_add * (1_ic - gyroTrust));

        // advance position
        m_synth_loc = m_synth_loc.operator+(pos_pol(dS, heading::from_angle(m_synth_rot)));
    }

    void odometry::setPosition(const pos_vec &pos, const pos_ang &ang, int nowR, int nowL) {
        // zero velocities
        m_gyro_w = vel_ang::zero();
        m_odom_w = vel_ang::zero();
        m_synth_w = vel_ang::zero();
        m_odom_v = vel_lin::zero();

        // zero adds
        m_gyro_add = pos_ang::zero();
        m_odom_add = pos_ang::zero();

        m_synth_rot = ang;
        m_gyro_rot = m_gyroAng.measure();
        m_oldGyroAng = m_gyro_rot;
        m_synth_loc = pos;

        m_oldR = nowR;
        m_oldL = nowL;
    }

    int odometry::diffWheel(int now, int &old) {
        int diff = now - old;
        old = now;
        return diff;
    }

    pos_ang odometry::diffAng(const pos_ang &now, pos_ang &old) {
        pos_ang diff = now - old;
        old = now;
        return diff;
    }

    pos_ang odometry::odomGyroDifference() const {
        return pos_ang(fix16_abs((m_synth_rot - m_gyro_rot).value));
    }

    bool odometry::needsRefresh() const {
        return odomGyroDifference().value > maxDifference.cached().value;
    }
}
