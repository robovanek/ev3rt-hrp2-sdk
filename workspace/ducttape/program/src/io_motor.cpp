//
// Created by kuba on 26.10.18.
//

#include <program/include/io_motor.hpp>
#include <program/include/base_names.hpp>
#include <ev3api.h>

namespace duck {
    static motor_port_t cast(const roboconfino::field<motor_port> &field) {
        return (motor_port_t) field.cached();
    }

    static motor_type_t cast(motor_type t) {
        return (motor_type_t) t;
    }

    static motor_mask_t cast(motor_mask t) {
        return (motor_mask_t) t;
    }

    duck::radar_motor::radar_motor(store &store)
            : port(scan_port, "b", store),
              power(scan_pwr, "30", store),
              idle_power(scan_idPwr, "10", store),
              init_power(scan_inPwr, "60", store),
              init_delay(scan_inDly, "50", store),
              init_threshold(scan_inThr, "5", store),
              init_xtreme(scan_inEdge, "-90", store),
              m_center_tacho(0) {
        motor_port_t prt = cast(port);

        ev3_motor_config(prt, cast(type));

        activate(-init_power);
        do_sleep(init_delay);
        activate(-idle_power);
        m_center_tacho = ev3_motor_get_counts(prt) + init_xtreme;
    }

    int radar_motor::getTachoCount() const {
        return ev3_motor_get_counts(cast(port)) - m_center_tacho;
    }

    void radar_motor::stop() {
        ev3_motor_ex_stop(cast(port_bit(port)), true);
    }

    void radar_motor::moveLeft() {
        activate(+power);
    }

    void radar_motor::moveRight() {
        activate(-power);
    }

    void radar_motor::activate(int8_t pwr) {
        ev3_motor_ex_stop(cast(port_bit(port)), false);
        ev3_motor_set_power(cast(port), pwr);
        ev3_motor_ex_start(cast(port_bit(port)));
    }

    void radar_motor::moveTo(int rel) {
        ev3_motor_ex_moveparams_t params;
        params.speed = power * sgn(rel);
        params.rampup = 0;
        params.sustain = gabs(rel);
        params.rampdown = 0;
        params.brake = true;
        ev3_motor_ex_reg_step(cast(port_bit(port)), &params);
    }

    void radar_motor::update() {
        power.update();
    }

    bool radar_motor::isMoving() const {
        int velocity = 0;
        ev3_motor_get_velocity(cast(port), &velocity);
        return velocity > init_threshold;
    }

    void radar_motor::idleLeft() {
        activate(+idle_power);
    }

    void radar_motor::idleRight() {
        activate(-idle_power);
    }
}
