//
// Created by kuba on 18.11.18.
//

#include <program/include/plan_executor.hpp>
#include <ev3api.h>
#include <program/include/base_names.hpp>

namespace duck {

    motor_port_t cast(motor_port p) {
        return (motor_port_t)p;
    }

    motor_mask_t mask(motor_port a) {
        return (motor_mask_t)port_bit(a);
    }

    motor_mask_t mask(motor_port a, motor_port b) {
        return (motor_mask_t)(port_bit(a) | port_bit(b));
    }



    executor::executor(odometry &odom, roboconfino::store &conf)
            : colorPort(diff_color, "2", conf),
              touchPort(diff_touch, "c", conf),
              leftPort(diff_left, "a", conf),
              rightPort(diff_right, "d", conf),
              ghostChromaThreshold(ghost_chroma, "0.5", conf),
              fastPower(power_fast, "50", conf),
              slowPower(power_slow, "10", conf),
              turnPower(power_turn, "50", conf),
              steerPower(power_steer, "50", conf),
              turnRatio(turn_ratio, "50", conf),
              turn1Len(turn_1_len, "360", conf),
              turn2Len(turn_2_len, "720", conf),
              unalignLen(unalign_len, "200", conf),
              steerAngle(steer_angle, "90", conf),
              m_color(colorPort),
              m_color_calib(m_color),
              m_touch(touchPort),
              m_color_curves(conf),
              m_color_hsl(),
              m_odom(&odom) {
        debug("in executor ctor");
        ev3_motor_config(cast(leftPort), LARGE_MOTOR);
        ev3_motor_config(cast(rightPort), LARGE_MOTOR);
        ev3_motor_ex_reset_counts(mask(leftPort, rightPort));
    }

    void executor::update() {
        colorPort.update();
        touchPort.update();
        leftPort.update();
        rightPort.update();
        ghostChromaThreshold.update();
        fastPower.update();
        slowPower.update();
        turnPower.update();
        steerPower.update();
        turnRatio.update();
        turn1Len.update();
        turn2Len.update();
        unalignLen.update();
        steerAngle.update();
        m_color_curves.params.update();

        if (m_current == state::none) {
            m_done = ev3_motor_ex_running(mask(leftPort, rightPort)) == 0;
        } else if (m_current == state::steer_reg) {
            if (m_steerDir == steer::to_right) {
                heading startOK = m_startHead >> steerAngle.cached();
                heading endOK = startOK >> pos_ang(Fix16(fix16_pi_half));

                m_done = heading::from_angle(m_odom->theta()).in_range_direct(startOK, endOK);
                if (m_done) {
                    goSlow();
                    m_current = state::none;
                }
            } else {
                heading startOK = m_startHead << steerAngle.cached();
                heading endOK = startOK << pos_ang(Fix16(fix16_pi_half));

                m_done = heading::from_angle(m_odom->theta()).in_range_direct(endOK, startOK);
                if (m_done) {
                    goSlow();
                    m_current = state::none;
                }
            }
        }
    }

    void executor::steerLeft() {
        ev3_motor_ex_stop(mask(leftPort, rightPort), false);

        ev3_motor_ex_syncparams_t param;
        param.speed = turnPower;
        param.turn_ratio = +turnRatio;
        param.length = 0;
        param.brake = false;
        ev3_motor_ex_sync_step(cast(leftPort), cast(rightPort), &param);

        m_startHead = heading::from_direction(heading::from_angle(m_odom->theta()).get_direction());
        m_current = state::steer_reg;
        m_steerDir = steer::to_left;
    }

    void executor::steerRight() {
        ev3_motor_ex_syncparams_t param;
        param.speed = turnPower;
        param.turn_ratio = -turnRatio;
        param.length = 0;
        param.brake = false;
        ev3_motor_ex_sync_step(cast(leftPort), cast(rightPort), &param);

        m_startHead = heading::from_direction(heading::from_angle(m_odom->theta()).get_direction());
        m_current = state::steer_reg;
        m_steerDir = steer::to_right;
    }

    void executor::turnLeft() {
        ev3_motor_ex_syncparams_t param;
        param.speed = turnPower;
        param.turn_ratio = +200;
        param.length = 0;
        param.brake = true;
        ev3_motor_ex_sync_step(cast(leftPort), cast(rightPort), &param);

        m_startHead = heading::from_direction(heading::from_angle(m_odom->theta()).get_direction());
        m_current = state::steer_reg;
        m_steerDir = steer::to_left;
    }

    void executor::turnRight() {
        ev3_motor_ex_syncparams_t param;
        param.speed = turnPower;
        param.turn_ratio = -200;
        param.length = 0;
        param.brake = true;
        ev3_motor_ex_sync_step(cast(leftPort), cast(rightPort), &param);

        m_startHead = heading::from_direction(heading::from_angle(m_odom->theta()).get_direction());
        m_current = state::steer_reg;
        m_steerDir = steer::to_right;
    }

    void executor::goFast() {
        ev3_motor_ex_stop(mask(leftPort, rightPort), false);

        ev3_motor_ex_syncparams_t param;
        param.speed = fastPower;
        param.turn_ratio = 0;
        param.length = 0;
        param.brake = false;
        ev3_motor_ex_sync_step(cast(leftPort), cast(rightPort), &param);
        m_current = state::none;
    }

    void executor::goFastReg() {
        goFast(); // noop
    }

    void executor::goSlow() {
        ev3_motor_ex_stop(mask(leftPort, rightPort), false);

        ev3_motor_ex_syncparams_t param;
        param.speed = slowPower;
        param.turn_ratio = 0;
        param.length = 0;
        param.brake = false;
        ev3_motor_ex_sync_step(cast(leftPort), cast(rightPort), &param);
        m_current = state::none;
    }

    void executor::stop() {
        ev3_motor_ex_stop(mask(leftPort, rightPort), true);
        m_current = state::none;
    }

    void executor::floatMotors() {
        ev3_motor_ex_stop(mask(leftPort, rightPort), false);
        m_current = state::none;
    }

    void executor::frontAlign() {
        // noop
    }

    void executor::frontUnalign() {
        ev3_motor_ex_stop(mask(leftPort, rightPort), false);

        ev3_motor_ex_syncparams_t param;
        param.speed = -slowPower;
        param.turn_ratio = 0;
        param.length = unalignLen;
        param.brake = true;
        ev3_motor_ex_sync_step(cast(leftPort), cast(rightPort), &param);
        m_current = state::none;
    }

    void executor::turnAround() {
        ev3_motor_ex_syncparams_t param;
        param.speed = turnPower;
        param.turn_ratio = +200;
        param.length = 0;
        param.brake = true;
        ev3_motor_ex_sync_step(cast(leftPort), cast(rightPort), &param);

        m_startHead = heading::from_direction(direction_ccw(heading::from_angle(m_odom->theta()).get_direction()));
        m_current = state::steer_reg;
        m_steerDir = steer::to_left;
    }

    bool executor::isDone() const {
        return m_done;
    }

    bool executor::touchPressed() {
        return m_touch.measure();
    }

    bool executor::colorPressed() {
        hc c = currentColor();
        return c.chroma > ghostChromaThreshold;
    }

    tacho_params executor::getTacho() {
        tacho_params a{};
        ev3_motor_get_velocity(cast(leftPort), &a.velocityLeft);
        ev3_motor_get_velocity(cast(rightPort), &a.velocityRight);
        a.positionLeft = ev3_motor_get_counts(cast(leftPort));
        a.positionRight = ev3_motor_get_counts(cast(rightPort));
        return a;
    }

    rgb executor::calibrateWhite() {
        return m_color_calib.measureWhite();
    }

    rgb executor::calibrateBlack() {
        return m_color_calib.measureBlack();
    }

    void executor::calibrateWrite() {
        m_color_calib.exportConfig(m_color_curves.params);
    }

    hc executor::currentColor() {
        hc trans = m_color_hsl(currentColorRgb());
        return trans;
    }

    rgbf executor::currentColorRgb() {
        rgb raw = m_color.measure();
        rgbf norm = m_color_curves(raw);
        return norm;
    }

    void executor::reset() {
        m_current = state::none;
    }
}