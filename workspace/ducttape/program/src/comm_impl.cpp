//
// Created by kuba on 6.10.18.
//

#include "serial.hpp"
#include "comm_impl.hpp"
#include "store_manager.hpp"
#include <sstream>
#include <kernel.h>
#include <t_stddef.h>
#include <syssvc/serial.h>

namespace duck {

    template<typename T>
    void write_datagram(Ev3rtChannel *out, const T &datagram) {
        datagram_base::message_data &&raw = datagram.serialize();
        out->sendMessage(raw);
    }

    Ev3rtComm::Ev3rtComm(Ev3rtChannel &chan, store &config, serial_buffer &dbg)
            : m_config(&config), m_serial(&dbg), m_chan(&chan), m_rpc_lock(create_mutex()) {
        debug("comm init");
    }

    void Ev3rtComm::operator()(err &pkt) {}

    void Ev3rtComm::operator()(ack &pkt) {}

    void Ev3rtComm::operator()(configure &pkt) {
        uint8_t level = pkt.index();
        if (level >= severity_debug && level <= severity_fatal) {
            m_serial->set_verbosity(static_cast<severity>(level));
            write_datagram(m_chan, ack{});
        } else {
            write_datagram(m_chan, err{});
        }
    }

    void Ev3rtComm::operator()(file_to_brick &pkt) {
        const char *data = reinterpret_cast<const char *>(pkt.payload().data());
        if (write_file(pkt.name(), data, pkt.payload().size())) {
            write_datagram(m_chan, ack{});
        } else {
            write_datagram(m_chan, err{});
        }
    }

    void Ev3rtComm::operator()(file_to_pc &pkt) {
        std::vector<char> chars;
        if (read_file(pkt.name(), chars)) {
            std::vector<cf::byte> bytes(chars.begin(), chars.end());
            write_datagram(m_chan, reply_bytes{}.payload(bytes));
        } else {
            write_datagram(m_chan, err{});
        }
    }

    void Ev3rtComm::operator()(get_option_all &pkt) {
        store_manager manage(*m_config);
        write_datagram(m_chan, reply_string{}.name(manage.do_export()));
    }

    void Ev3rtComm::operator()(get_option &pkt) {
        std::string &&result = m_config->get(pkt.name())->load();
        write_datagram(m_chan, reply_string{}.name(result));
    }

    void Ev3rtComm::operator()(set_option &pkt) {
        std::string value = pkt.value();
        m_config->get(pkt.name())->store(std::move(value));
        write_datagram(m_chan, ack{});
    }

    void Ev3rtComm::operator()(reply_string &pkt) {}

    void Ev3rtComm::operator()(reply_bytes &pkt) {}

    void Ev3rtComm::operator()(datagram_visitor::fail_reason ko) {}

    void Ev3rtComm::operator()(request_serial &out) {
        std::ostringstream ost;
        while (m_serial->available()) {
            ost << m_serial->take();
        }
        write_datagram(m_chan, reply_string{}.name(ost.str()));
    }

    void Ev3rtComm::register_rpc(const std::string &name, rpc_target rpc) {
        scoped_lock lock(m_rpc_lock);
        m_rpcs[name] = std::move(rpc);
    }

    void Ev3rtComm::operator()(rpc_request &rpc) {
        scoped_lock lock(m_rpc_lock);

        auto it = m_rpcs.find(rpc.name());
        if (it == m_rpcs.end()) {
            write_datagram(m_chan, err{});
        } else {
            std::string &&result = it->second(rpc.value());
            write_datagram(m_chan, reply_string{}.name(result));
        }
    }

    void Ev3rtComm::operator()(rpc_list &list) {
        std::ostringstream ostr;
        {
            scoped_lock lock(m_rpc_lock);
            for (auto &&pair : m_rpcs) {
                ostr << pair.first << std::endl;
            }
        }
        write_datagram(m_chan, reply_string{}.name(ostr.str()));
    }

    Ev3rtChannel::Ev3rtChannel(Ev3rtComm &comm)
            : m_queue(m_rdevent),
              m_rdevent(m_queue, m_read, m_write),
              m_read(m_write, *this),
              m_write(*this),
              m_comm(&comm) {
        debug("channel init");
    }

    void Ev3rtChannel::reset() {
        m_queue.reset();
        m_rdevent.reset();
        m_read.reset();
        m_write.reset();
    }

    void Ev3rtChannel::sendMessage(const std::vector<cf::byte> &data) {
        m_write.writeMessage(data);
    }

    void Ev3rtChannel::receivedMessage(const std::vector<cf::byte> &data) {
        m_comm->apply(data);
    }

    void Ev3rtChannel::writeDirect(const cf::byte *data, size_t len) {
        serial_wri_dat(SIO_PORT_BT,
                       reinterpret_cast<const char *>(data),
                       static_cast<uint_t>(len));
    }

    void Ev3rtChannel::update() {
        //debug("pre-report\n");
        T_SERIAL_RPOR report;
        ER er = serial_ref_por(SIO_PORT_BT, &report);
        if (er != E_OK) {
            return;
        }
        //debug("pre-if\n");
        if (report.reacnt > 0) {
            //debug("pre-read\n");
            std::vector<cf::byte> data(report.reacnt);
            ER_UINT er = serial_rea_dat(SIO_PORT_BT,
                                        reinterpret_cast<char *>(data.data()),
                                        static_cast<uint_t>(data.size()));
            //debug("post-read\n");
            if (er >= 0) {
                data.erase(data.begin() + er, data.end());
                m_queue.pushData(data);
            }
        }
        //debug("update-end\n");
    }
}