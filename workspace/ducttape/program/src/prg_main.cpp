//
// Created by kuba on 8.10.18.
//

#include <program/include/prg_main.hpp>

#include "store_manager.hpp"
#include "prg_main.hpp"
#include <functional>
#include <program/include/base_units.hpp>
#include "base_names.hpp"
#include "field.hpp"

#include "kernel.h"
#include "target_svc.h"
#include "target_serial.h"
#include "syssvc/serial.h"
#include "svc_call.h"
#include <t_syslog.h>
#include <ev3api.h>


namespace duck {
    using std::placeholders::_1;

    program::program()
            : m_config(),
              m_exporter(m_config),
              m_config_path(duck_config_path),
              m_autoimport(m_exporter, m_config_path),
              m_channel(m_comm),
              m_comm(m_channel, m_config, m_debug),
              m_debug(),
              m_logic(m_config, *this),
              m_log(create_logger("mainloop")) {
        m_comm.register_rpc("config.save", std::bind(&program::export_hook, this, _1));
        time::program_begin = get_tick();
        srand(time::program_begin);
    }

    void program::enter() {
        debug("ENTERED!! YAAAY!!");
        m_logic.reset();
        export_config();

        m_log.info("Ready to start.");
        ev3_led_set_color(LED_ORANGE);
        ev3_lcd_draw_string("ENTER->START", 10, 20);
        ev3_lcd_draw_string("ESCAPE->EXIT", 10, 40);
        ev3_speaker_play_tone(800, 100);
        while (!ev3_button_is_pressed(ENTER_BUTTON) && !ev3_button_is_pressed(BACK_BUTTON)) {
            //m_channel.update();
            do_sleep(2);
        }

        if (ev3_button_is_pressed(BACK_BUTTON)) {
            ev3_lcd_fill_rect(0, 0, EV3_LCD_WIDTH, EV3_LCD_HEIGHT, EV3_LCD_WHITE);
            ev3_led_set_color(LED_GREEN);
            return;
        }


        m_log.info("Starting.");
        m_logic.start();
        ev3_led_set_color(LED_GREEN);
        ev3_lcd_fill_rect(0, 0, EV3_LCD_WIDTH, EV3_LCD_HEIGHT, EV3_LCD_WHITE);
        ev3_lcd_draw_string("ESCAPE->EXIT", 10, 20);
        ev3_lcd_draw_string("RIGHT->RESET", 10, 40);
        while (!ev3_button_is_pressed(BACK_BUTTON)) {
            //m_channel.update();
            m_logic.update();
            do_sleep(2);
        }
        m_logic.stop();
    }

    void program::register_rpc(const std::string &name, rpc_target call) {
        m_comm.register_rpc(name, std::move(call));
    }

    serial_logger program::create_logger(const std::string &component) {
        return serial_logger(component, m_debug);
    }

    store &program::config() {
        return m_config;
    }

    std::string program::export_hook(const std::string &arg) {
        export_config();
        return "Done.";
    }

    void program::export_config() {
        std::string conf = m_exporter.do_export();
        write_file(m_config_path, conf.c_str(), conf.size());
    }

    void program::import_config() {
        std::vector<char> data;
        if (read_file(m_config_path, data)) {
            std::string str(data.begin(), data.end());
            m_exporter.do_import(str);
        }
    }

    do_import::do_import(store_manager &mgr, const std::string &path) {
        std::vector<char> data;
        if (read_file(path, data)) {
            std::string str(data.begin(), data.end());
            mgr.do_import(str);
        }
    }
}

void debug(const char *a, int b) {
    if (!a) return;
    syslog(LOG_EMERG, a, b);
}

void debug(const char *a, const char *b) {
    if (!a || !b) return;
    syslog(LOG_EMERG, a, b);
    //int len = strlen(str);
    //serial_wri_dat(SIO_PORT_UART, str, len);
}

void debug(const char *a) {
    if (!a) return;
    return debug("%s", a);
}