//
// Created by kuba on 26.10.18.
//

#include <program/include/map_scanner.hpp>
#include <program/include/base_names.hpp>
#include <program/include/prg_main.hpp>
#include <utility>

namespace duck {
    scanner::scanner(store &config, program &prg)
            : sonicPort(diff_sonic, "3", config),
              moveDelay(scan_dly, "100", config),
              moveAng(scan_ang, "100", config),
              centerDelay(scan_cdly, "100", config),
              m_sonic(sonicPort),
              m_actuator(config),
              m_scanned(false),
              m_position(sonic_to::s_right),
              m_log(prg.create_logger("scanner")) {
        debug("in scanner ctor");
    }

    void scanner::update() {
        m_actuator.update();
        moveAng.update();
        moveDelay.update();


        if (m_queue.empty()) {
            return;
        }

        auto &&tsk = m_queue.front();
        if (tsk.op == sonic_op::goto_passive) {
            if (!m_started) {
                m_log.debug("PASSIVE IS HERE");
                tsk.tacho_begin = m_actuator.getTachoCount();
                tsk.ts_begin = get_tick();
                statefulStart(tsk);
                m_started = true;
            } else {
                if (statefulDone(tsk)) {
                    statefulStop(tsk);
                    m_queue.pop();
                    m_started = false;
                }
            }
        } else if (tsk.op == sonic_op::goto_active) {
            if (!m_started) {
                m_log.debug("SCAN IS HERE");
                m_log.debug("Starting US scan");
                m_scanned.setAll(false);

                auto &first = m_msr[tsk.begin];
                first.distance = m_sonic.measure();
                first.latestID = tsk.id;
                m_scanned.scan[tsk.begin] = true;
                report(tsk.begin);

                tsk.ts_begin = get_tick();
                tsk.tacho_begin = m_actuator.getTachoCount();
                statefulStart(tsk);
                m_started = true;
            } else {
                if ((tsk.begin == sonic_to::s_left && tsk.end == sonic_to::s_right) ||
                    (tsk.begin == sonic_to::s_right && tsk.end == sonic_to::s_left)) {

                    if (!m_scanned.scan[sonic_to::s_center] &&
                        gabs(m_actuator.getTachoCount() - tsk.tacho_begin) > moveAng) {

                        auto &&msr = m_msr[sonic_to::s_center];
                        msr.distance = m_sonic.measure();
                        msr.latestID = tsk.id;
                        m_scanned.scan[sonic_to::s_center] = true;
                        report(sonic_to::s_center);
                    }
                }

                if (statefulDone(tsk)) {
                    statefulStop(tsk);

                    auto &last = m_msr[tsk.end];
                    last.distance = m_sonic.measure();
                    last.latestID = tsk.id;
                    report(tsk.end);

                    m_queue.pop();
                    m_started = false;
                }
            }
        } else if (tsk.op == sonic_op::delay) {
            if (!m_started) {
                m_log.debug("DELAY IS HERE");
                tsk.ts_begin = get_tick();
                m_started = true;
            } else {
                if ((get_tick() - tsk.ts_begin) > centerDelay) {
                    m_queue.pop();
                    m_started = false;
                }
            }
        }
    }

    sonic_msr &scanner::of(sonic_to where) {
        return m_msr[where];
    }

    const sonic_msr &scanner::of(sonic_to where) const {
        return m_msr[where];
    }

    void scanner::put(sonic_to where) {
        m_queue.push(sonic_task(sonic_op::goto_passive, where));
        m_position = where;
    }

    void scanner::putScan(sonic_to from, sonic_to where, int id) {
        m_queue.push(sonic_task(sonic_op::goto_active, from, where, id));
        m_position = where;
    }

    void scanner::scan(sonic_to end, const sonic_req &where, int id) {
        sonic_to mLeft = leftmost(where);
        sonic_to mRight = rightmost(where);

        if (gabs(mLeft - m_position) <= gabs(mRight - m_position)) {

            if (mLeft != m_position)
                put(mLeft);

            if (mLeft == sonic_to::s_left && mRight == sonic_to::s_right) {
                putScan(sonic_to::s_left, sonic_to::s_center, id);
                m_queue.push(sonic_task(sonic_op::delay));
                putScan(sonic_to::s_center, sonic_to::s_right, id);
            } else {
                putScan(mLeft, mRight, id);
            }

            if (mRight != end)
                put(end);

        } else {

            if (mRight != m_position)
                put(mRight);

            if (mLeft == sonic_to::s_left && mRight == sonic_to::s_right) {
                putScan(sonic_to::s_right, sonic_to::s_center, id);
                m_queue.push(sonic_task(sonic_op::delay));
                putScan(sonic_to::s_center, sonic_to::s_left, id);
            } else {
                putScan(mRight, mLeft, id);
            }


            if (mLeft != end)
                put(end);

        }
    }

    sonic_to scanner::leftmost(const sonic_req &where) {
        for (int i = sonic_to::s_left; i <= sonic_to::s_right; i++) {
            if (where.scan[i]) {
                return (sonic_to) i;
            }
        }
        return sonic_to::s_left;
    }

    sonic_to scanner::rightmost(const sonic_req &where) {
        for (int i = sonic_to::s_right; i >= sonic_to::s_left; i--) {
            if (where.scan[i]) {
                return (sonic_to) i;
            }
        }
        return sonic_to::s_right;
    }

    void scanner::report(sonic_to pos) {
        const char *pStr = "N/A";
        switch (pos) {
            case s_left:
                pStr = "left";
                break;
            case s_center:
                pStr = "center";
                break;
            case s_right:
                pStr = "right";
                break;
        }
        m_log.debug("Pinging: %s", pStr);
        m_log.debug(" - tacho: %d", m_actuator.getTachoCount());
        m_log.debug(" - distance: %d mm", (int) (int16_t) m_msr[(int) pos].distance.value);
    }

    bool scanner::statefulDone(sonic_task &tsk) {
        if ((get_tick() - tsk.ts_begin) < moveDelay) {
            return false;
        }
        return !m_actuator.isMoving();
    }

    void scanner::statefulStart(sonic_task &tsk) {
        switch (tsk.end) {
            case s_left:
                m_actuator.moveLeft();
                break;
            case s_center:
                m_actuator.moveTo(-m_actuator.getTachoCount());
                break;
            case s_right:
                m_actuator.moveRight();
                break;
        }
    }

    void scanner::statefulStop(sonic_task &task) {
        switch (task.end) {
            case s_left:
                m_actuator.idleLeft();
                break;
            case s_center:
                m_actuator.stop();
                break;
            case s_right:
                m_actuator.idleRight();
                break;
        }
    }

    sonic_task::sonic_task(sonic_op op) : op(op), begin(sonic_to::s_center), end(sonic_to::s_center), id(0) {}

    sonic_task::sonic_task(sonic_op op, sonic_to end)
            : op(op), begin(end), end(end), id(0) {}

    sonic_task::sonic_task(sonic_op op, sonic_to begin, sonic_to end, int id)
            : op(op), begin(begin), end(end), id(id) {}

    sonic_req::sonic_req(bool l, bool c, bool r) : scan() {
        scan[sonic_to::s_left] = l;
        scan[sonic_to::s_center] = c;
        scan[sonic_to::s_right] = r;
    }

    void sonic_req::setAll(bool doScan) {
        if (doScan) {
            scan.set();
        } else {
            scan.reset();
        }
    }

    sonic_req::sonic_req(bool all) : scan() {
        setAll(all);
    }
}
