//
// Created by kuba on 18.11.18.
//

#include <program/include/map_path.hpp>

namespace duck {

    path_finder &duck::path_finder::addReturn(tile t) {
        m_allowReturn.set((size_t) t, true);
        return *this;
    }

    path_finder &path_finder::addThrough(tile t) {
        m_allowThrough.set((size_t) t, true);
        return *this;
    }

    path_finder::path_finder(map &map)
            : m_map(&map), m_allowThrough(), m_allowReturn() {}

    path path_finder::bfs(const coords &curPos, direction curDir) const {
        node_queue open_queue;
        node_set closed_set;
        node_track backtrack;

        open_queue.push_back(curPos);
        backtrack.insert(std::make_pair(curPos, curPos));

        while (!open_queue.empty()) {
            coords pos = open_queue.front();
            open_queue.pop_front();

            if (okReturn(m_map->at(pos))) {
                return backtrace(pos, backtrack);
            }

            direction fromParent;
            auto &parent = backtrack[pos];
            if (parent == pos) {
                fromParent = curDir;
            } else {
                fromParent = parent.dirOfNext(pos);
            }

            std::array<direction, 3> nextDirs{fromParent, direction_cw(fromParent), direction_ccw(fromParent)};
            for (direction nextDir : nextDirs) {
                coords nextCoord = pos.advance(nextDir);

                if (closed_set.find(nextCoord) != closed_set.end()) {
                    continue;
                }

                if (!okThrough(m_map->at(nextCoord))) {
                    continue;
                }

                if (std::find(open_queue.begin(), open_queue.end(), nextCoord) == open_queue.end()) {
                    backtrack.insert(std::make_pair(nextCoord, pos));
                    open_queue.push_back(nextCoord);
                }
            }

            closed_set.insert(pos);
        }
        return path{};
    }

    path path_finder::backtrace(const coords &goal, const path_finder::node_track &bt) const {
        path result;

        coords current = goal;
        for (coords parent = bt.at(current); current != parent; current = parent, parent = bt.at(current)) {
            result.push_front(current);
        }
        result.push_front(current);

        return result;
    }
}