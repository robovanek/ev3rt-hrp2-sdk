//
// Created by kuba on 19.10.18.
//

#include <program/include/map_tiles.hpp>
#include <algorithm>
#include <program/include/prg_main.hpp>

namespace duck {
    tile map::dummy;

    map::map(program &prg)
            : m_log(prg.create_logger("map")),
              m_storage(((debug("vector arg")), width * height)) {
        debug("map ctor");
        reset_full();
    }

    tile &map::at(const coords &coord) {
        const size_t &x = coord.x;
        const size_t &y = coord.y;

        if (!outside_bounds(x, y)) {
            return m_storage[index(x, y)];
        }
        return dummy;
    }

    const tile &map::at(const coords &coord) const {
        const size_t &x = coord.x;
        const size_t &y = coord.y;

        if (!outside_bounds(x, y)) {
            return m_storage[index(x, y)];
        }
        return dummy;
    }


    void map::reset_sides() {
        for (size_t x = 0; x < width; x++) {
            m_storage[index(x, 0)] = tile::wall;
            m_storage[index(x, height - 1)] = tile::wall;
        }
        for (size_t y = 1; y < height - 1; y++) {
            m_storage[index(0, y)] = tile::wall;
            m_storage[index(width - 1, y)] = tile::wall;
        }
        m_storage[index(5, 4)] = tile::space_off;
        m_storage[index(4, 3)] = tile::wall;
        m_storage[index(5, 3)] = tile::wall;
        m_storage[index(6, 3)] = tile::wall;
    }

    void map::reset_full() {
        for (size_t y = 1; y < height - 1; y++) {
            for (size_t x = 1; x < width - 1; x++) {
                m_storage[index(x, y)] = tile::unknown;
            }
        }
        reset_sides();
    }

    void map::record_measurement(const pos_vec &usPos, direction rbtAng, sonic_to usDir, const pos_lin &dist) {
        m_log.info("Recording measurement: %d mm", (int) (int16_t) dist.value);
        coords usCoord = coords::from_vector(usPos);

        if (dist.value == pos_lin(Fix16(2550_ic)).value) {
            return;
        }

        const char *dirstr = "N/A";
        direction usAng;
        switch (usDir) {
            case s_left:
                dirstr = "L";
                usAng = direction_ccw(rbtAng);
                break;
            case s_center:
                dirstr = "C";
                usAng = rbtAng;
                break;
            case s_right:
                dirstr = "R";
                usAng = direction_cw(rbtAng);
                break;
            default:
                usAng = direction::north;
                break;
        }
        m_log.debug(" - US coords: [%s] [%d,%d]", dirstr, usCoord.x, usCoord.y);

        auto wallVector = usPos + pos_vec(dist.value + sf16(center), usAng);
        auto wallCoord = coords::from_vector(wallVector);
        auto wallDist = std::max(gabs((int)wallCoord.x - (int)usCoord.x),
                gabs((int)wallCoord.y - (int)usCoord.y));

        for (coords step = usCoord; step != wallCoord; step = step.advance(usAng, 1)) {
            auto &tile = at(step);
            if (tile != tile::space_off) {
                tile = tile::space_on;
                m_log.debug(" - tile[%d,%d] = on", step.x, step.y);
            } else {
                m_log.debug(" - tile[%d,%d] = <set>", step.x, step.y);
            }
        }

        if (wallDist <= 1) {
            m_log.debug(" - tile[%d,%d] = wall", wallCoord.x, wallCoord.y);
            at(wallCoord) = tile::wall;
        }
    }

    bool operator==(const coords &a, const coords &b) {
        return a.x == b.x && a.y == b.y;
    }

    bool operator!=(const coords &a, const coords &b) {
        return !(a == b);
    }

    direction coords::dirOfNext(const coords &next) {
        if (next.x == x && next.y == y) {
            // foo
            return direction::north;
        }

        if (next.x == x) {
            return next.y > y ? direction::north : direction::south;

        } else if (next.y == y) {
            return next.x > x ? direction::east : direction::west;

        } else {
            // foo
            return direction::north;
        }
    }

    pos_vec coords::to_vector() const {
        pos_lin vX(Fix16(sf16((int)x)) * Fix16(sf16(module)) + Fix16(sf16(center)));
        pos_lin vY(Fix16(sf16((int)y)) * Fix16(sf16(module)) + Fix16(sf16(center)));
        return pos_vec(vX, vY);
    }
}