//
// Created by kuba on 18.11.18.
//

#include "base_types.hpp"
#include "plan_scheduler.hpp"
#include "plan_fsm.hpp"
#include "prg_main.hpp"

namespace duck {

    scheduler::scheduler(fsm &fsm, map &map, program &prg)
            : m_fsm(&fsm), m_map(&map),
              m_find_light(*m_map),
              m_find_unknown(*m_map),
              m_log(prg.create_logger("sched")){
        debug("in scheduler ctor");
        m_find_light.addReturn(tile::space_on);
        m_find_light.addThrough(tile::space_off);
        m_find_unknown.addReturn(tile::unknown);
        m_find_unknown.addThrough(tile::space_on).addThrough(tile::space_off);
    }

    void scheduler::returnTurnaround() {
        m_fsm->reportPlanTurnaround();
    }

    void scheduler::returnWay(steer free) {
        m_fsm->reportPlanGo(free);
    }

    void scheduler::planCross() {
        surroundings around(m_fsm->usPos(), m_fsm->theta(), *m_map);
        m_map->at(around.c_current) = tile::space_off;

        m_log.debug("Planning:");
        m_log.debug(" - current: [%d,%d]", around.c_current.x, around.c_current.y);
        m_log.debug(" - straight: [%d,%d]", around.c_straight.x, around.c_straight.y);
        m_log.debug(" - left: [%d,%d]", around.c_left.x, around.c_left.y);
        m_log.debug(" - right: [%d,%d]", around.c_right.x, around.c_right.y);

        switch (around.free) {
            case 0:
                returnTurnaround();
                m_log.info("No way free, turning around.");
                break;
            case 1:
                if (isFree(around.t_left)) {
                    m_log.info("Left way free");
                    returnWay(steer::to_left);
                } else if (isFree(around.t_right)) {
                    m_log.info("Right way free:");
                    returnWay(steer::to_right);
                } else {
                    m_log.info("Straight way free:");
                    returnWay(steer::to_straight);
                }
                break;
            case 2:
            case 3:
                m_log.info("Multiple ways free.");
                planMultiCross(around);
                break;
            default:
                // foo
                break;
        }
    }

    void scheduler::planMultiCross(surroundings &around) {
        int ons = around.countTiles(tile::space_on);
        if (ons == 1) {
            if (around.t_left == tile::space_on) {
                m_log.info("Only left ON.");
                return returnWay(steer::to_left);
            } else if (around.t_right == tile::space_on) {
                m_log.info("Only right ON.");
                return returnWay(steer::to_right);
            } else {
                m_log.info("Only straight ON.");
                return returnWay(steer::to_straight);
            }
        } else if (ons == 0) {
            m_log.info("Nothing ON.");
            if (planTryPath(around, m_find_light)) {
                m_log.info("Planned using nearest ON.");
                return;
            }
            if (planTryPath(around, m_find_unknown)) {
                m_log.info("Planned using nearest unknown.");
                return;
            }

            m_log.info("Random guess.");
            int random = rand() % around.free;
            if (around.free == 2) {
                if (around.t_right == tile::wall) {
                    return returnWay(random == 0 ? steer::to_left : steer::to_straight);
                } else if (around.t_left == tile::wall) {
                    return returnWay(random == 0 ? steer::to_straight : steer::to_right);
                } else {
                    return returnWay(random == 0 ? steer::to_left : steer::to_right);
                }
            } else {
                return returnWay(random == 0 ? steer::to_left :
                                 random == 1 ? steer::to_straight : steer::to_right);
            }

        } else {
            m_log.info("Multiple ONs.");
            if (around.t_straight == tile::space_on) {
                m_log.info("Driving straight.");
                return returnWay(steer::to_straight);
            }

            m_log.info("Random guess.");
            // TODO more advanced intelligence
            int random = rand() % 2;
            return returnWay(random == 0 ? steer::to_left : steer::to_right);
        }
    }

    void scheduler::planByPath(direction curDir, path &&pth) {
        coords thisNode = pth.front();
        pth.pop_front();
        coords nextNode = pth.front();
        pth.pop_front();

        direction newDir = thisNode.dirOfNext(nextNode);
        return returnWay(steerOf(curDir, newDir));
    }

    bool scheduler::planTryPath(const surroundings &around, const path_finder &type) {
        // find unknown blocks
        auto path = type.bfs(around.c_current, around.d_straight);
        if (path.size() >= 2) {
            planByPath(around.d_straight, std::move(path));
            return true;
        }
        return false;
    }


    surroundings::surroundings(const pos_vec &usPos, const pos_ang &rbtAng, const map &map)
            : c_current(coords::from_vector(usPos)),
              d_straight(heading::from_angle(rbtAng).get_direction()),
              d_left(direction_ccw(d_straight)),
              d_right(direction_cw(d_straight)),
              c_straight(c_current.advance(d_straight)),
              c_left(c_current.advance(d_left)),
              c_right(c_current.advance(d_right)),
              t_straight(map[c_straight]),
              t_left(map[c_left]),
              t_right(map[c_right]),
              free(countTiles(tile::space_on) + countTiles(tile::space_off)) {}

    int surroundings::countTiles(tile t) const {
        int no = 0;
        no += t_left == t;
        no += t_straight == t;
        no += t_right == t;
        return no;
    }

    steer steerOf(direction oldDir, direction newDir) {
        if (newDir == oldDir) {
            return steer::to_straight;
        } else if (newDir == direction_cw(oldDir)) {
            return steer::to_right;
        } else if (newDir == direction_ccw(oldDir)) {
            return steer::to_left;
        } else {
            // foo
            return steer::to_straight;
        }
    }
}