//
// Created by kuba on 26.10.18.
//

#include <program/include/io_sensor.hpp>
#include <program/include/base_names.hpp>
#include "io_sensor.hpp"
#include "ev3api.h"
#include <driver_interface_lcd.h>
#include <driver_interface_brick.h>
#include <driver_interface.h>

namespace duck {

    void sensor_configure(sensor_port where, sensor_type what) {
        debug("configure sensor[%d]", where+1);
        if (ev3_sensor_get_type((sensor_port_t) where) != (sensor_type_t) what) {
            ev3_sensor_config((sensor_port_t) where, (sensor_type_t) what);
        }
    }

    color_sensor_reflect::value_type color_sensor_reflect::measure_impl() const {
        int16_t a = ev3_color_sensor_get_reflect((sensor_port_t) m_port);
        return Fix16(a) / 100_ic;
    }

    color_sensor_rgb::value_type color_sensor_rgb::measure_impl() const {
        rgb_raw_t raw;
        rgb out{};
        ev3_color_sensor_get_rgb_raw((sensor_port_t) m_port, &raw);
        out.red = raw.r;
        out.grn = raw.g;
        out.blu = raw.b;
        return out;
    }

    ultrasonic_sensor::value_type ultrasonic_sensor::measure_impl() const {
        int16_t mm = ev3_ultrasonic_sensor_get_distance((sensor_port_t) m_port);
        return pos_lin(sf16(mm));
    }

    gyro_sensor_angle::value_type gyro_sensor_angle::measure_impl() const {
        int16_t angle = just_measure() - m_zero;
        return pos_ang::from_degrees(-angle);
    }

    void gyro_sensor_angle::recalibrate() {
        // that uses a different sensor mode
        ev3_gyro_sensor_get_angle((sensor_port_t) m_port);
        set_to_zero();
    }

    void gyro_sensor_angle::set_to_zero() {
        m_zero = just_measure();
    }

    int16_t gyro_sensor_angle::just_measure() const {
        int16_t angle;
        ev3_gyro_sensor_get_GnA((sensor_port_t) m_port, &angle, nullptr);
        return angle;
    }

    gyro_sensor_rate::value_type gyro_sensor_rate::measure_impl() const {
        int16_t rate;
        ev3_gyro_sensor_get_GnA((sensor_port_t) m_port, nullptr, &rate);
        return pos_ang::from_degrees(-rate) / time::unit();
    }

    touch_sensor::value_type touch_sensor::measure_impl() const {
        return ev3_touch_sensor_is_pressed((sensor_port_t) m_port) != 0;
    }

    button_sensor::button_sensor(button_port port) : m_port(port) {}

    button_sensor::value_type button_sensor::measure() {
        return ev3_button_is_pressed((button_t) m_port) != 0;
    }

    differential_sensor_factory::differential_sensor_factory(store &store)
            : m_port_color(diff_color, "1", store),
              m_port_touch(diff_touch, "2", store),
              m_port_gyro(diff_gyro, "3", store),
              m_port_sonic(diff_sonic, "4", store) {}

    color_sensor_rgb differential_sensor_factory::frontColor() {
        return color_sensor_rgb(m_port_color);
    }

    touch_sensor differential_sensor_factory::frontTouch() {
        return touch_sensor(m_port_touch);
    }

    gyro_sensor_angle differential_sensor_factory::gyroAngle() {
        return gyro_sensor_angle(m_port_gyro);
    }

    gyro_sensor_rate differential_sensor_factory::gyroRate() {
        return gyro_sensor_rate(m_port_gyro);
    }

    ultrasonic_sensor differential_sensor_factory::radarDistance() {
        return ultrasonic_sensor(m_port_sonic);
    }

    rgbf rgb_correction::operator()(const rgb &raw) const {
        rgbf out;
        out.red = fix16_clamp((Fix16(raw.red) - params.Rb) * params.Ra, 0_ic, 1_ic);
        out.grn = fix16_clamp((Fix16(raw.grn) - params.Gb) * params.Ga, 0_ic, 1_ic);
        out.blu = fix16_clamp((Fix16(raw.blu) - params.Bb) * params.Ba, 0_ic, 1_ic);
        return out;
    }

    rgb_correction::rgb_correction(store &store) : params(store) {}

    hc hc_convertor::operator()(const rgbf &rgb) const {
        hc out;

        // get max/min
        const Fix16 &max = std::max(std::max(rgb.red, rgb.grn), rgb.blu);
        const Fix16 &min = std::min(std::min(rgb.red, rgb.grn), rgb.blu);
        out.chroma = max - min;

        // CALC HUE //
        if (max == min) {
            out.hue = 0_ic;

        } else if (max == rgb.red && rgb.grn >= rgb.blu) {
            out.hue = 60_ic * (rgb.grn - rgb.blu) / (max - min) + 0_ic;

        } else if (max == rgb.red && rgb.grn < rgb.blu) {
            out.hue = 60_ic * (rgb.grn - rgb.blu) / (max - min) + 360_ic;

        } else if (max == rgb.grn) {
            out.hue = 60_ic * (rgb.blu - rgb.red) / (max - min) + 120_ic;

        } else if (max == rgb.blu) {
            out.hue = 60_ic * (rgb.red - rgb.grn) / (max - min) + 240_ic;

        } else {
            out.hue = 0_ic;
        }

        /*
        // CALC LIGHTNESS //
        out.lig = (max + min) / 2_ic;

        // CALC SATURATION //
        if (out.lig == 0_ic || max == min) {
            out.sat = 0_ic;

        } else if (out.lig > 0_ic && out.lig <= 0.5_fc) {
            out.sat = (max - min) / (2_ic * out.lig);

        } else if (out.lig > 0.5_fc) {
            out.sat = (max - min) / (2_ic - 2_ic * out.lig);

        } else {
            out.sat = 0_ic;
        }
         */

        return out;
    }

    rgb_params::rgb_params(store &store)
            : Ra(rgb_ra, "1.0", store),
              Rb(rgb_rb, "0.0", store),
              Ga(rgb_ga, "1.0", store),
              Gb(rgb_gb, "0.0", store),
              Ba(rgb_ba, "1.0", store),
              Bb(rgb_bb, "0.0", store) {}

    void rgb_params::update() {
        Ra.update();
        Rb.update();
        Ga.update();
        Gb.update();
        Ba.update();
        Bb.update();
    }

    rgb rgb_calibration::measureWhite() {
        max = sensor.average(color_calib_loops, color_calib_delay);
        return max;
    }

    rgb rgb_calibration::measureBlack() {
        min = sensor.average(color_calib_loops, color_calib_delay);
        return min;
    }

    void rgb_calibration::exportConfig(rgb_params &param) const {
        param.Rb = sf16(min.red);
        param.Gb = sf16(min.grn);
        param.Bb = sf16(min.blu);

        param.Ra = 1_ic / sf16(max.red - min.red);
        param.Ga = 1_ic / sf16(max.grn - min.grn);
        param.Ba = 1_ic / sf16(max.blu - min.blu);
    }

    rgb_calibration::rgb_calibration(color_sensor_rgb sensor) : sensor(sensor) {}

    rgb &rgb::operator+=(const rgb &other) {
        red += other.red;
        grn += other.grn;
        blu += other.blu;
        return *this;
    }

    rgb &rgb::operator/=(int16_t value) {
        red /= value;
        grn /= value;
        blu /= value;
        return *this;
    }

    motor_touch_sensor::motor_touch_sensor(motor_port port)
            : m_mask((uint8_t) port_bit(port)), m_mem(nullptr) {
        brickinfo_t brickinfo;
        fetch_brick_info(&brickinfo);
        m_mem = brickinfo.motor_lulz;

        char buf[2];
        buf[0] = (char) 0xE0;
        buf[1] = (char) port_bit(port);
        motor_command(&buf, sizeof(buf));
    }

    motor_touch_sensor::value_type motor_touch_sensor::measure() {
        return (*m_mem & m_mask) != 0;
    }
}