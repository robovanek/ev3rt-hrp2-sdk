
//
// Created by kuba on 18.10.18.
//
#include <utility>
#include <program/include/base_units.hpp>

#include "base_units.hpp"


namespace duck {

    heading heading::normalize() const {
        Fix16 angle = value;
        if (angle < 0_ipi || angle >= 2_ipi) {
            angle -= fix16_mul(fix16_floor(angle / 2_ipi), 2_ipi);
            return heading(angle);
        } else {
            return *this;
        }
    }

    heading heading::operator<<(const pos_ang &other) const {
        return heading(this->value + other.value).normalize();
    }

    heading heading::operator>>(const pos_ang &other) const {
        return heading(this->value - other.value).normalize();
    }

    direction heading::get_direction() const {
        if (in_range_center(heading(0.0_fpi), fix16_pi_half)) {
            return direction::east;
        } else if (in_range_center(heading(0.5_fpi), fix16_pi_half)) {
            return direction::north;
        } else if (in_range_center(heading(1.0_fpi), fix16_pi_half)) {
            return direction::north;
        } else if (in_range_center(heading(1.5_fpi), fix16_pi_half)) {
            return direction::south;
        } else {
            return heading(value + 0.01_fpi).get_direction();
        }
    }

    bool heading::in_range_center(heading center, Fix16 total) const {
        pos_ang half(total / 2_ic);
        return in_range_direct(center << half, center >> half);
    }

    bool heading::in_range_direct(heading left, heading right) const {
        left = left.normalize();
        right = right.normalize();
        heading nthis = this->normalize();

        if (left.value == right.value) {
            return (bool) (left.value == nthis.value);
        }


        // normal order
        if (left.value > right.value) {
            // test between
            return left.value >= nthis.value && nthis.value >= right.value;
        } else {
            // test not between inverse
            return !(right.value >= nthis.value && nthis.value >= left.value);
        }
    }

    pos_ang pos_ang::from_degrees(Fix16 degs) {
        return pos_ang(fix16_deg_to_rad(degs));
    }

    pos_ang pos_ang::from_degrees(int degs) {
        return pos_ang(fix16_deg_to_rad(sf16(degs)));
    }

    Fix16 pos_ang::to_degrees() const {
        return fix16_rad_to_deg(value);
    }

    Fix16 pos_ang::to_radians() const {
        return value;
    }

    heading heading::from_degrees(Fix16 degs) {
        return heading(fix16_deg_to_rad(degs));
    }

    constexpr heading heading::from_radians(Fix16 rads) {
        return heading(rads);
    }

    heading heading::from_degrees(int degs) {
        return heading(fix16_deg_to_rad(sf16(degs)));
    }

    Fix16 heading::to_degrees() const {
        return fix16_rad_to_deg(value);
    }

    Fix16 heading::to_radians() const {
        return value;
    }

    heading heading::from_angle(pos_ang angle) {
        return heading::from_radians(angle.to_radians()).normalize();
    }

    pos_ang heading::to_angle() const {
        return pos_ang::from_radians(to_radians());
    }


    vel_lin operator/(const pos_lin &dist, const time &time) {
        return vel_lin(dist.value / time.value);
    }

    vel_ang operator/(const pos_ang &dist, const time &time) {
        return vel_ang(dist.value / time.value);
    }

    acc_lin operator/(const vel_lin &vel, const time &time) {
        return acc_lin(vel.value / time.value);
    }

    acc_ang operator/(const vel_ang &vel, const time &time) {
        return acc_ang(vel.value / time.value);
    }

    pos_vec operator|(const pos_lin &x, const pos_lin &y) {
        return pos_vec(x, y);
    }

    vel_vec operator|(const vel_lin &x, const vel_lin &y) {
        return vel_vec(x, y);
    }

    acc_vec operator|(const acc_lin &x, const acc_lin &y) {
        return acc_vec(x, y);
    }

    vel_vec operator/(const pos_vec &vec, const time &dt) {
        return vel_vec(vec.x() / dt, vec.y() / dt);
    }

    acc_vec operator/(const vel_vec &vec, const time &dt) {
        return acc_vec(vec.x() / dt, vec.y() / dt);
    }

    vel_pol operator/(const pos_pol &vec, const time &dt) {
        return vel_pol(vec.length() / dt, vec.theta());
    }

    acc_pol operator/(const vel_pol &vec, const time &dt) {
        return acc_pol(vec.length() / dt, vec.theta());
    }

    pos_lin operator*(const pos_lin &dist, const pos_ang &ang) {
        return pos_lin(dist.value * ang.value);
    }

    vel_lin operator*(const vel_lin &dist, const pos_ang &ang) {
        return vel_lin(dist.value * ang.value);
    }

    acc_lin operator*(const acc_lin &dist, const pos_ang &ang) {
        return acc_lin(dist.value * ang.value);
    }

    vel_lin operator*(const vel_ang &ang, const pos_lin &radius) {
        return vel_lin(ang.value * radius.value);
    }

    acc_lin operator*(const acc_ang &ang, const pos_lin &radius) {
        return acc_lin(ang.value * radius.value);
    }

    vel_ang operator/(const vel_lin &dst, const pos_lin &radius) {
        return vel_ang(dst.value / radius.value);
    }

    acc_ang operator/(const acc_lin &dst, const pos_lin &radius) {
        return acc_ang(dst.value / radius.value);
    }

    Fix16 operator/(const pos_lin &a, const pos_lin &b) {
        return a.value / b.value;
    }

    pos_ang operator ""_deg(unsigned long long int a) {
        return pos_ang::from_degrees(sf16((int) a));
    }

    constexpr pos_ang operator ""_rad(long double a) {
        return pos_ang::from_radians(sf16((float) a));
    }

    constexpr pos_lin operator ""_mm(unsigned long long int a) {
        return pos_lin(sf16((int) a));
    }

    constexpr time operator ""_s(long double a) {
        return time(sf16((float) a));
    }

    pos_lin operator*(const vel_lin &vel, const time &time) {
        return pos_lin(vel.value * time.value);
    }

    pos_ang operator*(const vel_ang &vel, const time &time) {
        return pos_ang(vel.value * time.value);
    }

    vel_lin operator*(const acc_lin &acc, const time &time) {
        return vel_lin(acc.value * time.value);
    }

    vel_ang operator*(const acc_ang &acc, const time &time) {
        return vel_ang(acc.value * time.value);
    }

    direction direction_cw(direction old) {
        switch (old) {
            case direction::north:
                return direction::east;
            case direction::east:
                return direction::south;
            case direction::south:
                return direction::west;
            case direction::west:
            default:
                return direction::north;

        }
    }

    direction direction_ccw(direction old) {
        switch (old) {
            case direction::north:
                return direction::west;
            case direction::west:
                return direction::south;
            case direction::south:
                return direction::east;
            case direction::east:
            default:
                return direction::north;
        }
    }

    direction direction_opposite(direction old) {
        switch (old) {
            case direction::north:
                return direction::south;
            default:
            case direction::south:
                return direction::north;
            case direction::west:
                return direction::east;
            case direction::east:
                return direction::west;
        }
    }

    time time::now() {
        int64_t ticks = get_tick() - program_begin;
        Fix16 upperVal(sf16(static_cast<int>(ticks / 1000)));
        Fix16 lowerVal(sf16(static_cast<int>(ticks % 1000)));
        return time(upperVal + lowerVal / 1000_ic);
    }

    int64_t time::program_begin = 0;
}
