//
// Created by kuba on 18.11.18.
//

#include <program/include/plan_fsm.hpp>
#include <program/include/prg_main.hpp>
#include <ev3api.h>

namespace duck {

    void writeMsr(std::string &line, scanner &scan, sonic_to where) {
        switch (where) {
            case s_left:
                line += "L: ";
                break;
            case s_center:
                line += "C: ";
                break;
            case s_right:
                line += "R: ";
                break;
        }
        line += " [" + user_converter<cf::integer>::save(scan.of(where).latestID) + "] ";
        line += user_converter<pos_lin>::save(scan.of(where).distance) + "\n";
    }

    fsm::fsm(roboconfino::store &store, program &prg)
            : m_map(prg), m_where(store), m_us(store, prg), m_do(m_where, store),
              radar_offX(scan_offx, "0.0", store),
              radar_offY(scan_offy, "0.0", store),
              steer_off(steer_offset, "0.0", store),
              track_offX(track_offx, "0.0", store),
              track_offY(track_offy, "0.0", store),
              frontLength(rbt_front, "100", store),
              m_log(prg.create_logger("fsm")),
              m_sched(*this, m_map, prg) {
        debug("in fsm ctor");
        registerRPC(prg);
    }

    void fsm::update() {
        radar_offX.update();
        radar_offY.update();
        steer_off.update();
        track_offX.update();
        track_offY.update();
        frontLength.update();
        m_us.update();
        m_where.update(m_do.getTacho());

        if (m_passive) {
            return;
        }

        if (ev3_button_is_pressed(RIGHT_BUTTON)) {
            if (ev3_button_is_pressed(LEFT_BUTTON)) {
                m_map.reset_full();
                m_log.note("==== FULL RESET ====\n");
            } else {
                m_log.note("==== HALF RESET ====\n");
            }
            stop();
            while (ev3_button_is_pressed(RIGHT_BUTTON)) {
                do_sleep(10);
            }
            reset();
            start();
        }

        if (m_do.touchPressed()) {
            if (!was) {
                bool doIt = rand() % 2;
                if (doIt) {
                    reportPlanAlignTurn(steer::to_right);
                } else {
                    reportPlanAlignTurn(steer::to_left);
                }
                was = true;
            }
        } else {
            was = false;
        }


        switch (m_current) {
            case state::moving_to_edge:
                if (pastEdge() || (get_tick() - ts) > 10000) {
                    enterMeasure();
                }
                break;
            case state::measuring:
                // TODO make generic
                if (m_us.of(sonic_to::s_left).latestID == m_msrID) {
                    m_us.put(sonic_to::s_right);
                    recordMeasurements();
                    m_sched.planCross();
                }
                break;
            case state::steering_entry_wait:
                if (steerNow() || (get_tick() - ts) > 2000) {
                    enterSteering();
                }
                break;
            case state::steering:
                if (steerDone() || (get_tick() - ts) > 2000) {
                    enterMoveToEdge();
                }
                break;
            case state::align_wait:
                if (m_do.touchPressed() || m_do.colorPressed() || (get_tick() - ts) > 5000) {
                    if (m_do.touchPressed()) {
                        tacho_params tacho = m_do.getTacho();
                        direction newDir = heading::from_angle(m_where.theta()).get_direction();
                        pos_ang newAng = heading::from_direction(newDir).to_angle();
                        m_where.setPosition(fixPos(newDir), newAng, tacho.positionRight, tacho.positionLeft);
                    }
                    enterAlignBackStep();
                }
                break;
            case state::align_back_stepback:
                if (m_do.isDone() || (get_tick() - ts) > 2000) {
                    enterAlignBackTurn();
                }
                break;
            case state::align_back_turn:
                if (m_do.isDone() || (get_tick() - ts) > 2000) { // todo support more directions
                    enterMoveToEdge();
                }
                break;
        }
        m_do.update();
        if (m_where.position().x().value < Fix16(sf16(module)) ||
            m_where.position().x().value > Fix16(sf16(10 * module)) ||
            m_where.position().y().value < Fix16(sf16(module)) ||
            m_where.position().y().value > Fix16(sf16(7 * module))) {

            m_oldTrackPos = coords(5, 3);
            tacho_params t = m_do.getTacho();
            m_where.setPosition(m_oldTrackPos.to_vector(), head_north.to_angle(), t.positionRight, t.positionLeft);
            m_do.reset();
        }
    }

    pos_vec fsm::usPos() const {
        pos_vec off(radar_offX, radar_offY);
        pos_ang offRot = m_where.theta() - head_north.to_angle();
        pos_vec offGood = off << offRot;
        return offGood + m_where.position();
    }

    pos_vec fsm::trackPos() const {
        pos_vec off(track_offX, track_offY);
        pos_ang offRot = m_where.theta() - head_north.to_angle();
        pos_vec offGood = off << offRot;
        return offGood + m_where.position();
    }

    static constexpr Fix16 modulef(sf16(module));

    float filterCoord(const pos_lin &c) {
        return (float) (c.value / modulef);
    }

    bool fsm::pastEdge() {
        auto track = trackPos();
        auto curCoord = coords::from_vector(track);
        if (curCoord != m_oldTrackPos) {
            auto usLoc = usPos();
            m_oldTrackPos = curCoord;

            m_log.info("Passed the block edge");
            m_log.debug(" - position of tracker: [%f,%f]", filterCoord(track.x()), filterCoord(track.y()));
            m_log.debug(" - position of robot: [%f,%f]", filterCoord(m_where.position().x()),
                        filterCoord(m_where.position().y()));
            m_log.debug(" - position of US: [%f,%f]", filterCoord(usLoc.x()), filterCoord(usLoc.y()));
            m_log.debug(" - new coords: [%d,%d]", curCoord.x, curCoord.y);
            return true;
        }
        return false;
    }

    void fsm::reportPlanGo(steer direction) {
        m_steerDir = direction;
        if (direction == steer::to_straight) {
            enterMoveToEdge();
        } else {
            enterSteeringEntryWait();
        }
    }

    void fsm::reportPlanTurnaround() {
        m_steerDir = steer::to_straight;
        enterAlign();
    }

    void fsm::reportPlanAlignTurn(steer direction) {
        m_steerDir = direction;
        enterAlign();
    }

    void fsm::enterMeasure() {
        m_log.info("entering MEASUREMENT");
        sonic_req req(true, true, true);
        m_us.scan(sonic_to::s_right, req, ++m_msrID);
        m_do.goSlow();
        m_current = state::measuring;
        ts = get_tick();
    }

    void fsm::enterSteeringEntryWait() {
        m_log.info("entering STEERING WAIT");
        m_do.goFast();
        m_current = state::steering_entry_wait;
        ts = get_tick();
    }

    void fsm::enterSteering() {
        if (m_steerDir == steer::to_left) {
            m_log.info("entering STEER LEFT");
            m_do.steerLeft();
        } else {
            m_log.info("entering STEER RIGHT");
            m_do.steerRight();
        }
        m_current = state::steering;
        ts = get_tick();
    }

    void fsm::enterMoveToEdge() {
        m_log.info("entering MOVE TO EDGE");
        m_do.goFast();
        m_current = state::moving_to_edge;
        ts = get_tick();
    }

    void fsm::enterAlign() {
        m_log.info("entering ALIGN");
        m_do.goSlow();
        m_current = state::align_wait;
        ts = get_tick();
    }

    void fsm::enterAlignBackStep() {
        m_log.info("entering ALIGN BACK STEP");
        m_do.frontUnalign();
        m_current = state::align_back_stepback;
        ts = get_tick();
    }

    void fsm::enterAlignBackTurn() {
        m_log.info("entering ALIGN BACK TURN");
        switch (m_steerDir) {
            case steer::to_left:
                m_do.turnLeft();
                break;
            case steer::to_straight:
                m_do.turnAround();
                break;
            case steer::to_right:
                m_do.turnRight();
                break;
        }
        m_current = state::align_back_turn;
        ts = get_tick();
    }

    void fsm::recordMeasurements() {
        // todo make generic
        pos_vec sonicPos = usPos();
        direction rbtDir = heading::from_angle(m_where.theta()).get_direction();
        m_map.record_measurement(sonicPos, rbtDir,
                                 sonic_to::s_left, m_us.of(sonic_to::s_left).distance);
        m_map.record_measurement(sonicPos, rbtDir,
                                 sonic_to::s_center, m_us.of(sonic_to::s_center).distance);
        m_map.record_measurement(sonicPos, rbtDir,
                                 sonic_to::s_right, m_us.of(sonic_to::s_right).distance);
    }

    bool fsm::steerNow() {
        pos_vec center = coords::from_vector(usPos()).to_vector();
        pos_vec robot = m_where.position();
        pos_lin dist = (center - robot).length();
        return dist.value < steer_off.cached().value;
    }

    bool fsm::steerDone() {
        return m_do.isDone();
    }

    void fsm::reset() {
        m_us.put(sonic_to::s_right);
        m_oldTrackPos = coords(5, 3);
        tacho_params t = m_do.getTacho();
        m_where.setPosition(m_oldTrackPos.to_vector(), head_north.to_angle(), t.positionRight, t.positionLeft);
        m_do.reset();
    }

    void fsm::start() {
        enterMoveToEdge();
    }

    void fsm::registerRPC(program &prg) {
        prg.register_rpc("fsm.state", [this](const std::string &) -> std::string {
            switch (m_current) {
                case state::steering_entry_wait:
                    return "Waiting for steering time\n";
                case state::steering:
                    return "Steering\n";
                case state::moving_to_edge:
                    return "Moving to edge\n";
                case state::measuring:
                    return "Measuring\n";
                case state::align_wait:
                    return "Waiting for align\n";
                case state::align_back_stepback:
                    return "Going back after align\n";
                case state::align_back_turn:
                    return "Turning after align\n";
                default:
                    return "Unknown state\n";
            }
        });

        prg.register_rpc("map.state", [this](const std::string &) -> std::string {
            std::string lines;
            for (int y = map::height - 1; y >= 0; y--) {
                for (size_t x = 0; x < map::width; x++) {
                    switch (m_map[x][y]) {
                        case tile::unknown:
                            lines += "?";
                            break;
                        case tile::space_on:
                            lines += "*";
                            break;
                        case tile::space_off:
                            lines += "_";
                            break;
                        case tile::wall:
                            lines += "X";
                            break;
                        default:
                            lines += "E";
                            break;
                    }
                }
                lines += "\n";
            }
            return lines;
        });

        prg.register_rpc("loc.measurement", [this](const std::string &) -> std::string {
            pos_vec pos = m_where.position();
            pos_ang rot = m_where.theta();
            vel_lin vel = m_where.velocity();
            vel_ang rvl = m_where.angularVelocity();

            std::string lines;
            lines += "Position: [" + user_converter<Fix16>::save(pos.x().value) + "; " +
                     user_converter<Fix16>::save(pos.y().value) + "]\n";
            lines += "Rotation: " + user_converter<Fix16>::save(rot.to_degrees()) + " °\n";
            lines += "Velocity: " + user_converter<Fix16>::save(vel.value) + "\n";
            lines += "Rot. vel: " + user_converter<Fix16>::save(pos_ang::from_radians(rvl.value).to_degrees().value) +
                     " °/s\n";

            return lines;
        });

        prg.register_rpc("loc.tacho", [this](const std::string &) -> std::string {
            tacho_params p = m_do.getTacho();

            std::string lines;
            lines += "Tacho L: " + user_converter<cf::integer>::save(p.positionLeft) + "\n";
            lines += "Tacho R: " + user_converter<cf::integer>::save(p.positionRight) + "\n";
            lines += "Speed L: " + user_converter<cf::integer>::save(p.velocityLeft) + "\n";
            lines += "Speed R: " + user_converter<cf::integer>::save(p.velocityRight) + "\n";

            return lines;
        });

        prg.register_rpc("scan.measurement", [this](const std::string &) -> std::string {

            std::string lines;
            writeMsr(lines, m_us, sonic_to::s_left);
            writeMsr(lines, m_us, sonic_to::s_center);
            writeMsr(lines, m_us, sonic_to::s_right);
            return lines;
        });

        prg.register_rpc("scan.tacho", [this](const std::string &) -> std::string {

            std::string lines;
            lines += "Tacho: " + user_converter<cf::integer>::save(m_us.m_actuator.getTachoCount()) + "\n";
            lines += "Moving: " + user_converter<cf::boolean>::save(m_us.m_actuator.isMoving()) + "\n";

            return lines;
        });

        prg.register_rpc("scan.go.left", [this](const std::string &) -> std::string {
            m_us.m_actuator.moveLeft();
            return "Done.\n";
        });


        prg.register_rpc("scan.go.right", [this](const std::string &) -> std::string {
            m_us.m_actuator.moveRight();
            return "Done.\n";
        });

        prg.register_rpc("scan.idle.left", [this](const std::string &) -> std::string {
            m_us.m_actuator.idleLeft();
            return "Done.\n";
        });


        prg.register_rpc("scan.idle.right", [this](const std::string &) -> std::string {
            m_us.m_actuator.idleRight();
            return "Done.\n";
        });


        prg.register_rpc("robot.stop", [this](const std::string &) -> std::string {
            stop();
            return "Done.\n";
        });

        prg.register_rpc("robot.float", [this](const std::string &) -> std::string {
            stop();
            m_do.floatMotors();
            return "Done.\n";
        });

        prg.register_rpc("robot.reset_pose", [this](const std::string &) -> std::string {
            reset();
            return "Done.\n";
        });

        prg.register_rpc("color.calibrate.white", [this](const std::string &) -> std::string {
            rgb c = m_do.calibrateWhite();
            return formatn("Calibrated white: R %4d G %4d B %4d\n", c.red, c.grn, c.blu);
        });

        prg.register_rpc("color.calibrate.black", [this](const std::string &) -> std::string {
            rgb c = m_do.calibrateBlack();
            return formatn("Calibrated black: R %4d G %4d B %4d\n", c.red, c.grn, c.blu);
        });

        prg.register_rpc("color.calibrate.commit", [this](const std::string &) -> std::string {
            m_do.calibrateWrite();
            return "Commited.\n";
        });

        prg.register_rpc("color.msr", [this](const std::string &) -> std::string {
            hc h = m_do.currentColor();
            rgbf r = m_do.currentColorRgb();

            return formatn("Current HC  color: H %3d° C %f\n"
                           "Current RGB color: R %f G %f B %f\n",
                           h.hue.toInt(), (float) h.chroma,
                           (float) r.red, (float) r.grn, (float) r.blu);
        });

        prg.register_rpc("robot.passive", [this](const std::string &arg) -> std::string {
            m_passive = user_converter<cf::boolean>::load(arg);
            return "Set passive to " + user_converter<cf::boolean>::save(m_passive) + "\n";
        });
    }

    void fsm::stop() {
        m_do.stop();
    }

    pos_vec fsm::fixPos(direction direction) {
        pos_vec blockCenter = coords::from_vector(m_where.position()).to_vector();
        pos_lin add = pos_lin(sf16((int) (center - frontLength)));

        switch (direction) {
            default:
            case direction::north:
                return pos_vec(m_where.position().x(), blockCenter.y() + add);
            case direction::south:
                return pos_vec(m_where.position().x(), blockCenter.y() - add);
            case direction::east:
                return pos_vec(blockCenter.x() + add, m_where.position().y());
            case direction::west:
                return pos_vec(blockCenter.x() - add, m_where.position().y());
        }
    }
}