#include <utility>

//
// Created by kuba on 8.10.18.
//

#ifndef DUCK_PROGRAM_HPP
#define DUCK_PROGRAM_HPP

#include "comm_impl.hpp"
#include "store_manager.hpp"
#include "plan_fsm.hpp"

namespace duck {
    class do_import {
    public:
        do_import(store_manager &mgr, const std::string &path);
    };

    class program {
    public:
        program();

        void enter();

        void register_rpc(const std::string &name, rpc_target call);
        serial_logger create_logger(const std::string &component);
        store &config();

        void import_config();
        void export_config();

        volatile bool stop = false;
    private:
        std::string export_hook(const std::string &arg);

        store m_config;
        store_manager m_exporter;
        std::string m_config_path;
        do_import m_autoimport;
        Ev3rtChannel m_channel;
        Ev3rtComm m_comm;
        serial_buffer m_debug;
        fsm m_logic;
        serial_logger m_log;
    };
}

#endif //DUCK_PROGRAM_HPP
