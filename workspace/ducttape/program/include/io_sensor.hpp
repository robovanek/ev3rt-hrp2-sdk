//
// Created by kuba on 21.10.18.
//

#ifndef DUCTTAPE_SENSORS_A_HPP
#define DUCTTAPE_SENSORS_A_HPP

#include "integration.hpp"
#include "base_units.hpp"
#include "base_runnable.hpp"
#include "base_types.hpp"


namespace roboconfino {
    template<>
    struct user_converter<duck::sensor_port>
            : public base_user_converter<
                    user_converter<duck::sensor_port>,
                    duck::sensor_port,
                    cf::integer> {
        user_converter() = default;

        static parsed_type decode(const base_type &t) {
            return (parsed_type)(t - 1);
        }

        static base_type encode(const parsed_type &t) {
            return (base_type) t + 1;
        }
    };

    template<>
    struct user_converter<duck::button_port>
            : public base_user_converter<
                    user_converter<duck::button_port>,
                    duck::button_port,
                    cf::string> {
        user_converter() = default;

        static parsed_type decode(const base_type &t) {
            if (t == "up") {
                return duck::button_up;
            } else if (t == "down") {
                return duck::button_down;
            } else if (t == "left") {
                return duck::button_left;
            } else if (t == "right") {
                return duck::button_right;
            } else if (t == "enter") {
                return duck::button_enter;
            } else if (t == "back") {
                return duck::button_back;
            } else {
                return duck::button_enter;
            }
        }

        static base_type encode(const parsed_type &t) {
            switch (t) {
                case duck::button_left:
                    return "left";
                case duck::button_right:
                    return "right";
                case duck::button_up:
                    return "up";
                case duck::button_down:
                    return "down";
                case duck::button_enter:
                    return "enter";
                case duck::button_back:
                    return "back";
            }
        }
    };

    template<>
    struct user_converter<int16_t>
            : public base_user_converter<user_converter<int16_t>, int16_t, cf::integer> {

        static parsed_type decode(const base_type &t) {
            return (parsed_type) t;
        }

        static base_type encode(const parsed_type &t) {
            return (base_type) t;
        }
    };
}

namespace duck {
    struct rgb {
        rgb() = default;

        rgb(int16_t r, int16_t g, int16_t b) : red(r), grn(g), blu(b) {}

        rgb &operator+=(const rgb &other);
        rgb &operator/=(int16_t value);

        int16_t red = 0;
        int16_t grn = 0;
        int16_t blu = 0;
    };

    struct rgbf {
        rgbf() = default;

        rgbf(const Fix16 &r, const Fix16 &g, const Fix16 &b)
                : red(r), grn(g), blu(b) {}

        Fix16 red;
        Fix16 grn;
        Fix16 blu;
    };

    struct hc {
        hc() = default;

        hc(const Fix16 &h, const Fix16 &c)
                : hue(h), chroma(c) {}

        Fix16 hue;
        Fix16 chroma;
    };

    struct rgb_params : public base_runnable {
        explicit rgb_params(store &store);

        void update() override;

        field<Fix16> Ra;
        field<Fix16> Rb;
        field<Fix16> Ga;
        field<Fix16> Gb;
        field<Fix16> Ba;
        field<Fix16> Bb;
    };

    class rgb_correction {
    public:
        explicit rgb_correction(store &store);

        rgbf operator()(const rgb &raw) const;

        rgb_params params;
    };

    class hc_convertor {
    public:
        hc_convertor() = default;

        hc operator()(const rgbf &rgb) const;
    };

    extern void sensor_configure(sensor_port where, sensor_type what);

    template<typename DerivedT>
    class base_sensor {
    public:
        explicit base_sensor(sensor_port port)
                : m_port(port) {
            sensor_configure(m_port, DerivedT::type);
            measure();
        }

        sensor_port getPort() const {
            return m_port;
        }

        auto measure() {
            return static_cast<DerivedT *>(this)->measure_impl();
        }

        auto average(unsigned int loops, unsigned int delay) {
            typename DerivedT::value_type result;
            for (unsigned int i = 0; i < loops; i++) {
                result += measure();
                roboconfino::do_sleep(delay);
            }
            result /= loops;
            return result;
        }

    public:
        sensor_port m_port;
    };

    class color_sensor_reflect : public base_sensor<color_sensor_reflect> {
    public:
        static constexpr sensor_type type = sensor_color;
        typedef Fix16 value_type;
        using base_sensor::base_sensor;

    public:
        value_type measure_impl() const;
    };

    class color_sensor_rgb : public base_sensor<color_sensor_rgb> {
    public:
        static constexpr sensor_type type = sensor_color;
        typedef rgb value_type;
        using base_sensor::base_sensor;

    public:
        value_type measure_impl() const;
    };

    class ultrasonic_sensor : public base_sensor<ultrasonic_sensor> {
    public:
        static constexpr sensor_type type = sensor_ultrasonic;
        typedef pos_lin value_type;
        using base_sensor::base_sensor;

    public:
        value_type measure_impl() const;
    };

    class gyro_sensor_angle : public base_sensor<gyro_sensor_angle> {
    public:
        static constexpr sensor_type type = sensor_gyro;
        typedef pos_ang value_type;
        using base_sensor::base_sensor;

        void recalibrate();

        void set_to_zero();

    public:
        value_type measure_impl() const;

        int16_t just_measure() const;

        int16_t m_zero;
    };

    class gyro_sensor_rate : public base_sensor<gyro_sensor_rate> {
    public:
        static constexpr sensor_type type = sensor_gyro;
        typedef vel_ang value_type;
        using base_sensor::base_sensor;

    public:
        value_type measure_impl() const;
    };

    class touch_sensor : public base_sensor<touch_sensor> {
    public:
        static constexpr sensor_type type = sensor_touch;
        typedef bool value_type;
        using base_sensor::base_sensor;

    public:
        value_type measure_impl() const;
    };

    class motor_touch_sensor {
    public:
        typedef bool value_type;

        explicit motor_touch_sensor(motor_port port);

        value_type measure();

    private:
        uint8_t m_mask;
        volatile uint8_t *m_mem;
    };

    class button_sensor {
    public:
        typedef bool value_type;

        explicit button_sensor(button_port port);

        value_type measure();

    private:
        button_port m_port;
    };

    class rgb_calibration {
    public:
        explicit rgb_calibration(color_sensor_rgb sensor);

        rgb measureWhite();
        rgb measureBlack();

        void exportConfig(rgb_params &param) const;

    private:
        rgb max;
        rgb min;
        color_sensor_rgb sensor;
    };

    class differential_sensor_factory {
    public:
        explicit differential_sensor_factory(store &store);

        color_sensor_rgb frontColor();

        touch_sensor frontTouch();

        gyro_sensor_angle gyroAngle();

        gyro_sensor_rate gyroRate();

        ultrasonic_sensor radarDistance();

    private:
        roboconfino::field<sensor_port> m_port_color;
        roboconfino::field<sensor_port> m_port_touch;
        roboconfino::field<sensor_port> m_port_gyro;
        roboconfino::field<sensor_port> m_port_sonic;
    };
}

#endif //DUCTTAPE_SENSORS_A_HPP
