//
// Created by kuba on 18.10.18.
//

#ifndef DUCTTAPE_DUCKTYPES_HPP
#define DUCTTAPE_DUCKTYPES_HPP

#include <cstdint>
#include <roboconfino/include/our_types.hpp>

namespace duck {
    typedef uint8_t u8;
    typedef uint16_t u16;
    typedef uint32_t u32;
    typedef uint64_t u64;

    typedef int8_t s8;
    typedef int16_t s16;
    typedef int32_t s32;
    typedef int64_t s64;

    enum class direction {
        north,
        south,
        east,
        west
    };

    enum sonic_to {
        s_left = 0,
        s_center = 1,
        s_right = 2
    };

    enum class tile {
        unknown,
        space_on,
        space_off,
        wall,
        last = wall
    };

    inline bool isFree(tile t) {
        return t == tile::space_on || t == tile::space_off;
    }

    enum class steer {
        to_left,
        to_straight,
        to_right
    };

    struct tacho_params {
        int positionLeft;
        int positionRight;
        int velocityLeft;
        int velocityRight;
    };

    enum sensor_type {
        sensor_none = 0,
        sensor_ultrasonic,
        sensor_gyro,
        sensor_touch,
        sensor_color
    };

    enum sensor_port {
        port_s1 = 0,
        port_s2 = 1,
        port_s3 = 2,
        port_s4 = 3
    };

    enum button_port {
        button_left = 0,
        button_right = 1,
        button_up = 2,
        button_down = 3,
        button_enter = 4,
        button_back = 5
    };


    enum motor_port {
        port_a = 0,
        port_b = 1,
        port_c = 2,
        port_d = 3,
    };

    enum motor_mask {
        port_a_bit = 0x1,
        port_b_bit = 0x2,
        port_c_bit = 0x4,
        port_d_bit = 0x8,
    };

    inline motor_mask port_bit(motor_port port) {
        int value = 0x01 << (int) port;
        return (motor_mask) value;
    }

    enum motor_type {
        motor_none = 0,
        motor_medium,
        motor_large
    };

    using namespace roboconfino;
}

#endif //DUCTTAPE_DUCKTYPES_HPP
