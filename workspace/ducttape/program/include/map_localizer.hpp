//
// Created by kuba on 26.10.18.
//

#ifndef DUCTTAPE_ODOMETRY_HPP
#define DUCTTAPE_ODOMETRY_HPP

#include "base_units.hpp"
#include "base_types.hpp"
#include "io_sensor.hpp"
#include "field.hpp"

namespace duck {
    class odometry {
    public:
        explicit odometry(store &store);

        void update(const tacho_params &state);

        void setPosition(const pos_vec &pos, const pos_ang &ang, int nowR, int nowL);

        const pos_vec &position() const {
            return m_synth_loc;
        }

        pos_vec &position() {
            return m_synth_loc;
        }

        const pos_ang &theta() const {
            return m_synth_rot;
        }

        const vel_lin &velocity() const {
            return m_odom_v;
        }

        const vel_ang angularVelocity() const {
            return m_synth_w;
        }

        pos_ang odomGyroDifference() const;

        bool needsRefresh() const;

    private:
        /* OBJECTS */
        int64_t m_lastTime = 0;

        // discrete differentiators
        int diffWheel(int now, int &old);
        pos_ang diffAng(const pos_ang &now, pos_ang &old);

        // config
        field<pos_lin> radius;
        field<pos_lin> trackwidth;
        field<pos_ang> maxDifference;
        field<Fix16> pwr2vel;
        field<Fix16> gyroTrust;
        field<sensor_port> gyroPort;
        field<cf::integer> minDt;

        // gyro sensor
        gyro_sensor_angle m_gyroAng;
        gyro_sensor_rate m_gyroRate;

        /* STATE */

        // gyro state
        vel_ang m_gyro_w;
        pos_ang m_gyro_rot;
        pos_ang m_gyro_add;

        // odometry state
        vel_ang m_odom_w;
        pos_ang m_odom_add;

        // synthesized values
        pos_vec m_synth_loc;
        pos_ang m_synth_rot;
        vel_ang m_synth_w;
        vel_lin m_odom_v;

        // differentiation states
        int m_oldL;
        int m_oldR;
        pos_ang m_oldGyroAng;
    };
}

#endif //DUCTTAPE_ODOMETRY_HPP
