//
// Created by kuba on 21.10.18.
//

#ifndef DUCTTAPE_MOTORS_HPP
#define DUCTTAPE_MOTORS_HPP

#include "base_runnable.hpp"
#include "base_units.hpp"
#include "base_types.hpp"
#include "fix16.hpp"

namespace roboconfino {
    template<>
    struct user_converter<duck::motor_port>
            : public base_user_converter<
                    user_converter<duck::motor_port>,
                    duck::motor_port,
                    cf::string> {

        static parsed_type decode(const base_type &t) {
            if (t == "a") {
                return duck::port_a;
            } else if (t == "b") {
                return duck::port_b;
            } else if (t == "c") {
                return duck::port_c;
            } else if (t == "d") {
                return duck::port_d;
            } else {
                return duck::port_a;
            }
        }

        static base_type encode(const parsed_type &t) {
            switch (t) {
                case duck::port_a:
                    return "a";
                case duck::port_b:
                    return "b";
                case duck::port_c:
                    return "c";
                case duck::port_d:
                    return "d";
            }
        }
    };

    template<>
    struct user_converter<uint32_t>
            : public base_user_converter<user_converter<uint32_t>, uint32_t, cf::integer> {

        static parsed_type decode(const base_type &t) {
            return (parsed_type) t;
        }

        static base_type encode(const parsed_type &t) {
            return (base_type) t;
        }
    };

    template<>
    struct user_converter<int8_t>
            : public base_user_converter<user_converter<int8_t>, int8_t, cf::integer> {

        static parsed_type decode(const base_type &t) {
            return (parsed_type) t;
        }

        static base_type encode(const parsed_type &t) {
            return (base_type) t;
        }
    };
}

namespace duck {
    class radar_motor : public base_runnable {
    public:
        static constexpr motor_type type = motor_medium;

        explicit radar_motor(store &store);

        int getTachoCount() const;

        bool isMoving() const;

        void update() override;

        void stop();
        void moveLeft();
        void moveRight();

        void idleLeft();
        void idleRight();

        void moveTo(int relative);

        int getCenterTacho() const {
            return m_center_tacho;
        }

    private:
        void activate(int8_t power);

        roboconfino::field<motor_port> port;
        roboconfino::field<int8_t> power;
        roboconfino::field<int8_t> idle_power;
        roboconfino::field<int8_t> init_power;
        roboconfino::field<uint32_t> init_delay;
        roboconfino::field<int8_t> init_threshold;
        roboconfino::field<int8_t> init_xtreme;
        int m_center_tacho;
    };
}

#endif //DUCTTAPE_MOTORS_HPP
