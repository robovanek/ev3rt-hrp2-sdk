//
// Created by kuba on 18.10.18.
//

#ifndef DUCTTAPE_BASIC_GEOMETRY_HPP
#define DUCTTAPE_BASIC_GEOMETRY_HPP

#include <fix16.hpp>
#include <utility>
#include "base_types.hpp"
#include "field.hpp"

static constexpr fix16_t FIX16_POSITIVE_ONE = +0x00010000;
static constexpr fix16_t FIX16_NEGATIVE_ONE = -0x00010000;
static constexpr fix16_t fix16_minus_pi = -fix16_pi;
static constexpr fix16_t fix16_pi_half = fix16_pi / 2;
static constexpr fix16_t fix16_pi_fourth = PI_DIV_4;
static constexpr fix16_t fix16_pi_three_fourths = (3 * fix16_pi) / 4;
static constexpr fix16_t fix16_pi_eighth = 25736;

constexpr inline Fix16 operator ""_ic(unsigned long long int a) {
    return Fix16(sf16((int) a));
}

constexpr inline Fix16 operator ""_fc(long double a) {
    return Fix16(sf16((float) a));
}

inline Fix16 operator ""_fpi(long double a) {
    if (a == 0.0L) {
        return 0_ic;
    } else if (a == 0.5L) {
        return Fix16((fix16_t) fix16_pi_half);
    } else if (a == 1.0L) {
        return Fix16((fix16_t) fix16_pi);
    } else if (a == 1.5L) {
        return Fix16((fix16_t) (fix16_pi + fix16_pi_half));
    } else if (a == 2.0L) {
        return Fix16((fix16_t) (fix16_pi * 2));
    }
    return Fix16(fix16_mul(sf16((float) a), fix16_pi));
}

constexpr inline Fix16 operator ""_ipi(unsigned long long int a) {
    return (fix16_t) (a * fix16_pi);
}

namespace duck {

    template<typename CoordT>
    struct pol;

    template<typename CoordT>
    struct vec;

    template<typename T, typename CoordT>
    using enable_if_vector = typename
    std::enable_if<std::is_same<typename std::decay<T>::type, typename std::decay<vec<CoordT>>::type>::value ||
                   std::is_same<typename std::decay<T>::type, typename std::decay<pol<CoordT>>::type>::value>;

    template<typename DerivedT>
    class scalar {
    public:
        constexpr scalar() = default;

        explicit constexpr scalar(const Fix16 &value) : value(value) {}

        DerivedT operator+(const DerivedT &other) const {
            return DerivedT(value + other.value);
        }

        DerivedT operator-(const DerivedT &other) const {
            return DerivedT(value - other.value);
        }

        friend DerivedT operator*(const DerivedT &a, const Fix16 &b) {
            return DerivedT(a.value * b);
        }

        friend DerivedT operator*(const Fix16 &a, const DerivedT &b) {
            return DerivedT(a * b.value);
        }

        friend DerivedT operator/(const DerivedT &a, const Fix16 &b) {
            return DerivedT(a.value / b);
        }

        DerivedT operator+() const {
            return DerivedT(+value);
        }

        DerivedT operator-() const {
            return DerivedT(-value);
        }

        explicit operator Fix16() const {
            return value;
        }

        static DerivedT zero() {
            return DerivedT(0_ic);
        }

        static DerivedT unit() {
            return DerivedT(1_ic);
        }

        Fix16 value = 0_ic;
    };


    struct pos_ang : public scalar<pos_ang> {
    public:
        using scalar::scalar;

        static constexpr pos_ang from_radians(Fix16 rads) {
            return pos_ang(rads);
        }

        static pos_ang from_degrees(Fix16 degs);

        static pos_ang from_degrees(int degs);

        Fix16 to_degrees() const;

        Fix16 to_radians() const;
    };

    struct heading : public scalar<heading> {
    public:
        using scalar::scalar;

        heading operator<<(const pos_ang &other) const;

        heading operator>>(const pos_ang &other) const;

        heading normalize() const;

        bool in_range_center(heading center, Fix16 total) const;

        bool in_range_direct(heading left, heading right) const;

        direction get_direction() const;

        static constexpr heading from_radians(Fix16 rads);

        static heading from_degrees(Fix16 degs);

        static heading from_degrees(int degs);

        static constexpr heading from_direction(direction dir) {
            fix16_t angle = (dir == direction::east ? 0 :
                             dir == direction::north ? 1 :
                             dir == direction::west ? 2 :
                             dir == direction::south ? 3 : 0) * fix16_pi_half;
            return heading(angle);
        }

        Fix16 to_degrees() const;

        Fix16 to_radians() const;

        static heading from_angle(pos_ang angle);

        pos_ang to_angle() const;
    };

    static constexpr heading head_north = heading::from_direction(direction::north);
    static constexpr heading head_south = heading::from_direction(direction::south);
    static constexpr heading head_east = heading::from_direction(direction::east);
    static constexpr heading head_west = heading::from_direction(direction::west);

    direction direction_cw(direction old);
    direction direction_ccw(direction old);
    direction direction_opposite(direction old);

    template<typename CoordT>
    struct vec {
    public:
        constexpr vec() = default;

        explicit constexpr vec(const CoordT &x, const CoordT &y) : m_x(x), m_y(y) {}

        explicit constexpr vec(const Fix16 &x, const Fix16 &y) : m_x(x), m_y(y) {}

        explicit constexpr vec(const CoordT &len, direction where) : m_x(0_ic), m_y(0_ic) {
            switch (where) {
                case direction::north:
                    m_y = +len;
                    break;
                case direction::south:
                    m_y = -len;
                    break;
                case direction::east:
                    m_x = +len;
                    break;
                case direction::west:
                    m_x = -len;
                    break;
            }
        }

        explicit constexpr vec(const Fix16 &len, direction where) : vec(CoordT(len), where) {}

        template<typename VecT, typename = typename enable_if_vector<VecT, CoordT>::type>
        vec<CoordT> operator+(const VecT &other) const {
            return vec(this->x() + other.x(), this->y() + other.y());
        }

        template<typename VecT, typename = typename enable_if_vector<VecT, CoordT>::type>
        vec<CoordT> operator-(const VecT &other) const {
            return vec(this->x() - other.x(), this->y() - other.y());
        }

        vec<CoordT> operator*(const Fix16 &factor) const {
            return vec(this->x() * factor, this->y() * factor);
        }

        vec<CoordT> operator/(const Fix16 &factor) const {
            return vec(this->x() / factor, this->y() / factor);
        }

        vec<CoordT> operator<<(const pos_ang &rotation) const {
            Fix16 cos = rotation.value.cos();
            Fix16 sin = rotation.value.sin();

            CoordT newX = x() * cos - y() * sin;
            CoordT newY = x() * sin + y() * cos;
            return vec(newX, newY);
        }

        vec<CoordT> operator>>(const pos_ang &rotation) const {
            return *this << -rotation;
        }

        vec<CoordT> operator+() const {
            return *this;
        }

        vec<CoordT> operator-() const {
            return vec(-x(), -y());
        }

        template<typename VecT, typename = typename enable_if_vector<VecT, CoordT>::type>
        pos_ang angleTo(const VecT &other) {
            return other.theta().to_angle() - this->theta().to_angle();
        }

        CoordT length() const {
            fix16_t xx = fix16_sq(x().value);
            fix16_t yy = fix16_sq(y().value);
            Fix16 length = fix16_sqrt(xx + yy);
            return CoordT(length);
        }

        heading theta() const {
            Fix16 ang = fix16_atan2(y().value, x().value);
            if (ang < 0_ipi) {
                ang = 2_ipi - ang;
            }
            return heading();
        }

        vec<CoordT> normalize() const {
            Fix16 len = length().value;
            return vec(x() / len, y() / len);
        }

        static vec<CoordT> unit(heading hd) {
            return unit() << pos_ang(hd.value);
        }

        static vec<CoordT> unit() {
            return vec(CoordT::unit(), CoordT::zero());
        }

        CoordT x() const {
            return m_x;
        }

        CoordT y() const {
            return m_y;
        }

    private:
        CoordT m_x;
        CoordT m_y;
    };


    template<typename CoordT>
    struct pol {
    public:
        constexpr pol() = default;

        explicit constexpr pol(const CoordT &length, const heading &theta)
                : m_length(length), m_theta(theta) {}

        explicit constexpr pol(const Fix16 &length, const Fix16 &theta)
                : m_length(length), m_theta(theta) {}

        explicit pol(const vec<CoordT> &vect)
                : m_length(vect.length()),
                  m_theta(vect.theta()) {}

        // WARNING: SLOW!
        template<typename VecT, typename = typename enable_if_vector<VecT, CoordT>::type>
        pol<CoordT> operator+(const VecT &other) const {
            return pol(vec<CoordT>(*this) + vec<CoordT>(other));
        }

        // WARNING: SLOW!
        template<typename VecT, typename = typename enable_if_vector<VecT, CoordT>::type>
        pol<CoordT> operator-(const VecT &other) const {
            return pol(vec<CoordT>(*this) - vec<CoordT>(other));
        }

        operator vec<CoordT>() const {
            return vec<CoordT>(this->x(), this->y());
        }

        pol<CoordT> operator*(Fix16 factor) const {
            return pol(this->length() * factor, this->theta());
        }

        pol<CoordT> operator/(Fix16 factor) const {
            return pol(this->length() / factor, this->theta());
        }

        pol<CoordT> operator<<(const pos_ang &rotation) const {
            return pol(this->length(), (this->theta() + rotation).normalize());
        }

        pol<CoordT> operator>>(const pos_ang &rotation) const {
            return pol(this->length(), (this->theta() - rotation).normalize());
        }

        pol<CoordT> operator+() const {
            return *this;
        }

        pol<CoordT> operator-() const {
            return pol(this->length(), (this->theta() + pos_ang(1_ipi)).normalize());
        }

        template<typename VecT, typename = typename enable_if_vector<VecT, CoordT>::type>
        pos_ang angleTo(const VecT &other) {
            return other.theta().to_angle() - this->theta().to_angle();
        }

        heading theta() const {
            return m_theta;
        }

        CoordT length() const {
            return m_length;
        }


        pol<CoordT> normalize() const {
            return pol(1_ic, this->theta().normalize().value);
        }

        CoordT x() const {
            return CoordT(theta().value.cos() * length().value);
        }

        CoordT y() const {
            return CoordT(theta().value.sin() * length().value);
        }

        pol<CoordT> unit() {
            return pol(CoordT::unit(), heading::zero());
        }

        pol<CoordT> unit(heading what) {
            return pol(CoordT::unit(), what.normalize());
        }

    private:
        CoordT m_length;
        heading m_theta;
    };

    struct pos_lin : public scalar<pos_lin> {
    public:
        using scalar::scalar;
    };

    class time : public scalar<time> {
    public:
        using scalar::scalar;

        static time now();

        static int64_t program_begin;
    };

    struct vel_lin : public scalar<vel_lin> {
    public:
        using scalar::scalar;
    };

    struct vel_ang : public scalar<vel_ang> {
    public:
        using scalar::scalar;
    };

    struct acc_lin : public scalar<acc_lin> {
    public:
        using scalar::scalar;
    };

    struct acc_ang : public scalar<acc_ang> {
    public:
        using scalar::scalar;
    };


    vel_lin operator/(const pos_lin &dist, const time &time);

    vel_ang operator/(const pos_ang &dist, const time &time);

    acc_lin operator/(const vel_lin &vel, const time &time);

    acc_ang operator/(const vel_ang &vel, const time &time);


    pos_lin operator*(const vel_lin &vel, const time &time);

    pos_ang operator*(const vel_ang &vel, const time &time);

    vel_lin operator*(const acc_lin &acc, const time &time);

    vel_ang operator*(const acc_ang &acc, const time &time);

    // radius to circumference
    pos_lin operator*(const pos_lin &dist, const pos_ang &ang);

    vel_lin operator*(const vel_lin &dist, const pos_ang &ang);

    acc_lin operator*(const acc_lin &dist, const pos_ang &ang);

    // angular to metric
    vel_lin operator*(const vel_ang &ang, const pos_lin &radius);

    acc_lin operator*(const acc_ang &ang, const pos_lin &radius);

    // metric to angular
    vel_ang operator/(const vel_lin &dst, const pos_lin &radius);

    acc_ang operator/(const acc_lin &dst, const pos_lin &radius);

    Fix16 operator/(const pos_lin &a, const pos_lin &b);

    using pos_vec = vec<pos_lin>;
    using vel_vec = vec<vel_lin>;
    using acc_vec = vec<acc_lin>;
    using pos_pol = pol<pos_lin>;
    using vel_pol = pol<vel_lin>;
    using acc_pol = pol<acc_lin>;


    pos_vec operator|(const pos_lin &x, const pos_lin &y);

    vel_vec operator|(const vel_lin &x, const vel_lin &y);

    acc_vec operator|(const acc_lin &x, const acc_lin &y);

    vel_vec operator/(const pos_vec &vec, const time &dt);

    acc_vec operator/(const vel_vec &vec, const time &dt);

    vel_pol operator/(const pos_pol &vec, const time &dt);

    acc_pol operator/(const vel_pol &vec, const time &dt);


    pos_ang operator "" _deg(unsigned long long int a);

    constexpr pos_ang operator "" _rad(long double a);

    constexpr pos_lin operator "" _mm(unsigned long long int a);

    constexpr time operator "" _s(long double a);

    template<typename T>
    using enable_scalarconv = typename
    std::enable_if<
            std::is_base_of<scalar<typename std::decay<T>::type>, typename std::decay<T>::type>::value
            && !std::is_same<typename std::decay<T>::type, pos_ang>::value>;

    template<typename T>
    inline constexpr
    int8_t sgn(T x, std::false_type is_signed) {
        return T(0) < x;
    }

    template<typename T>
    inline constexpr
    int8_t sgn(T x, std::true_type is_signed) {
        return (T(0) < x) - (x < T(0));
    }

    template<typename T>
    inline constexpr
    int8_t sgn(T x) {
        return sgn(x, std::is_signed<T>());
    }

    template<typename IntT>
    IntT gabs(IntT arg) {
        if (arg < IntT()) {
            return -arg;
        } else {
            return arg;
        }
    }
}

namespace roboconfino {
    template<>
    class user_converter<Fix16>
            : public base_user_converter<user_converter<Fix16>, Fix16, cf::floating> {
    public:
        user_converter() = default;

        static parsed_type decode(const base_type &t) {
            return Fix16(t);
        }

        static base_type encode(const parsed_type &t) {
            return (float) t;
        }
    };

    template<typename DerivedT>
    class user_converter<DerivedT, typename duck::enable_scalarconv<DerivedT>::type>
            : public base_user_converter<user_converter<DerivedT, typename duck::enable_scalarconv<DerivedT>::type>, DerivedT, cf::floating> {
    private:
        typedef typename base_user_converter<user_converter<DerivedT, typename duck::enable_scalarconv<DerivedT>::type>, DerivedT, cf::floating>::parsed_type pt2;
        typedef typename base_user_converter<user_converter<DerivedT, typename duck::enable_scalarconv<DerivedT>::type>, DerivedT, cf::floating>::base_type bt2;
    public:

        user_converter() = default;

        static pt2 decode(const bt2 &t) {
            return DerivedT(Fix16(sf16(t)));
        }

        static bt2 encode(const pt2 &t) {
            return (float) t.value;
        }
    };

    template<>
    class user_converter<duck::pos_ang>
            : public base_user_converter<user_converter<duck::pos_ang>, duck::pos_ang, cf::floating> {
    public:
        static parsed_type decode(base_type t) {
            return duck::pos_ang::from_degrees(Fix16(sf16(t)));
        }

        static base_type encode(const parsed_type &p) {
            return (float) p.to_degrees();
        }
    };

}

#endif //DUCTTAPE_BASIC_GEOMETRY_HPP
