//
// Created by kuba on 18.11.18.
//

#ifndef DUCTTAPE_PLAN_SCHEDULER_HPP
#define DUCTTAPE_PLAN_SCHEDULER_HPP

#include "base_types.hpp"
#include "base_units.hpp"
#include "map_tiles.hpp"
#include "map_path.hpp"
#include "serial.hpp"

namespace duck {
    steer steerOf(direction oldDir, direction newDir);

    struct surroundings {
        surroundings(const pos_vec &usPos, const pos_ang &rbtAng, const map &map);

        coords c_current;

        direction d_straight;
        direction d_left;
        direction d_right;

        coords c_straight;
        coords c_left;
        coords c_right;

        tile t_straight;
        tile t_left;
        tile t_right;

        int free;

        int countTiles(tile t) const;
    };

    class fsm;
    class program;

    class scheduler {
    public:
        scheduler(fsm &fsm, map &map, program &prg);

        void planCross();


    private:
        void returnTurnaround();
        void returnWay(steer free);

        void planMultiCross(surroundings &around);
        void planByPath(direction curDir, path &&pth);
        bool planTryPath(const surroundings &around, const path_finder &type);

        fsm *m_fsm;
        map *m_map;
        path_finder m_find_light;
        path_finder m_find_unknown;
        roboconfino::serial_logger m_log;
    };
}

#endif //DUCTTAPE_PLAN_SCHEDULER_HPP
