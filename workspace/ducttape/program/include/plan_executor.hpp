//
// Created by kuba on 17.11.18.
//

#ifndef DUCTTAPE_PRG_EXECUTOR_HPP
#define DUCTTAPE_PRG_EXECUTOR_HPP


#include <roboconfino/include/store.hpp>
#include "base_runnable.hpp"
#include "base_types.hpp"
#include "io_sensor.hpp"
#include "map_localizer.hpp"
#include "io_motor.hpp"

namespace duck {

    class executor : public base_runnable {
    public:
        explicit executor(odometry &odom, roboconfino::store &conf);

        void update() override;

        tacho_params getTacho();

        void steerLeft();
        void steerRight();

        void turnLeft();
        void turnRight();
        void turnAround();

        void goFast();
        void goFastReg();
        void goSlow();
        void stop();
        void floatMotors();

        void frontAlign();
        void frontUnalign();

        bool touchPressed();
        bool colorPressed();

        void reset();

        bool isDone() const;

        rgb calibrateWhite();
        rgb calibrateBlack();
        void calibrateWrite();
        hc currentColor();
        rgbf currentColorRgb();

    private:
        enum class state {
            none,
            steer_reg,
            around_reg,
        };

        state m_current = state::none;
        heading m_startHead;
        steer m_steerDir = steer::to_right;

        field<sensor_port> colorPort;
        field<motor_port> touchPort;
        field<motor_port> leftPort;
        field<motor_port> rightPort;
        field<Fix16> ghostChromaThreshold;
        field<int8_t> fastPower;
        field<int8_t> slowPower;
        field<int8_t> turnPower;
        field<int8_t> steerPower;
        field<int8_t> turnRatio;
        field<uint32_t> turn1Len;
        field<uint32_t> turn2Len;
        field<uint32_t> unalignLen;
        field<pos_ang> steerAngle;

        bool m_done = false;

        color_sensor_rgb m_color;
        rgb_calibration m_color_calib;
        motor_touch_sensor m_touch;
        rgb_correction m_color_curves;
        hc_convertor m_color_hsl;
        odometry *m_odom;
    };
}

#endif //DUCTTAPE_PRG_EXECUTOR_HPP
