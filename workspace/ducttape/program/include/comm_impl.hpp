//
// Created by kuba on 6.10.18.
//

#ifndef DUCK_NEWCOMM_HPP
#define DUCK_NEWCOMM_HPP

#include "base_types.hpp"
#include <functional>
#include <forward_list>
#include "framing.hpp"
#include "config.hpp"
#include "protocol.hpp"
#include "serial.hpp"
#include "store.hpp"
#include "base_runnable.hpp"


namespace duck {
    class Ev3rtChannel;
    class Ev3rtComm;

    typedef std::function<std::string(const std::string &)> rpc_target;
    typedef std::unordered_map<std::string, rpc_target> rpc_register;

    class Ev3rtComm : public datagram_visitor {
    public:
        explicit Ev3rtComm(Ev3rtChannel &chan, store &config, serial_buffer &dbg);

        void operator()(err &pkt) override;

        void operator()(ack &pkt) override;

        void operator()(configure &pkt) override;

        void operator()(file_to_brick &pkt) override;

        void operator()(file_to_pc &pkt) override;

        void operator()(get_option_all &pkt) override;

        void operator()(get_option &pkt) override;

        void operator()(set_option &pkt) override;

        void operator()(reply_string &pkt) override;

        void operator()(reply_bytes &pkt) override;

        void operator()(fail_reason ko) override;

        void operator()(request_serial &out) override;

        void operator()(rpc_request &rpc) override;

        void operator()(rpc_list &list) override;

        void register_rpc(const std::string &name, rpc_target rpc);

    private:
        rpc_register m_rpcs;
        store *m_config;
        serial_buffer *m_serial;
        Ev3rtChannel *m_chan;
        mutable lockable m_rpc_lock;
    };

    class Ev3rtChannel : public base_runnable, public GenericChannel {
        friend class MessageReader;

        friend class MessageWriter;

    public:
        explicit Ev3rtChannel(Ev3rtComm &comm);

        void reset() override;

        void update() override;

        void sendMessage(const std::vector<cf::byte> &data) override;

    private:
        void receivedMessage(const std::vector<cf::byte> &data) override;

        void writeDirect(const cf::byte *data, size_t len) override;

    private:
        InputQueue m_queue;
        InputBarrier m_rdevent;
        MessageReader m_read;
        MessageWriter m_write;
        Ev3rtComm *m_comm;
    };
}

#endif //DUCK_NEWCOMM_HPP
