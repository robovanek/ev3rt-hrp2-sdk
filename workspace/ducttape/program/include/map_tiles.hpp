//
// Created by kuba on 18.10.18.
//

#ifndef DUCTTAPE_TILE_MAP_HPP
#define DUCTTAPE_TILE_MAP_HPP

#include <array>
#include <vector>
#include <roboconfino/include/serial.hpp>
#include "base_types.hpp"
#include "base_units.hpp"
#include "base_names.hpp"

namespace duck {
    class map;

    struct coords {
    public:
        constexpr coords() = default;

        constexpr coords(size_t x, size_t y) : x(x), y(y) {}

        coords operator+(const coords &other) const {
            return coords(x + other.x, y + other.y);
        }

        coords operator-(const coords &other) const {
            return coords(x - other.x, y - other.y);
        }

        coords advance(direction how, int amount = 1) const {
            size_t newX = x;
            size_t newY = y;
            switch (how) {
                case direction::north:
                    newY += amount;
                    break;
                case direction::south:
                    newY -= amount;
                    break;
                case direction::east:
                    newX += amount;
                    break;
                case direction::west:
                    newX -= amount;
                    break;
            }
            return coords(newX, newY);
        }

        direction dirOfNext(const coords &next);

        friend bool operator==(const coords &a, const coords &b);

        friend bool operator!=(const coords &a, const coords &b);

        template<typename VecT, typename = typename enable_if_vector<VecT, pos_lin>::type>
        static coords from_vector(VecT &&real) {
            Fix16 coordX = (real.x().value - Fix16(sf16(center))) / sf16(module);
            Fix16 coordY = (real.y().value - Fix16(sf16(center))) / sf16(module);
            auto x = static_cast<size_t>(int16_t(coordX));
            auto y = static_cast<size_t>(int16_t(coordY));
            return coords(x, y);
        }

        pos_vec to_vector() const;

        size_t x = 0;
        size_t y = 0;
    };

    namespace priv {
        template<bool constant>
        struct map_ref;

        template<>
        struct map_ref<false> {
            typedef duck::map &reference;
            typedef duck::map *pointer;
        };

        template<>
        struct map_ref<true> {
            typedef const duck::map &reference;
            typedef const duck::map *pointer;
        };

        template<bool constant>
        class partial_ref {
        private:
            typedef typename map_ref<constant>::pointer map_pointer;
            typedef typename map_ref<constant>::reference map_reference;
        public:
            partial_ref() = default;

            partial_ref(map_reference map, size_t x)
                    : m_map(&map), m_x(x) {}

            tile &operator[](size_t y) {
                return m_map->at(coords(m_x, y));
            }

            const tile &operator[](size_t y) const {
                return m_map->at(coords(m_x, y));
            }

        private:
            map_pointer m_map = nullptr;
            size_t m_x = 0;
        };
    }

    class program;
    class map {
    public:
        static constexpr size_t width = 1 + 9 + 1;
        static constexpr size_t height = 1 + 6 + 1;

        map(program &prg);

        auto operator[](size_t x) const {
            return priv::partial_ref<true>(*this, x);
        }

        auto operator[](size_t x) {
            return priv::partial_ref<false>(*this, x);
        }

        tile &operator[](const coords &xy) {
            return at(xy);
        }

        const tile &operator[](const coords &xy) const {
            return at(xy);
        }

        tile &at(const coords &coord);

        const tile &at(const coords &coord) const;

        void record_measurement(const pos_vec &usPos, direction rbtAng, sonic_to usDir, const pos_lin &dist);

        void reset_sides();

        void reset_full();

        static constexpr size_t index(size_t x, size_t y) {
            if (outside_bounds(x, y)) {
                return 0;
            } else {
                return y * width + x;
            }
        }

        static constexpr bool outside_bounds(size_t x, size_t y) {
            return x < 0 || y < 0 || x >= width || y >= height;
        }

    private:
        roboconfino::serial_logger m_log;
        std::vector<tile> m_storage;
        static tile dummy;
    };
}

namespace std
{
    template <>
    struct hash<duck::coords>
    {
        size_t operator()(const duck::coords& k) const
        {
            return k.x << 8 | k.y << 0;
        }
    };
}

#endif //DUCTTAPE_TILE_MAP_HPP
