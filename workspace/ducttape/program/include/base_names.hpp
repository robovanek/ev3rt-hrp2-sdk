//
// Created by kuba on 26.10.18.
//

#ifndef DUCTTAPE_IDENTIFIERS_HPP
#define DUCTTAPE_IDENTIFIERS_HPP

#include <fixed/include/fix16.hpp>

namespace duck {
    static constexpr const char *duck_config_path = "/config.txt";
    static constexpr const char *wheel_radius = "diff.wheel.radius";
    static constexpr const char *wheel_dist = "diff.wheel.distance";

    static constexpr const char *diff_color = "port.sensor.color";
    static constexpr const char *diff_touch = "port.sensor.touch";
    static constexpr const char *diff_sonic = "port.sensor.sonic";
    static constexpr const char *diff_gyro = "port.sensor.gyro";
    static constexpr const char *diff_left = "port.motor.left";
    static constexpr const char *diff_right = "port.motor.right";

    static constexpr const char *scan_port = "map_scanner.port";
    static constexpr const char *scan_pwr  = "map_scanner.pwr";
    static constexpr const char *scan_dly = "map_scanner.dly";
    static constexpr const char *scan_cdly = "map_scanner.center.dly";
    static constexpr const char *scan_ang = "map_scanner.ang0";
    static constexpr const char *scan_idPwr = "map_scanner.idle.pwr";
    static constexpr const char *scan_inPwr = "map_scanner.init.pwr";
    static constexpr const char *scan_inDly = "map_scanner.init.dly";
    static constexpr const char *scan_inThr = "map_scanner.init.thr";
    static constexpr const char *scan_inEdge = "map_scanner.init.edge";
    static constexpr const char *scan_offx = "map_scanner.offset.x";
    static constexpr const char *scan_offy = "map_scanner.offset.y";

    static constexpr const char *steer_offset = "ride.steer.offwait";
    static constexpr const char *track_offx = "ride.edger.offset.x";
    static constexpr const char *track_offy = "ride.edger.offset.y";

    static constexpr const char *rgb_ra = "rgb.red.a";
    static constexpr const char *rgb_rb = "rgb.red.b";
    static constexpr const char *rgb_ga = "rgb.green.a";
    static constexpr const char *rgb_gb = "rgb.green.b";
    static constexpr const char *rgb_ba = "rgb.blue.a";
    static constexpr const char *rgb_bb = "rgb.blue.b";

    static constexpr const char *gyro_trust   = "odom.gyro.trust";
    static constexpr const char *gyro_maxdiff = "odom.gyro.maxdiff";
    static constexpr const char *odom_wheelratio = "odom.wheel.power2vel";
    static constexpr const char *odom_mindt = "odom.minDt";
    static constexpr const char *rbt_front = "odom.front.len";

    static constexpr const char *ghost_chroma = "ghost.chroma";
    static constexpr const char *power_fast = "exec.pwr.fast";
    static constexpr const char *power_slow = "exec.pwr.slow";
    static constexpr const char *power_turn = "exec.pwr.turn";
    static constexpr const char *power_steer = "exec.pwr.steer";
    static constexpr const char *turn_ratio = "exec.turnratio";
    static constexpr const char *turn_1_len = "exec.len.turn90";
    static constexpr const char *turn_2_len = "exec.len.turn180";
    static constexpr const char *unalign_len = "exec.len.unalign";
    static constexpr const char *steer_angle = "exec.len.angle";

    static constexpr int center = 140;
    static constexpr int module = 280;

    static constexpr const int start_x = 140 * (5 * 2 + 1);
    static constexpr const int start_y = 140 * (3 * 2 + 1);

    static constexpr const unsigned int color_calib_delay = 20;
    static constexpr const unsigned int color_calib_loops = 10;
}

#endif //DUCTTAPE_IDENTIFIERS_HPP
