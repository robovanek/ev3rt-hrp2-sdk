//
// Created by kuba on 26.10.18.
//

#ifndef DUCTTAPE_STRATEGY_ALPHA_HPP
#define DUCTTAPE_STRATEGY_ALPHA_HPP

#include <roboconfino/include/store.hpp>
#include <program/include/base_runnable.hpp>
#include <roboconfino/include/serial.hpp>
#include "map_tiles.hpp"
#include "map_localizer.hpp"
#include "map_scanner.hpp"
#include "plan_executor.hpp"
#include "map_path.hpp"
#include "plan_scheduler.hpp"

namespace duck {

    class program;

    class fsm : public base_runnable {
    public:
        explicit fsm(roboconfino::store &store, program &prg);

        void update() override;
        void reset();
        void start();

        void reportPlanGo(steer direction);
        void reportPlanTurnaround();
        void reportPlanAlignTurn(steer direction);

        pos_vec usPos() const;

        pos_ang theta() const {
            return m_where.theta();
        }

        void stop();

    private:
        void registerRPC(program &prg);
        pos_vec trackPos() const;

        void enterSteeringEntryWait();
        void enterSteering();
        void enterMeasure();
        void enterMoveToEdge();
        void enterAlign();
        void enterAlignBackStep();
        void enterAlignBackTurn();

        bool pastEdge();

        void recordMeasurements();

        enum class state {
            steering_entry_wait,
            steering,
            moving_to_edge,
            measuring,
            align_wait,
            align_back_stepback,
            align_back_turn,
        };

        map m_map;
        odometry m_where;
        scanner m_us;
        executor m_do;
        field<pos_lin> radar_offX;
        field<pos_lin> radar_offY;
        field<pos_lin> steer_off;
        field<pos_lin> track_offX;
        field<pos_lin> track_offY;
        field<cf::integer> frontLength;
        roboconfino::serial_logger m_log;
        scheduler m_sched;
        int m_msrID = 0;
        state m_current = state::moving_to_edge;
        steer m_steerDir = steer::to_straight;

        bool was = false;
        int64_t ts;

        bool m_passive = false;

        coords m_oldTrackPos = coords(5, 3);

        bool steerNow();

        bool steerDone();

        pos_vec fixPos(direction direction);
    };
}

#endif //DUCTTAPE_STRATEGY_ALPHA_HPP
