//
// Created by kuba on 26.10.18.
//

#ifndef DUCTTAPE_SCANNER_HPP
#define DUCTTAPE_SCANNER_HPP

#include <program/include/base_types.hpp>
#include <program/include/base_runnable.hpp>
#include <program/include/io_sensor.hpp>
#include <program/include/io_motor.hpp>
#include <queue>
#include <bitset>
#include <roboconfino/include/serial.hpp>

namespace duck {

    struct sonic_msr {
        sonic_msr() = default;

        int latestID = 0;
        pos_lin distance = pos_lin::zero();
    };

    struct sonic_req {
        explicit sonic_req(bool all);
        sonic_req(bool l, bool c, bool r);

        void setAll(bool doScan);

        std::bitset<3> scan;
    };

    enum class sonic_op {
        goto_passive,
        goto_active,
        delay
    };

    struct sonic_task {
        explicit sonic_task(sonic_op op);
        sonic_task(sonic_op op, sonic_to end);
        sonic_task(sonic_op op, sonic_to begin, sonic_to end, int id);

        sonic_op op;
        sonic_to begin;
        sonic_to end;
        int id;
        int64_t ts_begin = 0;
        int32_t tacho_begin = 0;
    };

    class program;
    class scanner : public base_runnable {
    public:
        explicit scanner(store &config, program &prg);

        void update() override;

        sonic_msr &of(sonic_to where);
        const sonic_msr &of(sonic_to where) const;

        void put(sonic_to where);
        void scan(sonic_to end, const sonic_req &where, int id);

    private:
        void report(sonic_to pos);
        void putScan(sonic_to from, sonic_to where, int id);
        static sonic_to leftmost(const sonic_req &where);
        static sonic_to rightmost(const sonic_req &where);

        roboconfino::field<sensor_port> sonicPort;
        roboconfino::field<cf::integer> moveDelay;
        roboconfino::field<cf::integer> moveAng;
        roboconfino::field<cf::integer> centerDelay;
        ultrasonic_sensor m_sonic;
    public:
        radar_motor m_actuator;

    private:

        void statefulStart(sonic_task &tsk);
        bool statefulDone(sonic_task &tsk);

        std::array<sonic_msr, 3> m_msr;

        sonic_req m_scanned;
        bool m_started = false;

        sonic_to m_position;
        std::queue<sonic_task> m_queue;
        serial_logger m_log;

        void statefulStop(sonic_task &task);
    };
}

#endif //DUCTTAPE_SCANNER_HPP
