//
// Created by kuba on 21.10.18.
//

#ifndef DUCTTAPE_RUNNABLE_HPP
#define DUCTTAPE_RUNNABLE_HPP

namespace duck {
    class base_runnable {
    public:
        virtual ~base_runnable() = default;
        virtual void update() = 0;
    };
}

#endif //DUCTTAPE_RUNNABLE_HPP
