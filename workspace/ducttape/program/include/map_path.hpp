//
// Created by kuba on 18.11.18.
//

#ifndef DUCTTAPE_MAP_PATH_HPP
#define DUCTTAPE_MAP_PATH_HPP

#include <vector>
#include <bitset>
#include <deque>
#include <unordered_set>
#include <unordered_map>
#include "map_tiles.hpp"

namespace duck {
    typedef std::deque<coords> path;

    class path_finder {
    public:
        explicit path_finder(map &map);

        path_finder &addReturn(tile t);

        path_finder &addThrough(tile t);

        path bfs(const coords &curPos, direction curDir) const;

    private:
        typedef std::deque<coords> node_queue;
        typedef std::unordered_set<coords> node_set;
        typedef std::unordered_map<coords, coords> node_track;

        path backtrace(const coords &goal, const node_track &bt) const;

        bool okReturn(tile what) const {
            return m_allowReturn.test((size_t) what);
        }

        bool okThrough(tile what) const {
            return m_allowThrough.test((size_t) what);
        }

        map *m_map = nullptr;
        std::bitset<(size_t) tile::last> m_allowThrough;
        std::bitset<(size_t) tile::last> m_allowReturn;
    };
}

#endif //DUCTTAPE_MAP_PATH_HPP
