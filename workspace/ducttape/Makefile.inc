APPL_CXXOBJS += \
converter.o \
integration.o \
protocol.o \
framing.o \
posix_integration.o \
store_manager.o \
store.o \
serial_conf.o \
ev3rt_integration.o \
map_path.o \
comm_impl.o \
map_localizer.o \
base_units.o \
io_sensor.o \
io_motor.o \
map_scanner.o \
plan_executor.o \
plan_scheduler.o \
prg_main.o \
map_tiles.o \
plan_fsm.o \
fixarray.o \
fixstring.o \
fixmatrix.o \



APPL_COBJS += \
gcc_platform.o \
fix16_exp.o \
fix16_sqrt.o \
fix16_str.o \
fix16_trig.o \
fract32.o \
uint32.o \
fix16.o \



SRCLANG = c++
