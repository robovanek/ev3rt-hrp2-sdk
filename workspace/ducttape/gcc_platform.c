/* EV3RT implementation of atomic operations.
   Copyright (C) 2018 Jakub Vaněk

This file is part of GCC.

GCC is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation; either version 3, or (at your option) any later
version.

GCC is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

Under Section 7 of GPL version 3, you are granted additional
permissions described in the GCC Runtime Library Exception, version
3.1, as published by the Free Software Foundation.

You should have received a copy of the GNU General Public License and
a copy of the GCC Runtime Library Exception along with this program;
see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see
<http://www.gnu.org/licenses/>.  */

#include <kernel.h>

#define EV3RT_HIDDEN __attribute__ ((visibility ("hidden")))

// locking wrapper for interrupt and task contexts
bool_t ev3rt_cpu_lock(void) {
  // if cpu is already locked, do not unlock, otherwise go on
  if (sns_loc()) {
    return 0;
  } else {
    if (sns_ctx()) {
      // interrupt context
      iloc_cpu();
    } else {
      // task context
      loc_cpu();
    }
    return 1;
  }
}

// unlocking wrapper for interrupt and task contexts
void ev3rt_cpu_unlock(bool_t unlock) {
  // cpu wasn't locked, unlock it
  if (unlock) {
    if (sns_ctx()) {
      // interrupt context
      iunl_cpu();
    } else {
      // task context
      unl_cpu();
    }
  }
}

#define FETCH_OP_PAIR(OP, PFX_OP, INF_OP, WIDTH, TYPE)			\
  TYPE EV3RT_HIDDEN							\
  __sync_fetch_and_##OP##_##WIDTH (TYPE *ptr, TYPE val)			\
  {									\
    TYPE tmp;								\
									\
    bool_t unlock = ev3rt_cpu_lock();					\
    tmp = *ptr;								\
    *ptr = PFX_OP (tmp INF_OP val);					\
    ev3rt_cpu_unlock(unlock);						\
    return tmp;								\
  }									\
  TYPE EV3RT_HIDDEN							\
  __sync_##OP##_and_fetch_##WIDTH (TYPE *ptr, TYPE val)			\
  {									\
    TYPE  tmp;								\
									\
    bool_t unlock = ev3rt_cpu_lock();					\
    tmp = *ptr;								\
    tmp = PFX_OP (tmp INF_OP val);					\
    *ptr = tmp;								\
    ev3rt_cpu_unlock(unlock);						\
    return tmp;								\
  }

FETCH_OP_PAIR(add,   , +, 8, long long)
FETCH_OP_PAIR(sub,   , -, 8, long long)
FETCH_OP_PAIR(or,    , |, 8, long long)
FETCH_OP_PAIR(and,   , &, 8, long long)
FETCH_OP_PAIR(xor,   , ^, 8, long long)
FETCH_OP_PAIR(nand, ~, &, 8, long long)

FETCH_OP_PAIR(add,   , +, 4, int)
FETCH_OP_PAIR(sub,   , -, 4, int)
FETCH_OP_PAIR(or,    , |, 4, int)
FETCH_OP_PAIR(and,   , &, 4, int)
FETCH_OP_PAIR(xor,   , ^, 4, int)
FETCH_OP_PAIR(nand, ~, &, 4, int)

FETCH_OP_PAIR(add,   , +, 2, short)
FETCH_OP_PAIR(sub,   , -, 2, short)
FETCH_OP_PAIR(or,    , |, 2, short)
FETCH_OP_PAIR(and,   , &, 2, short)
FETCH_OP_PAIR(xor,   , ^, 2, short)
FETCH_OP_PAIR(nand, ~, &, 2, short)

FETCH_OP_PAIR(add,   , +, 1, signed char)
FETCH_OP_PAIR(sub,   , -, 1, signed char)
FETCH_OP_PAIR(or,    , |, 1, signed char)
FETCH_OP_PAIR(and,   , &, 1, signed char)
FETCH_OP_PAIR(xor,   , ^, 1, signed char)
FETCH_OP_PAIR(nand, ~, &, 1, signed char)

#define VAL_BOOL_CAS(WIDTH,TYPE) \
  TYPE EV3RT_HIDDEN								\
  __sync_val_compare_and_swap_##WIDTH (TYPE *ptr, TYPE oldval, TYPE newval)	\
  {										\
    TYPE tmp;									\
										\
    bool_t unlock = ev3rt_cpu_lock();						\
    tmp = *ptr;									\
    if (tmp == oldval)								\
    {										\
      *ptr = newval;								\
    }										\
    ev3rt_cpu_unlock(unlock);							\
    return tmp;									\
  }										\
  unsigned char EV3RT_HIDDEN							\
  __sync_bool_compare_and_swap_##WIDTH (TYPE *ptr, TYPE oldval, TYPE newval)	\
  {										\
    bool_t unlock = ev3rt_cpu_lock();						\
    unsigned char same = *ptr == oldval;					\
    if (same)									\
    {										\
      *ptr = newval;								\
    }										\
    ev3rt_cpu_unlock(unlock);							\
    return same;								\
  }

VAL_BOOL_CAS(1, signed char)
VAL_BOOL_CAS(2, short)
VAL_BOOL_CAS(4, int)
VAL_BOOL_CAS(8, long long)

void EV3RT_HIDDEN
__sync_synchronize (void)
{
}

#define SYNC_LOCKS(WIDTH,TYPE)						\
  TYPE EV3RT_HIDDEN							\
  __sync_lock_test_and_set_##WIDTH (TYPE *ptr, TYPE val)		\
  {									\
    TYPE tmp;								\
									\
    bool_t unlock = ev3rt_cpu_lock();					\
    tmp = *ptr;								\
    *ptr = val;								\
    ev3rt_cpu_unlock(unlock);						\
    return tmp;								\
  }									\
  void EV3RT_HIDDEN							\
  __sync_lock_release_##WIDTH (TYPE *ptr)				\
  {									\
    bool_t unlock = ev3rt_cpu_lock();					\
    *ptr = 0;								\
    ev3rt_cpu_unlock(unlock);						\
  }

SYNC_LOCKS(1, signed char)
SYNC_LOCKS(2, short)
SYNC_LOCKS(4, int)
SYNC_LOCKS(8, long long)
