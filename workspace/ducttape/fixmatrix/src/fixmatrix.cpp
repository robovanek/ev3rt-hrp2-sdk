
#include <fixmatrix/include/fixmatrix.hpp>
#include "fixmatrix.hpp"
#include "fixarray.h"


/*********************************
 * Operations between 2 matrices *
 *********************************/

mf16 mf16_mul(const mf16 &a, const mf16 &b) {
    uint8_t errors = a.errors() | b.errors();

    if (a.cols() != b.rows()) {
        errors |= FIXMATRIX_DIMERR;
    }

    mf16 dest(a.rows(), b.cols());
    dest.errors() = errors;

    for (uint_fast8_t row = 0; row < dest.rows(); row++) {
        for (uint_fast8_t column = 0; column < dest.cols(); column++) {
            fix16_t result = fa16_dot(
                    a.raw() + row * a.cols(), 1,
                    b.raw() + column, b.cols(),
                    a.cols());

            dest.at(row, column) = result;

            if (result == fix16_overflow)
                dest.errors() |= FIXMATRIX_OVERFLOW;
        }
    }
    return dest;
}

// Multiply transpose of at with b
mf16 mf16_mul_at(const mf16 &at, const mf16 &b) {
    uint8_t errors = at.errors() | b.errors();

    if (at.rows() != b.rows()) {
        errors |= FIXMATRIX_DIMERR;
    }

    mf16 dest(at.cols(), b.cols());
    dest.errors() = errors;

    for (uint_fast8_t row = 0; row < dest.rows(); row++) {
        for (uint_fast8_t column = 0; column < dest.cols(); column++) {
            fix16_t result = fa16_dot(
                    at.raw() + row, at.cols(),
                    b.raw() + column, b.cols(),
                    at.rows());

            dest.at(row, column) = result;
            if (result == fix16_overflow)
                dest.errors() |= FIXMATRIX_OVERFLOW;
        }
    }
    return dest;
}

mf16 mf16_mul_bt(const mf16 &a, const mf16 &bt) {
    uint8_t errors = a.errors() | bt.errors();

    if (a.cols() != bt.cols()) {
        errors |= FIXMATRIX_DIMERR;
    }

    mf16 dest(a.rows(), bt.rows());
    dest.errors() = errors;

    for (uint_fast8_t row = 0; row < dest.rows(); row++) {
        for (uint_fast8_t column = 0; column < dest.cols(); column++) {
            fix16_t result = fa16_dot(
                    a.raw() + row * a.cols(), 1,
                    bt.raw() + column * bt.cols(), 1,
                    a.cols());

            dest.at(row, column) = result;
            if (result == fix16_overflow)
                dest.errors() |= FIXMATRIX_OVERFLOW;
        }
    }
    return dest;
}

static mf16 mf16_addsub(const mf16 &a, const mf16 &b, bool add) {
    uint8_t errors;

    errors = a.errors() | b.errors();
    if (a.cols() != b.cols() || a.rows() != b.rows())
        errors |= FIXMATRIX_DIMERR;

    mf16 dest(a.rows(), a.cols());
    dest.errors() = errors;

    for (uint_fast8_t row = 0; row < dest.rows(); row++) {
        for (uint_fast8_t column = 0; column < dest.cols(); column++) {
            fix16_t sum;
            if (add)
                sum = fix16_add(a.at(row, column), b.at(row, column));
            else
                sum = fix16_sub(a.at(row, column), b.at(row, column));

            if (sum == fix16_overflow)
                dest.errors() |= FIXMATRIX_OVERFLOW;

            dest.at(row, column) = sum;
        }
    }
    return dest;
}

mf16 operator+(const mf16 &a, const mf16 &b) {
    return mf16_addsub(a, b, true);
}

mf16 operator-(const mf16 &a, const mf16 &b) {
    return mf16_addsub(a, b, false);
}

/*********************************
 * Operations on a single matrix *
 *********************************/

mf16 mf16::transpose() const {
    uint_fast8_t fullRow = cols();
    uint_fast8_t fullCol = rows();
    mf16 dest (fullRow, fullCol);

    for (uint_fast8_t row = 0; row < rows(); row++) {
        for (uint_fast8_t column = 0; column < cols(); column++) {
            int newCol = row;
            int newRow = column;
            dest.at(newRow, newCol) = at(row, column);
        }
    }

    return dest;
}

/***************************************
 * Operations of a matrix and a scalar *
 ***************************************/

static mf16 mf16_divmul_s(const mf16 &matrix, fix16_t scalar, bool mul) {
    mf16 dest(matrix.rows(), matrix.cols());
    dest.errors() = matrix.errors();

    for (uint_fast8_t row = 0; row < dest.rows(); row++) {
        for (uint_fast8_t column = 0; column < dest.cols(); column++) {
            fix16_t value = matrix.at(row, column);

            if (mul)
                value = fix16_mul(value, scalar);
            else
                value = fix16_div(value, scalar);

            if (value == fix16_overflow)
                dest.errors() |= FIXMATRIX_OVERFLOW;

            dest.at(row, column) = value;
        }
    }
    return dest;
}

mf16 operator*(const mf16 &matrix, const Fix16 &scalar) {
    return mf16_divmul_s(matrix, scalar.value, true);
}

mf16 operator*(const Fix16 &scalar, const mf16 &matrix) {
    return mf16_divmul_s(matrix, scalar.value, true);
}

mf16 operator/(const mf16 &matrix, const Fix16 &scalar) {
    return mf16_divmul_s(matrix, scalar.value, false);
}

/***************************************************
 * Solving linear equations using QR decomposition *
 ***************************************************/

/*
// Takes two columns vectors, v and u, of size n.
// Performs v = v - dot(u, v) * u,
// where dot(u,v) has already been computed
// u is assumed to be an unit vector.
static void subtract_projection(fix16_t *v, const fix16_t *u, fix16_t dot, int n, uint8_t *errors) {
    while (n--) {
        // For unit vector u, u[i] <= 1
        // Therefore this multiplication cannot overflow
        fix16_t product = fix16_mul(dot, *u);

        // Overflow here is rare, but possible.
        fix16_t diff = fix16_sub(*v, product);

        if (diff == fix16_overflow)
            *errors |= FIXMATRIX_OVERFLOW;

        *v = diff;

        v += FIXMATRIX_MAX_SIZE;
        u += FIXMATRIX_MAX_SIZE;
    }
}

void mf16_qr_decomposition(mf16 *q, mf16 *r, const mf16 *matrix, int reorthogonalize) {
    int i, j, reorth;
    fix16_t dot, norm;

    uint8_t stride = FIXMATRIX_MAX_SIZE;
    uint8_t n = matrix->rows;

    // This uses the modified Gram-Schmidt algorithm.
    // subtract_projection takes advantage of the fact that
    // previous columns have already been normalized.

    // We start with q = matrix
    if (q != matrix) {
        *q = *matrix;
    }

    // R is initialized to have square size of cols(A) and zeroed.
    r->columns = matrix->columns;
    r->rows = matrix->columns;
    r->errors = 0;
    mf16_fill(r, 0);

    // Now do the actual Gram-Schmidt for the rows.
    for (j = 0; j < q->columns; j++) {
        for (reorth = 0; reorth <= reorthogonalize; reorth++) {
            for (i = 0; i < j; i++) {
                fix16_t *v = &q->data[0][j];
                fix16_t *u = &q->data[0][i];

                dot = fa16_dot(v, stride, u, stride, n);
                subtract_projection(v, u, dot, n, &q->errors);

                if (dot == fix16_overflow)
                    q->errors |= FIXMATRIX_OVERFLOW;

                r->data[i][j] += dot;
            }
        }

        // Normalize the row in q
        norm = fa16_norm(&q->data[0][j], stride, n);
        r->data[j][j] = norm;

        if (norm == fix16_overflow)
            q->errors |= FIXMATRIX_OVERFLOW;

        if (norm < 5 && norm > -5) {
            // Nearly zero norm, which means that the row
            // was linearly dependent.
            q->errors |= FIXMATRIX_SINGULAR;
            continue;
        }

        for (i = 0; i < n; i++) {
            // norm >= v[i] for all i, therefore this division
            // doesn't overflow unless norm approaches 0.
            q->data[i][j] = fix16_div(q->data[i][j], norm);
        }
    }

    r->errors = q->errors;
}

void mf16_solve(mf16 *dest, const mf16 *q, const mf16 *r, const mf16 *matrix) {
    int row, column, variable;

    if (r->columns != r->rows || r->columns != q->columns || r == dest) {
        dest->errors |= FIXMATRIX_USEERR;
        return;
    }

    // Ax=b <=> QRx=b <=> Q'QRx=Q'b <=> Rx=Q'b
    // Q'b is calculated directly and x is then solved row-by-row.
    mf16_mul_at(dest, q, matrix);

    for (column = 0; column < dest->columns; column++) {
        for (row = dest->rows - 1; row >= 0; row--) {
            fix16_t value = dest->data[row][column];

            // Subtract any already solved variables
            for (variable = row + 1; variable < r->columns; variable++) {
                fix16_t multiplier = r->data[row][variable];
                fix16_t known_value = dest->data[variable][column];
                fix16_t product = fix16_mul(multiplier, known_value);
                value = fix16_sub(value, product);

                if (product == fix16_overflow || value == fix16_overflow) {
                    dest->errors |= FIXMATRIX_OVERFLOW;
                }
            }

            // Now value = R_ij x_i <=> x_i = value / R_ij
            fix16_t divider = r->data[row][row];
            if (divider == 0) {
                dest->errors |= FIXMATRIX_SINGULAR;
                dest->data[row][column] = 0;
                continue;
            }

            fix16_t result = fix16_div(value, divider);
            dest->data[row][column] = result;

            if (result == fix16_overflow) {
                dest->errors |= FIXMATRIX_OVERFLOW;
            }
        }
    }
}
*/
/**************************
 * Cholesky decomposition *
 **************************/

mf16 mf16_cholesky(const mf16 &matrix) {
    // This is the Cholesky–Banachiewicz algorithm.
    // Refer to http://en.wikipedia.org/wiki/Cholesky_decomposition#The_Cholesky.E2.80.93Banachiewicz_and_Cholesky.E2.80.93Crout_algorithms

    uint8_t err  = matrix.errors();

    if (matrix.rows() != matrix.cols())
        err |= FIXMATRIX_DIMERR;

    mf16 dest(matrix.rows(), matrix.cols());
    dest.errors() = err;

    for (uint_fast8_t row = 0; row < dest.rows(); row++) {
        for (uint_fast8_t column = 0; column < dest.cols(); column++) {
            if (row == column) {
                // Value on the diagonal
                // Ljj = sqrt(Ajj - sum(Ljk^2, k = 1..(j-1))
                fix16_t value = matrix.at(row, column);
                for (uint_fast8_t k = 0; k < column; k++) {
                    fix16_t Ljk = dest.at(row, k);
                    Ljk = fix16_mul(Ljk, Ljk);
                    value = fix16_sub(value, Ljk);

                    if (value == fix16_overflow || Ljk == fix16_overflow)
                        dest.errors() |= FIXMATRIX_OVERFLOW;
                }

                if (value < 0) {
                    if (value < -65)
                        dest.errors() |= FIXMATRIX_NEGATIVE;
                    value = 0;
                }

                dest.at(row, column) = fix16_sqrt(value);
            } else if (row < column) {
                // Value above diagonal
                dest.at(row, column) = 0;
            } else {
                // Value below diagonal
                // Lij = 1/Ljj (Aij - sum(Lik Ljk, k = 1..(j-1)))
                fix16_t value = matrix.at(row, column);
                for (uint_fast8_t k = 0; k < column; k++) {
                    fix16_t Lik = matrix.at(row, k);
                    fix16_t Ljk = matrix.at(column,k);
                    fix16_t product = fix16_mul(Lik, Ljk);
                    value = fix16_sub(value, product);

                    if (value == fix16_overflow || product == fix16_overflow)
                        dest.errors() |= FIXMATRIX_OVERFLOW;
                }
                fix16_t Ljj = dest.at(column, column);
                value = fix16_div(value, Ljj);
                dest.at(row, column) = value;

                if (value == fix16_overflow)
                    dest.errors() |= FIXMATRIX_OVERFLOW;
            }
        }
    }
    return dest;
}


/***********************************
 * Lower-triangular matrix inverse *
 **********************************/

mf16 mf16_invert_lt(const mf16 &matrix) {
    // This is port of the algorithm as found in the Efficient Java Matrix Library
    // https://code.google.com/p/efficient-java-matrix-library

    int_fast8_t i, j, k;
    const int_fast8_t n = std::min(matrix.rows(), matrix.cols());

    mf16 dest(n, n);

    dest.errors() = matrix.errors();

    // lolTODO reorder these operations to avoid cache misses

    // inverts the lower triangular system and saves the result
    // in the upper triangle to minimize cache misses
    for (i = 0; i < n; ++i) {
        const fix16_t el_ii = matrix.at(i, i);
        for (j = 0; j <= i; ++j) {
            fix16_t sum = (i == j) ? fix16_one : 0;
            for (k = i - 1; k >= j; --k) {
                sum = fix16_sub(sum, fix16_mul(matrix.at(i, k), dest.at(j, k)));
            }
            dest.at(j, i) = fix16_div(sum, el_ii);
        }
    }
    // solve the system and handle the previous solution being in the upper triangle
    // takes advantage of symmetry
    for (i = n - 1; i >= 0; --i) {
        const fix16_t el_ii = matrix.at(i, i);
        for (j = 0; j <= i; ++j) {
            fix16_t sum = (i < j) ? 0 : dest.at(j, i);
            for (k = i + 1; k < n; ++k) {
                sum = fix16_sub(sum, fix16_mul(matrix.at(k, i), dest.at(j, k)));
            }
            fix16_t result = fix16_div(sum, el_ii);;
            dest.at(i, j) = result;
            dest.at(j, i) = result;
        }
    }

    return dest;
}

fix16_t &mf16_row::operator[](uint_fast8_t column) {
    return m_parent->at(m_row, column);
}
