#include "fixstring.h"
#include <string.h>

void print_fix16_t(FILE *stream, fix16_t value, uint_fast8_t width, uint_fast8_t decimals)
{
    char buf[13];
    fix16_to_str(value, buf, decimals);

    uint_fast8_t len = strlen(buf);
    if (len < width) {
        width -= len;
        while (width-- > 0)
            fputc(' ', stream);
    }

    fputs(buf, stream);
}

void print_mf16(FILE *stream, const mf16 &matrix)
{
    if (matrix.errors()) {
        fprintf(stream, "MATRIX ERRORS: %d\n", matrix.errors());
    }

    uint_fast8_t row, column;
    for (row = 0; row < matrix.rows(); row++) {
        for (column = 0; column < matrix.cols(); column++) {
            fix16_t value = matrix.at(row, column);
            print_fix16_t(stream, value, 9, 4);
            fprintf(stream, " ");
        }
        fprintf(stream, "\n");
    }
}
