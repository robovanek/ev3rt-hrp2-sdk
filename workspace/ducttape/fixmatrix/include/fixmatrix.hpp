/* A very basic and small matrix algebra library atop libfixmath
 * fixed point numbers. Suitable for small matrices, usually less
 * than 10x10.
 * 
 * This library does not do saturating arithmetic, but it does
 * feature an overflow flag for detecting erroneous results.
 * 
 * Goals of the library are small footprint and fast execution.
 * Only very basic operations are supported.
 * 
 * Matrices can have any size from 1x1 up to FIXMATRIX_MAX_SIZE
 * (configurable), also non-square, but memory is always allocated
 * for the maximum size.
 * 
 * Error handling is done using flags in the matrix structure.
 * This makes it easy to detect if any errors occurred in any of
 * the computations, without checking a return status from each
 * function.
 * 
 * The functions still perform the calculations even if the result
 * is known to be erroneous.
 */

#ifndef FIXMATRIX_HPP
#define FIXMATRIX_HPP

#include <vector>
#include <cstdint>
#include <cstdbool>
#include <cstddef>
#include <fix16.hpp>

enum mf16_flag {
    FIXMATRIX_OVERFLOW = 0x01,
    FIXMATRIX_DIMERR = 0x02,
    FIXMATRIX_USEERR = 0x04,
    FIXMATRIX_SINGULAR = 0x08,
    FIXMATRIX_NEGATIVE = 0x10
};

class mf16;

class mf16_row {
public:
    mf16_row(mf16 &owner, uint_fast8_t row) : m_parent(&owner), m_row(row) {}

    fix16_t &operator[](uint_fast8_t column);

private:
    mf16 *m_parent;
    uint_fast8_t m_row;
};

class mf16 {
public:
    mf16() : m_rows(4), m_cols(4), m_data(m_rows * m_cols, 0) {}

    mf16(uint_fast8_t rows, uint_fast8_t cols) : m_rows(rows), m_cols(cols), m_data(m_rows * m_cols, 0) {}

    mf16(mf16 const &other) = default;

    mf16(mf16 &&other) noexcept = default;

    mf16 &operator=(mf16 const &other) = default;

    mf16 &operator=(mf16 &&other) noexcept = default;

    mf16_row operator[](uint_fast8_t row) {
        return mf16_row(*this, row);
    }

    mf16_row operator[](uint_fast8_t row) const {
        return mf16_row(const_cast<mf16 &>(*this), row);
    }


    fix16_t &at(uint_fast8_t row, uint_fast8_t col) {
        return m_data[row * m_cols + col];
    }

    fix16_t at(uint_fast8_t row, uint_fast8_t col) const {
        return m_data[row * m_cols + col];
    }

    void fill(fix16_t value) {
        m_errors = 0;
        m_data.assign(m_cols * m_cols, value);
    }

    void fill_diagonal(fix16_t value) {
        fill(0);

        for (uint_fast8_t n = 0; n < m_rows && n < m_cols; n++) {
            at(n, n) = value;
        }
    }

    uint_fast8_t rows() const {
        return m_rows;
    }

    uint_fast8_t cols() const {
        return m_cols;
    }

    friend mf16 operator+(const mf16 &a, const mf16 &b);

    friend mf16 operator-(const mf16 &a, const mf16 &b);

    friend mf16 operator*(const mf16 &a, const Fix16 &b);

    friend mf16 operator*(const Fix16 &a, const mf16 &b);

    friend mf16 operator/(const mf16 &a, const Fix16 &b);

    mf16 transpose() const;

    uint8_t errors() const {
        return m_errors;
    }

    uint8_t &errors() {
        return m_errors;
    }

    fix16_t *raw() {
        return m_data.data();
    }

    const fix16_t *raw() const {
        return m_data.data();
    }

private:
    uint_fast8_t m_rows;
    uint_fast8_t m_cols;
    /* Error flags are used to detect exceptions in the computations.
     * The flags are automatically propagated to the result if either
     * of the operands is invalid.
     * Currently the following flags are defined:
     * - FIXMATRIX_OVERFLOW: A value has exceeded 32767 and wrapped around
     * - FIXMATRIX_DIMERR: Operands have incompatible dimensions
     * - FIXMATRIX_USEERR: Function was called in unsupported way
     */
    uint8_t m_errors = 0;

    /* Data is stored in memory in row-major format, e.g.
     * entry at (row, column) is data[row][column]
     */
    std::vector<fix16_t> m_data = {};
};

// Operations between two matrices
mf16 mf16_mul(const mf16 &a, const mf16 &b);

// Multiply transpose of at with b
mf16 mf16_mul_at(const mf16 &at, const mf16 &b);

// Multiply a with transpose of bt
mf16 mf16_mul_bt(const mf16 &a, const mf16 &bt);

// QR-decomposition of a matrix
//
// Finds Q and R so that QR = A and Q is orthogonal and R is upper triangular.
//
// This function does not support rank-deficient matrices.
// If rank(A) < cols(A), FIXMATRIX_SINGULAR is set.
//
// Overdetermined systems of rows(A) > cols(A) are supported.
// For them, an 'economy' factorization is returned, with q
// being non-square. mf16_solve will then return least squares
// solution.
//
// Specifying reorthogonalize > 0 increases iterations and
// improves result accuracy. Reorthogonalize = 0 is the fastest
// and typically gives rounding error of 0.1-0.5%. Values >1
// rarely improve precision.
//
// In mf16_qr_decomposition, q and matrix, or alternatively, r and matrix
// may alias i.e. point to the same memory location.
//void mf16_qr_decomposition(mf16 &q, mf16 &r, const mf16 &matrix, int reorthogonalize);

// Solving a system of linear equations Ax = b, or equivalently,
// left division A\b, using QR-decomposition.
// matrix is the b and x is stored to dest.
// Dest can alias with matrix or q, but not with r.
// matrix may have multiple columns, which are then solved
// independently.
// If you really really want and think that it is a
// good idea to invert matrices, you can do it by
// passing identity matrix as 'matrix'.
//mf16 mf16_solve(const mf16 &q, const mf16 &r, const mf16 &matrix);

// Cholesky decomposition of a symmetric positive-definite matrix (matrix square root)
//
// Finds L so that L L' = A and L is lower triangular.
//
// Any negative square roots in computation are floored to 
// zero. If they are smaller than -0.001, FIXMATRIX_NEGATIVE
// error flag is set. Small negative values are often caused
// by rounding errors, while large negative values indicate
// non-positive definite matrix.
//
// Matrix is not checked for symmetricity. Only values in
// the lower left triangle are used.
//
// Dest and matrix can alias.
mf16 mf16_cholesky(const mf16 &matrix);

// Matrix inversion of a matrix through its lower triangular decomposition.
//
// Finds inv(A) through L, so that A inv(A) = I and I is the identitiy matrix 
// and L a lower triangular matrix such that L L' = A.
//
// The required lower triangular matrix can be obtained through mf16_cholesky().
//
// Matrix is not checked for symmetricity. Only values in
// the lower left triangle are used.
//
// Dest and matrix can alias.
mf16 mf16_invert_lt(const mf16 &matrix);

#endif // FIXMATRIX_HPP
