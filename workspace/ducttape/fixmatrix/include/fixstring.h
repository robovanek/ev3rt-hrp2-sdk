/* Utilities for printing fix16_t datatypes. */

#ifndef _FIXSTRING_H_
#define _FIXSTRING_H_

#include <stdio.h>
#include <fix16.h>
#include "fixmatrix.hpp"

/* All print_*() functions have interface similar to fprintf().
 */
void print_fix16_t(FILE *stream, fix16_t value, uint_fast8_t width, uint_fast8_t decimals);
void print_mf16(FILE *stream, const mf16 &matrix);

#endif
