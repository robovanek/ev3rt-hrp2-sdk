//
// main entry point
//

#include "app.h"
#include "prg_main.hpp"
#include <t_stddef.h>
#include <t_stdlib.h>
#include <kernel.h>
#include <syssvc/serial.h>
#include <ev3api.h>


void main_task(intptr_t arg) {
    ev3_lcd_set_font(EV3_FONT_MEDIUM);
    ev3_led_set_color(LED_RED);
    ev3_lcd_draw_string("DuctTape", 0, 0);
    duck::program prg;
    prg.enter();
}
